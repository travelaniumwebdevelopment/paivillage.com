<?php

if (empty($_POST)) {
    die('Post data should not be empty!');
}

#
# Verify captcha
$post_data = http_build_query(
    array(
        'secret' => '6Ld-FasUAAAAACZrTSM_lcPrsA6EPyeCn3AMsbRb',
        'response' => $_POST['g-recaptcha-response']
    )
);
$opts = array('http' =>
    array(
        'method'  => 'POST',
        'header'  => 'Content-type: application/x-www-form-urlencoded',
        'content' => $post_data
    )
);
$context  = stream_context_create($opts);
$response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
$result = json_decode($response);

if (!$result->success) {
    die($result['error-codes']);
}

$to         = "reservation@paivillage.com";
// $to         = "websupport@travelanium.com";

$title      = $_POST['title'];
$firstname  = $_POST['firstname'];
$lastname   = $_POST['lastname'];
$name       = "$title $firstname $lastname";
$email      = $_POST['email'];
$tel        = $_POST['tel'];
$country    = $_POST['country'];
$message    = $_POST['message'];


$subject = "Contact website form $email";

// define( 'SMTPHOST', 'smtp.mailgun.org' );
// define( 'SMTPUSER', 'postmaster@sandbox0c0bbe016a5548948e28ea2baeaa40b9.mailgun.org' );
// define( 'SMTPPASS', 'ac0066a4a470f5a242f9772e27f6dcd1' );
// define( 'SMTPPORT', 2525 );
define( 'SMTPHOST', 'smtp.gmail.com' );
define( 'SMTPUSER', '3rdparty@in10momentum.com' );
define( 'SMTPPASS', 'Q1w@e3r4t5' );
define( 'SMTPPORT', 587 );

//Load composer's autoloader
require '../forms/PHPMailer-5.2.25/PHPMailerAutoload.php';

$mail = new PHPMailer;                              // Passing `true` enables exceptions
//Server settings
$mail->CharSet = 'UTF-8';
$mail->SMTPDebug = 0;                                 // Enable verbose debug output
$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = SMTPHOST;
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = SMTPUSER;
$mail->Password = SMTPPASS;
// $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = SMTPPORT;

$mail->set('Timeout', 11);

//Content
$mail->Subject = $subject;

$messages  = "Customer contact from website" . "\r\n\r\n";
$messages .= $message . "\r\n\r\n";
$messages .= "Name: $name" .  "\r\n";
$messages .= "Email: $email" . "\r\n";
$messages .= "Tel: $tel" . "\r\n";
$messages .= "Country: $country" . "\r\n\r\n";


$mail->Body    = $messages;
//Recipients
$mail->setFrom('3rdparty@in10momentum.com', 'Message from website Pai Village Boutique Resort & Farm');
$mail->addAddress($to, 'Hotel Customer Service');
$mail->addReplyTo($email, $name);

if(!$mail->send()){
    jsonResponse( 'error', 'Message could not be sent.', $mail->ErrorInfo );
} else {
    jsonResponse('success','Message has been sent.');
}

function jsonResponse($status, $msg, $detail=NULL) {
    $data = [
        'status'    => $status,
        'message'   => $msg,
        'detail'    => $detail
    ];
    echo json_encode($data);
}
