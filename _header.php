<?php require_once '_functions.php' ?>
<!DOCTYPE html>
<html lang="<?php echo get_info('site_lang'); ?>" class="<?php html_class(); ?>">
<head>
    <?php include_once 'inc/head.php'; ?>
</head>
<body class="<?php body_class(); ?>">
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5H7BB78" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div id="page">
        <header class="site-header">
            <div class="main-header">
                <div class="site-logo">
                    <a class="link" href="./" rel="home">
                        <img class="logo lazy-load" data-src="./assets/elements/logo-small.png"
                        data-srcset="assets/elements/logo-small.png 1x, assets/elements/logo.png 2x"
                        alt="Dummy" width="80" height="66">
                    </a>
                </div>

                <div class="site-menu">
                    <nav class="site-navigation">
                        <ul class="site-navigation-list">
                            <?php include 'inc/main-menu.php'; ?>
                        </ul>
                    </nav>
                </div>

                <div class="site-lang">
                    <div class="site-lang-active">en <i class="far fa-chevron-down"></i></div>
                    <ul class="site-lang-list">
                        <li class="<?php echo get_current_lang('en'); ?>"><a href="<?php echo $lang_en ?>">en</a></li>
                        <li class="<?php echo get_current_lang('zh'); ?>"><a href="<?php echo $lang_zh; ?>">中文</a></li>
                    </ul>
                </div>

                <button type="button" id="booking_panel_toggle" data-booking-panel>
                    BOOK NOW
                </button>

                <button type="button" id="desktop_menu_toggle" class="menu-toggle lazy-load" data-desktop-menu data-bg="url(./assets/elements/bg-hamburger-menu.png)">
                    <span class="hamburgur">
                        <span class="bar"></span>
                        <span class="bar"></span>
                        <span class="bar"></span>
                    </span>
                </button>

                <button type="button" id="mobile_menu_toggle" class="menu-toggle lazy-load" data-mobile-menu data-bg="url(./assets/elements/bg-hamburger-menu.png)">
                    <span class="hamburgur">
                        <span class="bar"></span>
                        <span class="bar"></span>
                        <span class="bar"></span>
                    </span>
                </button>
            </div>
        </header>