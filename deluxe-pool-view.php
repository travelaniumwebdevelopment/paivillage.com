<?php
$html_class = '';
$body_class = 'page-accommodation';
$cur_page   = 'deluxe-pool-view';
$par_page   = 'accommodation';
$title      = 'Boutique Mountain View - Pai Village Boutique Resort & Farm';
$desc       = 'Exclusive Offers available on Official Site ONLY. Best Rates Guaranteed. Member Deal. Private Villa and Cottage, located along the banks of Pai River featuring Rustic Bamboo Cottage set among widing pathways.';
$keyw       = 'Rasa,Hospitality,Rasa Tower,Management,Development,Business Management,Business Development,Accounting,Manager,Hotel,Resort,Investment,,Buri Rasa,Rasa Collection,Boutique,Traditional, Pai, Mae Hong Son,Thailand';
$local      = 'en-US';
$page_url   = 'deluxe-pool-view.php';
$ogimage    = ['images/accommodation/boutique-mountain-view/main-slide-01.jpg', '1500', '1000'];

$lang_en    = $page_url;
$lang_zh    = 'zh/'. $page_url;
$lang_th    = 'th/'. $page_url;
include_once '_header.php' ?>
        <main class="site-main">
            <div class="swiper-container swiper-main">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img class="img-cover lazy-load" alt="luxury hotels Pai" data-src="images/accommodation/boutique-mountain-view/main-slide-01.jpg">
                    </div>
                </div>
                <?php include 'inc/scroll-down.php'; ?>
            </div>
            <section id="section" class="intro">
                <div class="bg-texture mountain">
                    <div class="container py-5">
                        <h1 class="header text-center mb-3 mb-md-5" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="100"><?php echo _room_name_5; ?></h1>
                        <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">Overlooking the resort pool with amazing green views of the distant mountains, our Boutique Mountain View Rooms are located on the second floor and feature nearly 40 square meters of indoor and outdoor living space. A king size bed, daybed and bathroom with luxurious rain shower set the tone for a holiday in true luxury and comfort. Decorated in a style unique to Pai Village, guests will appreciate touches of local hill tribe’s life in a variety of ways. A flat screen streaming TV and luxurious balcony with swing complete the setting for a wonderful holiday. The Boutique Mountain View rooms comfortably accommodate up to four guests with an extra bed.</p>
                        <div class="row py-4">
                            <div class="col-12 col-md-4 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <h2 class="subject"><b>Amenities</b></h2>
                                <ul class="pl-0">
                                    <li>Air Condition</li>
                                    <li>A Cable Flat Screen TV</li>
                                    <li>Bathrobe and Bath Amenities</li>
                                    <li>Coffee & Tea Making Facilities</li>
                                    <li>Private Balcony or Terrace</li>
                                    <li>Complimentary Refreshment</li>
                                </ul>
                            </div>
                            <div class="col-12 col-md-4 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <h2 class="subject"><b>Services</b></h2>
                                <ul class="pl-0">
                                    <li>Turn Down Service</li>
                                    <li>Daily Housekeeping</li>
                                </ul>
                            </div>
                            <div class="col-12 col-md-4 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <h2 class="subject"><b>Information</b></h2>
                                <ul class="pl-0">
                                    <li>Indoor Space <?php echo _room_space_5; ?> Sq.m</li>
                                    <li>Allow up to <?php echo _room_guest_5; ?> guests</li>
                                </ul>
                            </div>
                            <div class="col-12 py-3 text-center" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="250">
                                <a class="btn btn-radius-main-color" href="<?php echo ibe_url(get_info('ibeID'), get_info('site_lang')); ?>">BOOK NOW</a>
                            </div>
                        </div>
                        <div class="gallery-slider" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">
                            <h2 class="subject mb-4">Gallery :</h2>
                            <div class="swiper-container swiper-gallery">
                                <div class="swiper-wrapper gallery-popup">
                                    <a class="swiper-slide" href="./images/accommodation/boutique-mountain-view/main-slide-01.jpg"><img src="./images/accommodation/boutique-mountain-view/thumb/main-pic-01.jpg" alt="luxury hotels Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/accommodation/boutique-mountain-view/main-slide-02.jpg"><img src="./images/accommodation/boutique-mountain-view/thumb/main-pic-02.jpg" alt="Pai luxury hotels" width="1500" height="843"></a>
                                    <!-- <a class="swiper-slide" href="./images/accommodation/boutique-mountain-view/main-slide-03.jpg"><img src="./images/accommodation/boutique-mountain-view/thumb/main-pic-03.jpg" alt="perfect homestay Pai" width="1500" height="843"></a> -->
                                    <a class="swiper-slide" href="./images/accommodation/boutique-mountain-view/main-slide-04.jpg"><img src="./images/accommodation/boutique-mountain-view/thumb/main-pic-04.jpg" alt="hotel resort Pai Thailand" width="1500" height="843"></a>
                                    <!-- <a class="swiper-slide" href="./images/accommodation/boutique-mountain-view/main-slide-05.jpg"><img src="./images/accommodation/boutique-mountain-view/thumb/main-pic-05.jpg" alt="hotel resort Pai Thailand" width="1500" height="843"></a> -->
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <?php include_once '_footer.php'; ?>