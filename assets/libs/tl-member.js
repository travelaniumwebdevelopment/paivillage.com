/**
 * Module Name: Member
 * Description: This plugin will prepare member system by using browser cookies
 * Version: 0.2.3
 */

/**
 * @desc Check member cookies in browser
 * @return {string || boolean} are users member?
 */
(function($, window) {
    window.memberChecker = function() {
        var m = Cookies.get('member');
        if (m!==undefined)
            return Cookies.getJSON('member');
        else
            return false;
    }

    /**
     * @desc Create cooking for member
     */
    window.memberCreateCookies = function(options) {
        var defaults = {
            code: null,
            expire: 7,
            user: {
                email: null,
                name: null,
            }
        }
        var value = $.extend(defaults, options);

        var fname = value.user.name;
        var email = value.user.email;
        var username = (fname!==undefined) ? fname : email.substring(0, email.indexOf('@'));

        Cookies.set('member', {
            code: value.code,
            email: value.user.email,
            name: username,
        }, {
            expires: value.expire,
        });
    }

    /**
     * @desc This function will modify all url in page to use member code except url that already has accesscode parameter.
     * @param {string} code
     */
    window.memberAppendCode = function(code) {
        var origin = $('a[href^="https://reservation.travelanium.net/"]:not([href*="booking-management"])');
        origin.each( function() {
            var href = this.href;
            var paramExist = (href.indexOf('accesscode=') > -1) ? true : false;
            var newHref = href;

            if (!paramExist) {
                var param = (href.indexOf('?') > -1) ? '&accesscode=' : '?accesscode=';
                newHref = href + param + code;
            }

            this.href = newHref;
        });
    }

    /**
     * @desc This function will send customer data through Travelanium Form API
     * @param {string} options
     * @return {boolean}
     */
    window.memberSendData = function(options) {
        var defaults = {
            base:       'https://hotelservices.travelanium.net/crs-customer-form-rs/restresources/CRSCustomerFormDataService',
            formAPI:    null,
            customer: {
                name:       null,
                phone:      null,
                email:      null,
                message:    null,
                country: {
                    code:       null,
                    name:       null
                }
            }
        }, data = jQuery.extend(true, defaults, options);

        var formData = JSON.stringify({
            formKey:            data.formAPI,
            name:               data.customer.name,
            phone:              data.customer.phone,
            email:              data.customer.email,
            countryCode:        data.customer.country.code,
            countryName:        data.customer.country.name,
            message:            data.customer.message
        });

        var sendAjax = jQuery.ajax({
            type: 'POST',
            url: data.base,
            data: formData,
            contentType: 'application/json',
            cache: false,
        });

        sendAjax.done( function(response, status, jqXHR) {
            status = true;
        });

        sendAjax.fail( function(jqXHR, status, response) {
            status = false;
        });
    }
})(jQuery, window);