;(function($, window) {
    'use strict';
    
    if (window.tlForm!==undefined) {
        return false;
    }

    window.tlForm = function(options) {
        var status = false;
        var defaults = {
            url: 'https://hotelservices.travelanium.net/crs-customer-form-rs/restresources/CRSCustomerFormDataService',
            key: null,
            type: null,
            data: {
                name: null,
                phone: null,
                email: null,
                message: null,
                countryCode: null,
                countryName: null,
            },
            onSuccess: function(){},
            onError: function(){},
            complete: function(){},
        };
        var configs = $.extend('', defaults, options);

        if (!configs.key) {
            console.error('$.fn.tlForm require API key');
        }

        var data = {
            formKey: configs.key,
            customerFormType: configs.type,
            name: configs.data.name,
            phone: configs.data.phone,
            email: configs.data.email,
            countryCode: configs.data.countryCode,
            countryName: configs.data.countryName,
            message: configs.data.message
        };

        var dataStr = JSON.stringify(data);
        var senddata = $.post({
            url: configs.url,
            cache: false,
            contentType: 'application/json',
            data: dataStr,
        });

        senddata.done(function(response, status, jqXHR) {
            configs.onSuccess(response, status, jqXHR);
            status = true;
        });

        senddata.fail(function(jqXHR, status, response) {
            configs.onError(jqXHR, status, response);
            status = false;
        });

        senddata.always(function() {
            configs.complete();
        });

        return status;
    };
})(jQuery, window);