// ALL OPTIONS

// ACTION
var Defaults = {
	// Tự động tất table calendar khi click và html, root, class="t-datepicker-open"
	setAutoClose      : true,
}

// FORMAT
var Defaults = {
	// Định nghĩa giá trị nhập vào của các options, methods setDate cũng như khi in ra trong value của input.
	// Nên nhớ rằng Javascript sẽ hoạt động tốt nhất ở kiểu dữ liệu yyyy-mm-dd
	formatDate  : 'yyyy-mm-dd',
	// yyyy-mm-dd, yyyy-mm-dd
	// mm-dd-yyyy, dd-mm-yyyy
}

// THEME
var Defaults = {
	// Qui định calendar theme hiển thị, có liên quan đến limitNextMonth, limitPrevMonth, limitDateRanges
	numCalendar     : 2,

	titleCheckIn    : 'Check In',
	titleCheckOut   : 'Check Out',

	titleToday      : 'Today',
	// Title cho chuỗi ngày date-ranges 1 ngày là title số ít với ngôn ngữ là tiếng anh, trên 2 ngày là số nhiều thêm 's'
	titleDateRange  : 'night',
	titleDateRanges : 'nights',

	titleDayTime    : 'day',
	titleDayTimes   : 'days',

	titleDays       : [ 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su' ],
	titleMonths     : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'Septemper', 'October', 'November', "December"],

	// Qui định limit số ký tự từ 0 -> 3 để hiện thị trên date-picker, lệ thuộc vào titleMonths
	titleMonthsLimitShow : 3,  // January -> Jan
	// Qui định ký tự hiển thị là chuỗi + số tháng -> Thg 06, đơn vị tháng chuyển mặc định về số number, không lệ thuộc vào titleMonths
	replaceTitleMonths : null,    // Thg
	// Qui định ngày được hiển thị trên date-picker (dd-mm chỉ có ngày và tháng)
	showDateTheme   : null,    // dd-mm-yy, dd-mm, dd

	iconArrowTop : true,
	iconDate   : '&#x279C;',
    arrowPrev  : '&#x276E;',
    arrowNext  : '&#x276F;',

    // Ẩn hiện Title toDay
    toDayShowTitle       : true,  // true|false
    // Ẩn hiện title số đêm của chuỗi ngày date-ranges
    dateRangesShowTitle  : true,  // true|false

    // Định nghĩa ngày active toDay và nextDay nếu chưa có giá trị Date ở datepicker
    toDayHighlighted     : false, // true|false
    nextDayHighlighted   : false, // true|false

    // Định nghĩa màu sắc của ngày trong tuần
    daysOfWeekHighlighted: [0,6], // [0,1,2,3,4,5,6]

    // updateViewDate: 1, Chưa có v1.0.1
    // Day of the week start. 0 (Sunday) to 6 (Saturday)
}

// DATE
var Defaults = {
	// Cả 3 giá trị = null thì lấy default là toDay
	// Là ngày được chọn, bắt kì ngày nào trong cuốn lịch, nếu chọn quá setStartDate or endDate, nó vẫn selected.
	// Nhưng chuỗi limitDateRanges sẽ hoạt động không hiệu quả.
	// Giá trị được thêm vào phụ thuộc vào setFormatDate, nếu
	dateCheckIn  : null,
  dateCheckOut : null,

  // Là ngày bắt đầu của cuốn lịch, có thể lớn hơn hoặc nhở hơn ngày toDay, những ngày trước nó điều .t-disabled
  // Khi đã chọn ngày, cuốn lịch sẽ hiển thị vị trí mà ta đã chọn setStartDate
  // Để sử dụng view history lịch sử đã có
  // Nếu không có ngày thì mặc định lấy ngày toDay + limitDateRanges + limitNextMonth, có giá trị thì cộng dồn lên
  startDate    : null, // || toDay

  // Ngày cuối cùng của cuốn lịch, những ngày sau nó điều .t-disabled
  // Nếu không có ngày thì mặc định lấy ngày (startDate || toDay) + limitDateRanges + limitNextMonth
  // Nếu endDate nhở hơn startDate thì cả cuốn lịch .t-disabled
  endDate    : null,

  // Số tháng được next hoặc prev trong phạm vi show ra của calendar tính từ ngày toDay - mặc định next 12 tháng
  limitPrevMonth : 0,
  limitNextMonth : 11,

  // Số ngày giới hạn của t-check-in -> t-check-out để thê hiện chuỗi .t-range
  // Chuỗi ngày còn phụ thuộc vào startDate, endDate
  // Số đêm limit từ ngày CI - CO 5 ngày nhưng chỉ có 4 đêm
	limitDateRanges: 31,
	// Muốn hiển thị full 5 ngày ta sử dụng showFullDateRanges, nhưng phải thay đổi
	// titleDateRange: 'day', titleDateRanges: 'days'
	showFullDateRanges : false, // true || false - hiển thị số đêm, true là show full ngày
	dateDisabled  : []     // Array yyyy-mm-dd
}

// DATA HOLIDAYS
var Defaults = {
	// Là những ngày đặc biệt muốn hiển thị vào trong cuốn lịch
	fnDataEvent   : null,
	mergeDataEvent: false, // true || false - yyyy-mm-dd, false mm-dd
}


// ON EVENT TRIGGER
// Event khi click vào picker CI không phải click vào ngày của theme table
// .on('clickDateCI', function(e, dateCI) {
//     console.log('clickDateCI do something')
// })

// Event khi click vào picker CO không phải click vào ngày của theme table
// .on('clickDateCO', function(e, dateCO) {
//     console.log('clickDateCO do something')
// })
// Trước khi hiển thị table theme, thì làm gì đó
// .on('beforeShowDay', function() {
//     console.log('beforeShowDay do something')
// })
// Sau khi table calendar theme xuất hiện sẽ làm gì tiếp theo
// .on('afterShowDay', function(){
//     console.log('afterShowDay do something')
// })
// Sau khi table calendar them ẩn đi sẽ làm gì tiếp theo
// .on('toggleAfterHideDay', function(){
//     console.log('toggleAfterHideDay do something')
// })
// Sẽ chạy sau khi click vào ngày ở table theme CO
// .on('afterCheckOut', function(e, dataDate){
//     console.log('afterCheckOut do something')
//     console.log(dataDate[0], dataDate[1])
// })
// Chạy bất cứ lúc nào khi click vào ngày trên table theme
// .on('eventClickDay', function(e, dataDate) {
//     console.log('eventClickDay do something')
// })
// Chỉ chạy khi click vào ngày trên table theme CI
// .on('selectedCI', function(e, slDateCI) {
//     console.log('selectedCI do something')
// })
// Chỉ chạy khi click vào ngày trên table theme CO
// .on('selectedCO', function(e, slDateCO) {
//     console.log('selectedCO do something')
// })
// Chỉ chạy khi giá trị input của CI thay đổi
// .on('onChangeCI', function(e, changeDateCI) {
//     console.log('changeDateCI do something')
// })
// Chỉ chạy khi giá trị input của CO thay đổi
// .on('onChangeCO', function(e, changeDateCO) {
//     console.log('changeDateCO do something')
// })


// METHODS
// Hiển thị calendar với data date đã có
$('.t-datepicker').tDatePicker('show')

// Ẩn calendar với data date default, không reset ngày
$('.t-datepicker').tDatePicker('hide')

// update có giá trị đầu vào là (string, Object, '')
// Chịu ảnh hưởng trực tiếp từ setFormatDate yyyy-mm-dd ...
// '2018-12-24'
// new Date('2018-12-24')


// $('.t-datepicker').tDatePicker('update', 'dd-mm-yyyy')
// $('.t-datepicker').tDatePicker('update', ['dd-mm-yyyy', 'dd-mm-yyyy'])

// $('.t-datepicker').tDatePicker('updateCI', 'dd-mm-yyyy')
// $('.t-datepicker').tDatePicker('updateCO', 'dd-mm-yyyy')


$('.t-datepicker').tDatePicker('setStartDate', '2018-07-17')
$('.t-datepicker').tDatePicker('setEndDate', '2018-12-30')

$('.t-datepicker').tDatePicker('getDate')        // Wed Jul 18 2018 07:00:00 GMT+0700 (Indochina Time)
$('.t-datepicker').tDatePicker('getDates')       // ['CI', 'CO']

$('.t-datepicker').tDatePicker('getDateInput')   // yyyy-mm-dd
$('.t-datepicker').tDatePicker('getDateInputs')  // ['CI', 'CO']

$('.t-datepicker').tDatePicker('getDateUTC')     // 1333065600000
$('.t-datepicker').tDatePicker('getDateUTCs')    // ['CI', 'CO']


$('.t-datepicker').tDatePicker('setStartDate')     // Wed Jul 18 2018 07:00:00 GMT+0700 (Indochina Time)
$('.t-datepicker').tDatePicker('getStartDate')     // Wed Jul 18 2018 07:00:00 GMT+0700 (Indochina Time)
$('.t-datepicker').tDatePicker('getStartDateUTC')  // 1333065600000

$('.t-datepicker').tDatePicker('endDate')     // Wed Jul 18 2018 07:00:00 GMT+0700 (Indochina Time)
$('.t-datepicker').tDatePicker('getEndDate')     // Wed Jul 18 2018 07:00:00 GMT+0700 (Indochina Time)
$('.t-datepicker').tDatePicker('getEndDateUTC')  // 1333065600000





// Chưa có CO
// CI không được nhỏ hơn toDay -> if nhỏ hơn CI = toDay
// Tất cả các ngày update gọi sau sẽ replace ngày trước đó
// $(".t-datepicker").tDatePicker('updateCI', '16-06-2018')
// $(".t-datepicker").tDatePicker('updateCI', '16-07-2018')
// $(".t-datepicker").tDatePicker('updateCI', '15-04-2018')

// Đã có CO
// CI >= CO thì CO = null
// CO > CI-limitDay thì CO = null CI = CI
// CI-limitDay là ngày từ CI + options setLimitDays

// CO Flow là dựa theo ngày CO_limitDay cùng với toDay để làm mốc thời gian cố định
// Khi chưa có CI
// CO không vượt 31 ngày setLimitsDay
// CI = toDay
// Đã vượt qua 31 ngày setLimitDays - cần xác định CI -> default
// CI = CO_limitDay
// CO_limitDay là CO - setLimitDays - 31

// Khi đã có CI
// CO không vượt 31 ngày setLimitDays nằm trong từ toDay - setLimitDays
// CI = op_CI;
// Nếu CO nhỏ hơn CI: CI = toDay

// CO vượt 31 ngày setLimitDays nằm trong từ toDay - setLimitDays
// CI > CO_dateLimit giữ lại ngày CI = CI, vì lúc này nằm trong khoản setLimitDays
// Nhỏ hơn, là ngoài khoản setLimitDays thì xác định lại
// CI = CO_dateLimit;

// Nếu rơi vào trường họp CO < CI thì chắc chắn phải reset lại
// CI = CO_dateLimit;

// Nếu CO nhỏ hơn CI: CI = CO_dateLimit

// UPDATE
// update CI = 15-06-2018
$(".t-datepicker").tDatePicker('update', '14-06-2018')
$(".t-datepicker").tDatePicker('update', '15-06-2018')
$(".t-datepicker").tDatePicker('update', ['20-08-2018', '20-08-2018'])

// Among setLimitDays CO_limits but CI,CO > toDay
$(".t-datepicker").tDatePicker('update', ['20-08-2018', '21-08-2018'])
$(".t-datepicker").tDatePicker('update', ['20-08-2018', '19-08-2018'])

// Among setLimitDays CO_limits but CI,CO > toDay
$(".t-datepicker").tDatePicker('update', ['20-06-2018', '21-06-2018'])
$(".t-datepicker").tDatePicker('update', ['20-06-2018', '19-06-2018'])

// toDay default 15-06-2018 - Check-In
// None value CO
// (CI < toDay) => CI = toDay
$(".t-datepicker").tDatePicker('updateCI', '14-06-2018') // CI = 15-06-2018
// (CI >= toDay) => CI = CI
$(".t-datepicker").tDatePicker('updateCI', '15-06-2018') // CI = 15-06-2018

// Have value CO,
// Have CO, CI >= CO => CI = CI, CO = null
$(".t-datepicker").tDatePicker('updateCI', '16-06-2018') // CI = 16-06-2018
$(".t-datepicker").tDatePicker('updateCO', '16-06-2018') // CO = null

// CO_limit = CO - setLimitDays (default 31 days)
// (CI < CO && CI < CO_limit) =>  CI = toDay, CO = CO
$(".t-datepicker").tDatePicker('updateCI', '13-06-2018') // CI = 15-06-2018
$(".t-datepicker").tDatePicker('updateCO', '16-07-2018') // CO = 16-07-2018

// (CI < CO && CI > CO_limit) => CI = CI, CO = CO
$(".t-datepicker").tDatePicker('updateCI', '19-06-2018') // CI = 19-06-2018
$(".t-datepicker").tDatePicker('updateCO', '20-07-2018') // CO = 20-07-2018



// toDay 15-06-2018 - Check-Out
// None value CI
// (CO =< toDay) => CO = nextDay, CI = toDay
$(".t-datepicker").tDatePicker('updateCO', '15-06-2018')
// CI = 15-06-2018 - CO = 16-06-2018

// Case 1:
// (CO > toDay && CO_limits > toDay) => CO = CO, CI = CO_limits
$(".t-datepicker").tDatePicker('updateCO', '30-08-2018')
// CI = 30-07-2018, CO = 30-08-2018

// Case 2:
// (CO > toDay && CO_limits < toDay) => CO = CO, CI = toDay
$(".t-datepicker").tDatePicker('updateCO', '25-06-2018')
// CI = 15-06-2018, CO = 25-06-2018

// CO_limits = CO - setLimitDays (31 days default)

// Have value CI
// (CO_limits < toDay)
// Case 1: (CO > CI) => CO = CO, CI = CI
$(".t-datepicker").tDatePicker('updateCI', '20-06-2018') // CI = 20-06-2018
$(".t-datepicker").tDatePicker('updateCO', '21-06-2018') // CO = 21-06-2018

// Case 2: (CO < CI) => CO = CO, CI = toDay
$(".t-datepicker").tDatePicker('updateCI', '20-06-2018') // CI = 15-06-2018
$(".t-datepicker").tDatePicker('updateCO', '19-06-2018') // CO = 19-06-2018

// (CO_limits > toDay)
// Case 1: (CI > CO_limits) => CO = CO, CI = CI
$(".t-datepicker").tDatePicker('updateCI', '20-09-2018') // CI = 20-09-2018
$(".t-datepicker").tDatePicker('updateCO', '25-09-2018') // CO = 25-09-2018

// Case 2: (CI < CO_limits) => CO = CO, CI = CO_limits
$(".t-datepicker").tDatePicker('updateCI', '20-06-2018')
$(".t-datepicker").tDatePicker('updateCO', '30-09-2018')
// CI = 30-08-2018, CO = 30-09-2018

