//@prepros-prepend '../libs/tl-member.js'
;(function($){
    //
    // Member Panel
    //
    var $body = $('body');
    $('[data-member-panel]').on('click', function(event) {
        event.preventDefault();
        var method = this.dataset.memberPanel;
        switch(method) {
            case 'show':
                $body.addClass('member-panel-show');
                break;
            case 'hide':
                $body.removeClass('member-panel-show');
                break;
            default:
                $body.toggleClass('member-panel-show');
        }
    });

    // Check member
    var getMember = memberChecker();
    var $memberPanel = $('#member_panel');

    if (!getMember) {
        // You're guest
        $memberPanel.addClass('is-guest');
    } else {
        // You're Member
        createMemberUI('member');
    }

    $.validate({
        form: '#form_member',
        modules: 'html5',
        onSuccess: function($form) {
            var email       = $form.find('[name=email]').val();

            memberCreateCookies({
                code: 'member',
                expire: 7,
                user: {
                    email: email,
                }
            });

            var memberCode  = memberChecker().code;
            window.open( 'https://reservation.travelanium.net/propertyibe2?propertyId=253&onlineId=4&accesscode='+memberCode, '_blank' );

            createMemberUI( memberCode );
            memberSendData({
                formAPI: 'e6047c68ac120002536fa7ef2fb27256',
                customer: {
                    email: email,
                }
            });

            return false;
        }
    });

    /**
     * Create Member User Interface
     * @param {string} code
     */
    function createMemberUI(code) {
        memberAppendCode(code);
        $memberPanel.removeClass('is-guest').addClass('is-member');
        $('#form_booking').booking('update', 'secretCode', code);
    }
})(jQuery);