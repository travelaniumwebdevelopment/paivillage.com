//@prepros-prepend '../plugins/t-datepicker/public/theme/js/t-datepicker.min.js'
//@prepros-prepend '../libs/tl-booking.js

;(function($){
    var now = moment().tz('Asia/Bangkok');
    var twoday = moment().add(2,'d');
    var holidays = {
        '2018-12-31' : 'Happy New Years',
        '2019-02-14' : 'Valentine\'s Day ',
    };
    var datepickerPanel = $('.datepicker-panel').tDatePicker({
        dateCheckIn: now,
        dateCheckOut: twoday,
        startDate: now,
        limitPrevMonth: 0,
        limitNextMonth: 13,
        limitDateRanges: 91,
        // fnDataEvent: holidays,
        // mergeDataEvent: true,
        showFullDateRanges: false,
        numCalendar: 1,
        titleDateRange: 'night',
        titleDateRanges: 'nights',
        titleToday: 'Today',
        titleDays: ['MO','TU','WE','TH','FR','SA','SU'],
        titleMonths: ['January','February','March','April','May','June','July','August','September','October','November','December'],
        titleMonthsLimitShow: 99,
        showDateTheme: 'dd-mm-yy',
        iconArrowTop: false,
        iconDate: '<i class="fal fa-calendar-alt" aria-hidden="true"></i>',
        arrowPrev: '❮',
        arrowNext: '❯',
        toDayShowTitle: true,
        dateRangesShowTitle: true,
        toDayHighlighted: true,
        nextDayHighlighted: true,
        daysOfWeekHighlighted: ['0','6'],
    }).on('afterCheckOut',function(e, dataDate){
        var valCI = new Date(dataDate[0]);
        var valCO = new Date(dataDate[1]);
        var valueCI = moment(valCI).format('YYYY-MM-DD');
        var valueCO = moment(valCO).format('YYYY-MM-DD');
        $('#form_booking').find('[name="checkin"]').val(valueCI);
        $('#form_booking').find('[name="checkout"]').val(valueCO);
    })
    datepickerPanel.tDatePicker('update', [now,twoday]);

    //
    // Booking Panel
    //
    var today = new Date();
    var tomorrow = nextDay(today, 1);

    var $checkin = $('#checkin_datepicker');
    var $checkout = $('#checkout_datepicker');
    var $checkinBox = $('#checkin_display');
    var $checkinBoxDate = $checkinBox.find('.date');
    var $checkinBoxMonth = $checkinBox.find('.month');
    var $checkinField = $checkinBox.find('[name="checkin"]');
    var $checkoutBox = $('#checkout_display');
    var $checkoutBoxDate = $checkoutBox.find('.date');
    var $checkoutBoxMonth = $checkoutBox.find('.month');
    var $checkoutField = $checkoutBox.find('[name="checkout"]');

    var dayNameMinDefault = ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA'];
    var monthNameDefault = ['JANUARY', 'FEBRUARY', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY', 'AUGUST', 'SEPTEMBER', 'OCTOBER', 'NOVEMBER', 'DECEMBER'];

    $checkin.datepicker({
        minDate: today,
        dayNamesMin: dayNameMinDefault,
        dateFormat: 'yy-mm-dd',
        onSelect: function(date, obj) {
            setCheckinDate(date);

            var tomorrow = nextDay(date, 1);
            $checkout.datepicker('option', 'minDate', tomorrow);

            var checkoutDate = $checkout.datepicker('getDate');
            setCheckoutDate(getFormatDate('yy-mm-dd', checkoutDate));

            $checkinBox.removeClass('show-datepicker');
            $checkoutBox.addClass('show-datepicker');
        }
    });

    $checkout.datepicker({
        minDate: tomorrow,
        dayNamesMin: dayNameMinDefault,
        dateFormat: 'yy-mm-dd',
        onSelect: function(date, obj) {
            setCheckoutDate(date);

            $checkoutBox.removeClass('show-datepicker');
        }
    });

    var $dateBox = $('.control-datepicker');
    $dateBox.each(function() {
        var $this = $(this);
        $this.on('click', function(e) {
            if (!$this.hasClass('show-datepicker')) {
                $this.addClass('show-datepicker');
                $dateBox.not($this).removeClass('show-datepicker');
            } else {
                $this.removeClass('show-datepicker');
            }
        });
    });

    $(document).on('click', function(e) {
        var $target = $(e.target);
        if (
            !$target.closest('.control-datepicker').length &&
            !$target.is('.ui-datepicker-prev') &&
            !$target.is('.ui-datepicker-next')
        ) {
            $dateBox.removeClass('show-datepicker');
        }
    });

    var $room = $('#room_display');
    var $roomUp = $room.find('.value-up');
    var $roomDown = $room.find('.value-down');
    var $roomValue = $room.find('.value');
    var $roomField = $room.find('[name="numofroom"]');

    $roomUp.on('click', roomIncreasement);
    $roomDown.on('click', roomDecreasement);

    // Set Default Value
    setCheckinDate(getFormatDate('yy-mm-dd', today));
    setCheckoutDate(getFormatDate('yy-mm-dd', tomorrow));
    setRoomValue(1);

    function nextDay(current, next) {
        var a = new Date(current).getTime();
        var b = a + (1000 * 60 * 60 * 24 * next);
        var date = new Date(b);
        return date;
    }

    function updateText(element, text) {
        $(element).text(text);
    }

    function updateValue(field, value) {
        $(field).val(value);
    }

    function getDateString(date) {
        var obj = new Date(date);
        var value = {
            date: obj.getDate(),
            month: getMonthString(obj.getMonth()),
            year: obj.getFullYear(),
        }
        return value;
    }

    function getFormatDate(format, dateObject) {
        var formatted = $.datepicker.formatDate(format, dateObject);
        return formatted;
    }

    function getMonthString(month) {
        var monthName = monthNameDefault;
        return monthName[month];
    }

    function setCheckinDate(date) {
        updateValue($checkinField, date);
        updateText($checkinBoxDate, getDateString(date).date);
        updateText($checkinBoxMonth, getDateString(date).month);
    }

    function setCheckoutDate(date) {
        updateValue($checkoutField, date);
        updateText($checkoutBoxDate, getDateString(date).date);
        updateText($checkoutBoxMonth, getDateString(date).month);
    }

    function setRoomValue(room) {
        updateValue($roomField, room);
        $roomValue.text(room);
        checkStateRoomControl(room);
    }

    function roomIncreasement() {
        var value = parseInt($roomValue.text());
        if (value >= 1 && value < 9) {
            var newValue = ++value;
            setRoomValue(newValue);
        }
    }

    function roomDecreasement() {
        var value = parseInt($roomValue.text());
        if (value > 1 && value <= 9) {
            var newValue = --value;
            setRoomValue(newValue);
        }
    }

    function checkStateRoomControl(value) {
        if (value <= 1) {
            $roomDown.addClass('disabled');
        } else {
            $roomDown.removeClass('disabled');
        }
        if (value >= 9) {
            $roomUp.addClass('disabled');
        } else {
            $roomUp.removeClass('disabled');
        }
    }
    //*** end booking panel */

    /*** another click close collapse */
    $(document).on('click', function(event) {
        var target = event.target;
        var excerpt = '.ui-datepicker-prev, .ui-datepicker-next';
        if (
            !$(target).closest('#collapseBooking').length &&
            !$(target).is(excerpt) 
        ) {
            $('#collapseBooking').removeClass('show');
        }
    });

    // Booking
    $('#form_booking').booking({
        propertyId: 253,
        submitSelector: '[type="submit"]',
        checkInSelector: '[name="checkin"]',
        checkOutSelector: '[name="checkout"]',
        adultSelector: '[name="numofadult"]',
        childSelector: '[name="numofchild"]',
        roomSelector: '[name="numofroom"]',
        codeSelector: '[name="accesscode"]',
        language: function() {
            var lang = document.documentElement.lang;
            return lang;
        },
        dateFormat: "dd M yy",
        secretCode: '',
        builtinDatepicker: false,
        onlineId: 4,
    });
})(jQuery);