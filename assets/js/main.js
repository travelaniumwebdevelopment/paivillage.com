//@prepros-prepend '../js/vendor.js'

;(function($){
    var $document = $(document),
        $body = $('body'),
        $win = $(window);

    var winY = window.scrollY || window.pageYOffset;
    var winW = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

    objectFitVideos();
    objectFitImages();
    
    AOS.init({ disable: 'mobile' });

    $win.on('load scroll', function() {
        winY = window.scrollY;
    }.throttle(16));

    $win.on('resize', function() {
        winW = window.innerWidth;
    }.throttle(16));

    /**
     * Header Super Effect
     */
    $(window).on('load scroll resize', function() {
        if (winY > 100 && winW >= 1200) {
            $('.main-header').addClass('lite');
        } else {
            $('.main-header').removeClass('lite');
        }
    }.throttle(16));

    var headerPos = 0;
    var offset = 0;

    function updateGhostHeaderPosition() {
        headerPos = $('.site-header').offset().top;
    }

    function updateGhostHeaderOffset() {
        offset = $body.hasClass('admin-bar') && winW > 782 ? 32 : 0;
    }

    function updateHeaderState() {
        if (winY >= headerPos - offset) {
            $('.main-header').addClass('fixed');
        } else {
            $('.main-header').removeClass('fixed');
        }
    }

    if ($body.hasClass('admin-bar')) {
        updateGhostHeaderPosition();
        updateGhostHeaderOffset();
        updateHeaderState();

        $(window).on('load resize', function() {
            updateGhostHeaderPosition();
            updateGhostHeaderOffset();
        }.throttle(16));

        $(window).on('load scroll resize', function() {
            updateHeaderState();
        }.throttle(16));
    }

    /**
     * Menu functons
     * @desc Control menu by toogle body classes
     * @since 1.1.0
     */
    $('[data-desktop-menu]').on('click', function() {
        var method = this.dataset.desktopMenu || null;
        switch (method) {
            case 'show':
                desktopMenu('show');
                break;
            case 'hide':
                desktopMenu('hide');
                break;
            default:
                desktopMenu('toggle');
        }
    });
    
    $('[data-mobile-menu]').on('click', function() {
        var method = this.dataset.mobileMenu || null;
        switch (method) {
            case 'show':
                $body.addClass('mobile-menu-show');
                break;
            case 'hide':
                $body.removeClass('mobile-menu-show');
                break;
            default:
                $body.addClass('mobile-menu-show');
        }
    });

    $(window).on('load resize', function() {
        if (winW >= 1200) {
            $body.removeClass('mobile-menu-show');
        }
    }.throttle(16));

    $(window).on('load scroll resize', function() {
        if (winY > 100 && winW >= 1200) {
            desktopMenu('show');
        }

        if (winW < 1200) {
            desktopMenu('hide');
        }
    }.throttle(16));

    function desktopMenu(method, animateTimeout) {
        var timeout = animateTimeout || 350;
        var methods = {
            show: function() {
                if (this._state()) return;
                $body.addClass('desktop-menu-show desktop-menu-show--animating');
                setTimeout(function() {
                    $body.removeClass('desktop-menu-show--animating');
                    $body.addClass('desktop-menu-show--shown');
                }, timeout);
            },
            hide: function() {
                if (!this._state()) return;
                $body.removeClass('desktop-menu-show desktop-menu-show--shown');
                $body.addClass('desktop-menu-show--animating');
                setTimeout(function() {
                    $body.removeClass('desktop-menu-show--animating');
                }, timeout);
            },
            toggle: function() {
                if (!this._state()) {
                    this.show();
                } else {
                    this.hide();
                }
            },
            _state: function() {
                return $body.hasClass('desktop-menu-show');
            }
        };

        return methods[method]();
    }
    
    // Go to top
    $('#back-top').hide();
    $win.on('scroll', function() {
        if ($(this).scrollTop() > 100) {
            $('#back-top').fadeIn();
        } else {
            $('#back-top').fadeOut();
        }
    });
        
    $('#back-top a').on('click', function () {
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
    
    //
    // Scroll Detection
    //
    var scrollY = window.scrollY || window.pageYOffset;
    function scrollDetection() {
        var newY = window.scrollY || window.pageYOffset;
        if (newY > scrollY) {
            $body.addClass('scroll-down').removeClass('scroll-up');
        } else {
            $body.addClass('scroll-up').removeClass('scroll-down');
        }
        scrollY = newY;
    }

    $win.on('scroll', scrollDetection.throttle(16));

    // Block Download img
    $("body").on("contextmenu", "img", function(e) {
        return false;
    });

    // Side Panel
    $('[data-sidepanel-click]').on('click', function(event) {
        event.preventDefault();
        var action = this.dataset.sidepanelClick;
        return sidepanel(action);
    });

    $('[data-sidepanel-drag]').each(function() {
        var el = this;
        var action = el.dataset.sidepanelDrag;
        $(el).swipe({
            swipe: function(event, direction) {
                if (action=='show' && direction=='right') {
                    sidepanel('show');
                }
                if (action=='hide' && direction=='left') {
                    sidepanel('hide');
                }
            },
        });
    });

    $win.on('resize', (function() {
        var winW = window.innerWidth;
        if (winW >= 992) {
            sidepanel('hide');
        }
    }));

    function sidepanel(method) {
        var methods = {
            show: function() {
                return $('body').addClass('side-panel-show');
            },
            hide: function() {
                return $('body').removeClass('side-panel-show');
            },
            toggle: function() {
                return $('body').toggleClass('side-panel-show');
            },
            state: function() {
                return ($('body').hasClass('side-panel-show')) ? true : false;
            }
        };
        return methods[method]();
    }

    // Animate Scroll Button
    $('[data-scroll]').each(function() {
        var _obj = $(this);

        _obj.on('click', function(event) {
            event.preventDefault();

            var target = _obj.attr('href');
            var offset = _obj.data('scroll-offset') || 0;
            var speed = _obj.data('scroll-speed') || 600;

            $.smoothScroll({
                offset: offset,
                scrollTarget: target,
                speed: speed,
            });
        });
    });

    //Swiper
    new Swiper('.swiper-main', {
        loop: true,
        effect: 'fade',
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        autoplay: {
            delay: 5000,
        },
        speed : 1500,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });
    new Swiper('.carousel-basic', {
        loop: true,
        effect: 'fade',
        autoplay: {
            delay: 5000,
        },
        navigation: {
            nextEl: '.carousel-navigation-next',
            prevEl: '.carousel-navigation-prev',
        },
    });
    new Swiper('.swiper-offers', {
        autoplay: {
            delay: 5000,
        },
        speed : 1000,
        loop: false,
        slidesPerView: 3,
        spaceBetween: 15,
        breakpoints: {
            767: {
                slidesPerView: 1
            },
            991: {
                slidesPerView: 2
            }
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });
    new Swiper('.swiper-gallery', {
        autoplay: {
            delay: 5000,
        },
        speed : 1000,
        loop: false,
        slidesPerView: 4,
        spaceBetween: 5,
        breakpoints: {
            767: {
                slidesPerView: 2
            },
            991: {
                slidesPerView: 3
            }
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });
    new Swiper('.swiper-roomtypes', {
        loop: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        autoplay: {
            delay: 3000,
        },
        speed : 1000,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });

    var lazyLoadInstance = new LazyLoad({
        elements_selector: ".lazy-load"
    });

    //side booking
    var $document = $(document),
        $body = $('body');

    $('[data-booking-panel]').on('click', function(event) {
        event.preventDefault();

        var method = this.dataset.bookingPanel || null;
        switch (method) {
            case 'show':
                $body.addClass('booking-panel-show');
                break;
            case 'hide':
                $body.removeClass('booking-panel-show');
                break;
            default:
                $body.toggleClass('booking-panel-show');
        }
    });

    $document.on('click', function(event) {
        var target = event.target;
        var datepicker = '.ui-datepicker-prev, .ui-datepicker-next';
        if (
            ! $(target).closest('.booking-panel, .ui-datepicker, #booking_panel_toggle').length &&
            ! $(target).is(datepicker)
        ) {
            $body.removeClass('booking-panel-show');
        }
    });

    // Subscribe Form
    $.validate({
        form: '#subscribe_form',
        modules: 'html5, security',
        onSuccess: function( $form ) {
            var form = $form.get(0);
            var fromURL = form.baseURI;
            var formAction = form.action;
            var formData = $form.serializeArray();

            formData.push({
                name: 'base',
                value: fromURL,
            });

            var result = $form.find('.result');
            var txtSuccess = 'Subscription Successful';
            var txtFailed = 'Subscription Failed!';

            var sendform = tlForm({
                key: '935a3f9dac12000560adda2fa04f4691',
                data: {
                    email: $form.find('[name="s-mail"]').val(),
                },
            });

            if (!sendform) {
                $form.addClass('success')
                     .removeClass('failed')
                     .find(':focus').blur();
                result.hide();
                result.html(txtSuccess).slideDown();
                form.reset();
            } else {
                $form.addClass('failed')
                     .removeClass('success');
                result.hide();
                result.html(txtFailed).slideDown();            
            }

            return false;
        },
        onError: function( $form ) {
            var form = $form.get(0);
            var result = $form.find('.result');
            result.hide();
        },
    });

    $('.gallery-popup').each(function() { 
        $(this).magnificPopup({
            delegate: 'a', 
            type: 'image',
            gallery: {
              enabled:true
            }
        });
    });
})(jQuery);