;(function($, window) {
    $.fn.instagramGallery = function(options) {
        var defaults = {
            token: null,
            limit: 9,
            complete: function(){},
        };
        var configs = $.extend('', defaults, options);

        return this.each(function() {
            var $el = $(this);
            var path = window.location.origin;
            var subpath = (window.location.hostname === 'localhost' || window.location.hostname === '127.0.0.1' || window.location.hostname === 'travelanium.com') ? '/pai-village' : '';
            var data = $.getJSON(path + subpath + '/apps/instagram.php');
            data.then(function(response) {
                var images  = response.data;
                var limit   = Math.min(configs.limit, images.length);

                for (var i = 0; i <= limit-1; i++) {
                    var img     = images[i];
                    var caption = (img.caption) ? img.caption.text : '';
                    var url = img.link;
                    var thumb   = {
                        src: img.images.thumbnail.url,
                        width: img.images.thumbnail.width,
                        height: img.images.thumbnail.height,
                    };
                    var medium   = {
                        src: img.images.low_resolution.url,
                        width: img.images.low_resolution.width,
                        height: img.images.low_resolution.height,
                    };
                    var large   = {
                        src: img.images.standard_resolution.url,
                        width: img.images.standard_resolution.width,
                        height: img.images.standard_resolution.height,
                    };
                    var like = img.likes.count;
                    var comment = img.comments.count;
                    var markup = '<a class="swiper-slide ig-post" target="_blank" href="'+url+'">'+
                        '<div class="ig-post-inner">'+
                            '<div class="ig-post-img">'+
                                '<img class="img-cover" src="'+large.src+'" alt="Pai Village" width="'+large.width+'" height="'+large.height+'">'+
                            '</div>'+
                            '<div class="ig-post-item">'+
                                '<ul class="ig-post-icons">'+
                                    '<li><i class="fas fa-heart" aria-hidden="true"></i> '+like+'</li>'+
                                    '<li><i class="fas fa-comments" aria-hidden="true"></i> '+comment+'</li>'+
                                '</ul>'+
                                '<span class="ig-post-tagline">'+caption+'</span>'+
                            '</div>'+
                        '</div>'+
                    '</a>';
                    $el.append(markup);
                }
            });
            
            data.done(function() {
                configs.complete();
            });
        });
    }

})(jQuery, window);
