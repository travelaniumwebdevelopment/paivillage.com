(function($) {
  let cookieBanner = {
    el: document.querySelector('.pv-cookie-bar'),
    show(cb) {
      this.el.classList.add('show');
      if ('function' === typeof cb) cb();
    },
    hide(cb) {
      this.el.classList.remove('show');
      if ('function' === typeof cb) cb();
    },
    accept(cb) {
      this.hide();
      Cookies.set('pv-cookie-accept', '1', {
        expires: 30,
      });
      if ('function' === typeof cb) cb();
    },
    decline(cb) {
      Cookies.set('pv-cookie-accept', '0', {
        expires: 30,
      });
      if ('function' === typeof cb) cb();
    },
    confirmed() {
      return Cookies.get('pv-cookie-accept');
    },
    reset(cb) {
      Cookies.remove('pv-cookie-accept');
      Cookies.remove('pv-cookie-version')
      if ('function' === typeof cb) cb();
    },
    getVersion() {
      return Cookies.get('pv-cookie-version');
    },
    setVersion(version) {
      return Cookies.set('pv-cookie-version', version);
    },
  }

  if (!window.PV) {
    window.PV = {}
  }

  window.PV.CookieBanner = cookieBanner;
})(jQuery);