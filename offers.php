<?php
$html_class = '';
$body_class = 'page-offers';
$cur_page   = 'offers';
$par_page   = '';
$title      = 'Pai Village Boutique Resort and Farm | Pai Deals and Promotions';
$desc       = 'Exclusive Offers available on Official Site ONLY. Best Rates Guaranteed. Member Deal. Private Villa and Cottage, located along the banks of Pai River featuring Rustic Bamboo Cottage set among widing pathways.';
$keyw       = 'Rasa,Hospitality,Rasa Tower,Management,Development,Business Management,Business Development,Accounting,Manager,Hotel,Resort,Investment,,Buri Rasa,Rasa Collection,Boutique,Traditional, Pai, Mae Hong Son,Thailand';
$local      = 'en-US';
$page_url   = 'offers.php';

$lang_en    = $page_url;
$lang_zh    = 'zh/'. $page_url;
$lang_th    = 'th/'. $page_url;
include_once '_header.php' ?>
        <main class="site-main">
            <div class="swiper-container swiper-main">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img class="img-cover lazy-load" alt="Pai Village" data-src="./images/gallery/highlight/main-pic-02.jpg">
                    </div>
                </div>
                <?php include 'inc/scroll-down.php'; ?>
            </div>
            <section id="section" class="intro">
                <div class="bg-texture mountain">
                    <div class="container py-5">
                        <h1 class="header text-center mb-3 mb-md-5" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="100">Special Offers</h1>
                        <div class="row py-md-12 text-center">
                            <!-- <div class="col-12 col-md-12 py-3" data-aos="fade-right" data-aos-offset="100" data-aos-duration="500" data-aos-delay="100">
                                <div class="offers-card">
                                    <img class="img-cover" src="./images/offers/welcome-back-4.jpg" alt="Pai Village" width="600" height="954">
                                    <div class="main">
                                        <h2 class="title">Welcome Back Promotion</h2>
                                        <p class="desc">Have Yourself a staycation with a sanctuary away from the hustle, right in the heart of Pai Town!<br>The choice is yours. Select from any of the 3 benefits to enhance your stay at Pai Village Boutique.</p>
                                        <a class="btn btn-book" target="_blank" href="https://reservation.travelanium.net/propertyibe2/rates?propertyId=253&onlineId=4">Book Now</a>
                                    </div>
                                </div>
                            </div> -->
                            <!-- <div class="col-12 col-md-4 py-3" data-aos="fade-right" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">
                                <div class="offers-card">
                                    <img class="img-cover" src="./images/offers/rao-teaw-duay-kan-5.jpg" alt="Pai Village" width="403" height="257">
                                    <div class="main">
                                        <h2 class="title">ปาย วิลเลจ บูติก รีสอร์ท x เราเที่ยวด้วยกัน เฟส 5</h2>
                                        <p class="desc">สำรองห้องพัก ตั้งแต่ 7 มี.ค. 66 – 26 เม.ย. 66</p>
                                        <a class="btn btn-book" target="_blank" href="https://reservation.travelanium.net/propertyibe2/rates?propertyId=253&onlineId=4&checkinauto=3&numofnight=1&numofadult=2&numofchild=0&numofroom=1&pid=MDc0NzM2Nw%3D%3D&rid=ad1c12fb0ab02c16534d4293aa4f7379605b409e9a9ebbb2df1201a6d34f0eb8aad44f3195cb947fd33b41e9a077e004">Book Now</a>
                                    </div>
                                </div>
                            </div> -->
                            <div class="col-12 col-md-4 py-3" data-aos="fade-right" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">
                                <div class="offers-card">
                                    <img class="img-cover" src="./images/offers/Heritage-Discovery.jpg" alt="Pai Village" width="403" height="257">
                                    <div class="main">
                                        <h2 class="title">HERITAGE DISCOVERY</h2>
                                        <p class="desc">Discover unique experiences and get even more value during your stay at Pai Village Boutique</p>
                                        <a class="btn btn-book" target="_blank" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=253&onlineId=4&pid=MDg2OTAz">Book Now</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 py-3" data-aos="fade-right" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <div class="offers-card">
                                    <img class="img-cover" src="./images/offers/Super-Saving.jpg" alt="Pai Village" width="403" height="257">
                                    <div class="main">
                                        <h2 class="title">STAY LONGER SAVE MORE</h2>
                                        <p class="desc">Get away and take advantage of our super savings on room rate, daily breakfast and more</p>
                                        <a class="btn btn-book" target="_blank" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=253&onlineId=4&pid=MDczOTM1Mw%3D%3D">Book Now</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 py-3" data-aos="fade-right" data-aos-offset="100" data-aos-duration="500" data-aos-delay="250">
                                <div class="offers-card">
                                    <img class="img-cover" src="./images/offers/Best-Flexible-Rate.jpg" alt="Pai Village" width="403" height="257">
                                    <div class="main">
                                        <h2 class="title">BEST FLEXIBLE RATES</h2>
                                        <p class="desc">Save on our ‘Best Flexible Rates’ when you book in advance</p>
                                        <a class="btn btn-book" target="_blank" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=253&onlineId=4&pid=MDcyMjkyMw%3D%3D">Book Now</a>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-12 col-md-4 py-3" data-aos="fade-right" data-aos-offset="100" data-aos-duration="500" data-aos-delay="300">
                                <div class="offers-card">
                                    <img class="img-cover" src="https://images.travelanium.net/crs-file-manager/images/roompromotion?propertyid=253&group=23&width=450&height=300&imageid=15166&type=jpg" alt="Pai Village" width="403" height="257">
                                    <div class="main">
                                        <h2 class="title">60 Days Advance Purchase</h2>
                                        <p class="desc">Stay before 20 Oct 2020</p>
                                        <a class="btn btn-book" target="_blank" href="<?php echo ibe_url(get_info('ibeID'), get_info('site_lang'), 'MDcyMjkxOQ%3D%3D'); ?>">Book Now</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 py-3" data-aos="fade-right" data-aos-offset="100" data-aos-duration="500" data-aos-delay="300">
                                <div class="offers-card">
                                    <img class="img-cover" src="https://images.travelanium.net/crs-file-manager/images/roompromotion?propertyid=253&group=23&width=450&height=300&imageid=15167&type=jpg" alt="Pai Village" width="403" height="257">
                                    <div class="main">
                                        <h2 class="title">Today Special Rate</h2>
                                        <p class="desc">Stay before 20 Oct 2020</p>
                                        <a class="btn btn-book" target="_blank" href="<?php echo ibe_url(get_info('ibeID'), get_info('site_lang'), 'MDcxMjk0MA%3D%3D'); ?>">Book Now</a>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <?php include_once '_footer.php'; ?>