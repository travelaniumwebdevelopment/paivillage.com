        <footer class="site-footer">
            <div class="container">
                <div class="row py-3">
                    <div class="col-12 col-lg-12 py-4 pb-lg-5 text-center">
                        <h4 class="title text-uppercase">Don’t miss our secret offer!</h4>
                        <p class="word">Available when booking direct via the hotel website.</p>
                        <form class="form-inline" id="subscribe_form" method="POST" action="/">
                            <div class="result"></div>
                            <div class="form-wrap">
                                <input name="s-mail" class="input-text form-control" type="email" placeholder="Your email address"  required>
                                <button name="submit" class="input-submit form-button" type="submit">Subscribe</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-12 col-lg py-3">
                        <div class="social text-center">
                            <div class="bubble"><a class="yt" target="_blank" href="<?php echo get_info('youtube'); ?>"><i class="icon fab fa-youtube" aria-hidden="true"></i></a></div>
                            <div class="bubble"><a class="ig" target="_blank" href="<?php echo get_info('instagram'); ?>"><i class="icon fab fa-instagram" aria-hidden="true"></i></a></div>
                            <div class="bubble"><a class="fb" target="_blank" href="<?php echo get_info('facebook'); ?>"><i class="icon fab fa-facebook-f" aria-hidden="true"></i></a></div>
                            <div class="bubble"><a class="ta" target="_blank" href="<?php echo get_info('tripadvisor'); ?>"><i class="icon fab fa-tripadvisor" aria-hidden="true"></i></a></div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-2 py-3">
                        <div class="d-flex align-items-top">
                            <img class="filters" src="./assets/elements/logo-rasa.png" alt="Pai Village" width="26" height="60">
                            <div class="content">
                                <span class="title">Manage by</span>
                                <span class="detail"><a href="https://www.rasahospitality.com/">Rasa Hospitality</a></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg py-3">
                        <div class="content d-block mb-3">
                            <span class="title">Telephone</span>
                            <span class="detail"><a href="tel:+6653698152">+66 (0) 5369 8152</a></span>
                            <span class="detail"><a href="tel:+66818277047">+66 (0) 81827 7047</a></span>
                        </div>
                        <div class="content d-block">
                            <span class="title">Email</span>
                            <span class="detail"><a href="mailto:<?php echo get_info('email'); ?>"><?php echo get_info('email'); ?></a></span>
                        </div>
                    </div>
                    <div class="col-12 col-lg py-3">
                        <div class="d-flex align-items-top">
                            <div class="content">
                                <span class="title">Address</span>
                                <span class="detail"><a target="_blank" href="<?php echo get_info('map_url'); ?>"><?php echo get_info('address'); ?></a></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-2 py-3">
                        <div class="d-flex align-items-top">
                            <div class="content">
                                <span class="title">Media Center</span>
                                <span class="detail"><a target="_blank" href="https://www.rasahospitality.com/awards.php#collapseThree">Awards</a></span>
                                <!-- <span class="detail"><a target="_blank" href="./downloads/PaiFactSheet.pdf">Factsheet</a></span> -->
                                <span class="detail"><a target="_blank" href="mailto:pr@rasahospitality.com">Media Contacts</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="site-copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <p class="tl-m"><i class="fal fa-copyright" aria-hidden="true"></i> <?php echo get_info('site_name'); ?> 2019. All rights reserved.	</p>
                        </div>            
                        <div class="col-12 col-md-6">
                            <p class="tr-m mb-md-0">Hotel Web Design by <a href="https://www.travelanium.com" target="_blank" title="travelanium">Travelanium</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div id="back-top"><a href="#page" class="arrows bounce"><i class="fas fa-chevron-up" aria-hidden="true"></i></a></div>
        <div class="m-site-menu-bg" aria-hidden="true" data-mobile-menu="hide"></div>
        <div class="m-site-menu" data-simplebar>
            <div class="m-site-menu-wrapper">
                <div class="m-site-menu-logo"><img class="lazy-load" data-src="./assets/elements/logo.png" alt="Pai Village" width="82" height="64"></div>
                <nav class="menu-primary">
                    <ul class="menu-primary-list">
                        <?php include 'inc/main-menu.php'; ?>
                    </ul>
                </nav>
                <div class="divider"></div>
                <div class="block-html" id="language">
                    <h3 class="heading">Languages</h3>
                    <div class="content">
                        <ul class="list-inline">
                            <li class="list-inline-item <?php echo get_current_lang('en'); ?>"><a href="<?php echo $lang_en ?>">EN</a></li>
                            <li class="list-inline-item <?php echo get_current_lang('zh'); ?>"><a href="<?php echo $lang_zh ?>">中文</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <?php include_once 'inc/booking.php'; ?>
    </div>
    <?php include_once 'inc/member.php'; ?>

    <?php
    $privacy_link_url = get_base_uri() . '/privacy-policy.php';
    $privacy_link_text = 'Privacy Policy';
    ?>
    <div class="pv-cookie-bar">
        <div class="pv-cookie-bar__container">
            <div class="pv-cookie-bar__text">
                <?php echo sprintf('We may use cookies on this site to enhance your browsing experience and for marketing objectives. For more information on our use of cookies, please visit our <a href="%2$s" target="_blank">%1$s</a>.', $privacy_link_text, $privacy_link_url) ?>
            </div>
            <div class="pv-cookie-bar__cta">
                <button class="button button-accept" onclick="cookieAccept();return false;">Accept</button>
            </div>
        </div>
    </div>
    <script>
        var COOKIE_VERSION = '20220527';
        document.addEventListener('DOMContentLoaded', function() {
            if (!window.PV.CookieBanner.confirmed() || COOKIE_VERSION !== window.PV.CookieBanner.getVersion()) {
                window.PV.CookieBanner.show();
            }
        });
        document.addEventListener('DOMContentLoaded', function() {
            window.PV.CookieBanner.el.classList.add('ready');
        });
        function cookieAccept() {
            window.PV.CookieBanner.accept(function() {
                window.PV.CookieBanner.setVersion(COOKIE_VERSION);
            });
        }
    </script>

    <?php include_once 'inc/scripts.php'; ?>
</body>
</html>