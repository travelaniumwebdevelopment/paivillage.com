<?php
$html_class = '';
$body_class = 'page-dining';
$cur_page   = 'rabbit-cafe';
$par_page   = 'dining';
$title      = 'Pai Village Boutique Resort & Farm | Rabbit Cafe';
$desc       = 'Exclusive Offers available on Official Site ONLY. Best Rates Guaranteed. Member Deal. Private Villa and Cottage, located along the banks of Pai River featuring Rustic Bamboo Cottage set among widing pathways.';
$keyw       = 'Rasa,Hospitality,Rasa Tower,Management,Development,Business Management,Business Development,Accounting,Manager,Hotel,Resort,Investment,,Buri Rasa,Rasa Collection,Boutique,Traditional, Pai, Mae Hong Son,Thailand';
$local      = 'en-US';
$page_url   = 'rabbit-cafe.php';
$ogimage    = ['images/dining/rabbit-cafe/main-slide-01.jpg', '1500', '1000'];

$lang_en    = $page_url;
$lang_zh    = 'zh/'. $page_url;
$lang_th    = 'th/'. $page_url;
include_once '_header.php' ?>
        <main class="site-main">
            <div class="swiper-container swiper-main">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img class="img-cover lazy-load" alt="Rabbit Café" data-src="images/dining/rabbit-cafe/main-pic-01.jpg">
                    </div>
                </div>
                <?php include 'inc/scroll-down.php'; ?>
            </div>
            <section id="section" class="intro">
                <div class="bg-texture mountain">
                    <div class="container py-5">
                        <h1 class="header text-center mb-3 mb-md-5" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="100"><?php echo _dining_name_2; ?></h1>
                        <div class="row py-4">
                            <div class="col-12 col-md-9 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">Rabbit Café is a new style café which allows guests to enjoy a wide variety of beverages, fresh bakery and our lovable rabbits from Pai Village Farm! </p>
                                <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">Located at Pai Village Boutique Resort, Rabbit Café is an artisan bakery selling fresh, premium breads, pastries and sweets using farm to table organics where are possible. A variety of coffee drinks, signature shaken teas, fruit smoothies and handmade snacks as well as a mulberry soft serve ice cream, which is made fresh with organically grown mulberries are also on offer. All are enhanced by the sheer joy of our celebrity rabbits who are on site daily for your peaceful enjoyment.</p>
                            </div>
                            <div class="col-12 col-md-3 py-3 d-flex justify-content-center align-items-start" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <div class="box-information">
                                    <span class="d-block mb-2">OPENING HOURS</span>
                                    <span class="d-block main-color">**Temporarily Closed**  <!-- 08.00 AM - 04.00 PM --></span>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-slider" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">
                            <h2 class="subject mb-4">Gallery :</h2>
                            <div class="swiper-container swiper-gallery">
                                <div class="swiper-wrapper gallery-popup">
                                    <a class="swiper-slide" href="./images/dining/rabbit-cafe/main-pic-01.jpg"><img src="./images/dining/rabbit-cafe/thumb/main-pic-01.jpg" alt="best café Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/dining/rabbit-cafe/main-pic-02.jpg"><img src="./images/dining/rabbit-cafe/thumb/main-pic-02.jpg" alt="Pai best café" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/dining/rabbit-cafe/main-pic-03.jpg"><img src="./images/dining/rabbit-cafe/thumb/main-pic-03.jpg" alt="Pai best café" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/dining/rabbit-cafe/main-pic-04.jpg"><img src="./images/dining/rabbit-cafe/thumb/main-pic-04.jpg" alt="Pai best café" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/dining/rabbit-cafe/main-pic-05.jpg"><img src="./images/dining/rabbit-cafe/thumb/main-pic-05.jpg" alt="Pai coffee shops" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/dining/rabbit-cafe/main-pic-06.jpg"><img src="./images/dining/rabbit-cafe/thumb/main-pic-06.jpg" alt="café in Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/dining/rabbit-cafe/main-pic-07.jpg"><img src="./images/dining/rabbit-cafe/thumb/main-pic-07.jpg" alt="bakery café Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/dining/rabbit-cafe/main-pic-08.jpg"><img src="./images/dining/rabbit-cafe/thumb/main-pic-08.jpg" alt="best coffee shops and café Pai" width="1500" height="843"></a>
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <?php include_once '_footer.php'; ?>