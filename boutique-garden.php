<?php
$html_class = '';
$body_class = 'page-accommodation';
$cur_page   = 'deluxe-garden';
$par_page   = 'accommodation';
$title      = 'Boutique Garden - Pai Village Boutique Resort & Farm';
$desc       = 'Exclusive Offers available on Official Site ONLY. Best Rates Guaranteed. Member Deal. Private Villa and Cottage, located along the banks of Pai River featuring Rustic Bamboo Cottage set among widing pathways.';
$keyw       = 'Rasa,Hospitality,Rasa Tower,Management,Development,Business Management,Business Development,Accounting,Manager,Hotel,Resort,Investment,,Buri Rasa,Rasa Collection,Boutique,Traditional, Pai, Mae Hong Son,Thailand';
$local      = 'en-US';
$page_url   = 'boutique-garden.php';
$ogimage    = ['images/accommodation/deluxe-garden/09.jpg', '1500', '1000'];

$lang_en    = $page_url;
$lang_zh    = 'zh/'. $page_url;
$lang_th    = 'th/'. $page_url;
include_once '_header.php' ?>
        <main class="site-main">
            <div class="swiper-container swiper-main">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img class="img-cover lazy-load" alt="homestay in Pai" data-src="images/accommodation/boutique-garden/boutique-garden-2.jpg">
                    </div>
                </div>
                <?php include 'inc/scroll-down.php'; ?>
            </div>
            <section id="section" class="intro">
                <div class="bg-texture mountain">
                    <div class="container py-5">
                        <h1 class="header text-center mb-3 mb-md-5" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="100"><?php echo _room_name_3; ?></h1>
                        <p class="intro-desc mb-0" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">In a world of all their own surrounded by lush green scenery, our Boutique Garden Rooms offer maximum privacy in a relaxed garden setting. </p>
                        <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">With more than 19 square meters of indoor/outdoor living space, you will enjoy a queen size bed and the pleasure of your own hammock right on your balcony . The maximum occupancy of 2 guests</p>
                        <div class="row py-4">
                            <div class="col-12 col-md-4 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <h2 class="subject"><b>Amenities</b></h2>
                                <ul class="pl-0">
                                    <li>Air Conditioning</li>
                                    <li>Bath Amenities</li>
                                    <li>Coffee & Tea Making Facilities</li>
                                    <li>Private Balcony or Terrace</li>
                                    <li>Complimentary Refreshment</li>
                                </ul>
                            </div>
                            <div class="col-12 col-md-4 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <h2 class="subject"><b>Services</b></h2>
                                <ul class="pl-0">
                                    <li>Turn Down Service</li>
                                    <li>Daily Housekeeping</li>
                                </ul>
                            </div>
                            <div class="col-12 col-md-4 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <h2 class="subject"><b>Information</b></h2>
                                <ul class="pl-0">
                                    <li>Indoor Space <?php echo _room_space_3; ?> Sq.m</li>
                                    <li>Allow up to <?php echo _room_guest_3; ?> guests</li>
                                </ul>
                            </div>
                            <div class="col-12 py-3 text-center" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="250">
                                <a class="btn btn-radius-main-color" href="<?php echo ibe_url(get_info('ibeID'), get_info('site_lang')); ?>">BOOK NOW</a>
                            </div>
                        </div>
                        <div class="gallery-slider" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">
                            <h2 class="subject mb-4">Gallery :</h2>
                            <div class="swiper-container swiper-gallery">
                                <div class="swiper-wrapper gallery-popup">
                                    
                                    <a class="swiper-slide" href="./images/accommodation/boutique-garden/boutique-garden-1.jpg"><img src="./images/accommodation/boutique-garden/boutique-garden-1.jpg" alt="homestay in Pai" width="1500" height="843"></a>

                                    <a class="swiper-slide" href="./images/accommodation/boutique-garden/boutique-garden-2.jpg"><img src="./images/accommodation/boutique-garden/boutique-garden-2.jpg" alt="cottage homestay Pai" width="1500" height="843"></a>

                                    <a class="swiper-slide" href="./images/accommodation/boutique-garden/boutique-garden-3.jpg"><img src="./images/accommodation/boutique-garden/boutique-garden-3.jpg" alt="best homestay Pai" width="1500" height="843"></a>

                                    <a class="swiper-slide" href="./images/accommodation/boutique-garden/boutique-garden-4.jpg"><img src="./images/accommodation/boutique-garden/boutique-garden-4.jpg" alt="Pai hotel resort" width="1500" height="843"></a>

                                    <a class="swiper-slide" href="./images/accommodation/boutique-garden/boutique-garden-5.jpg"><img src="./images/accommodation/boutique-garden/boutique-garden-5.jpg" alt="Pai hotel resort" width="1500" height="843"></a>

                                    <a class="swiper-slide" href="./images/accommodation/boutique-garden/boutique-garden-6.jpg"><img src="./images/accommodation/boutique-garden/boutique-garden-6.jpg" alt="Pai hotel resort" width="1500" height="843"></a>
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <?php include_once '_footer.php'; ?>