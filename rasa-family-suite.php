<?php
$html_class = '';
$body_class = 'page-accommodation';
$cur_page   = 'rasa-family-suite';
$par_page   = 'accommodation';
$title      = 'Rasa Family Suite - Pai Village Boutique Resort & Farm';
$desc       = 'Exclusive Offers available on Official Site ONLY. Best Rates Guaranteed. Member Deal. Private Villa and Cottage, located along the banks of Pai River featuring Rustic Bamboo Cottage set among widing pathways.';
$keyw       = 'Rasa,Hospitality,Rasa Tower,Management,Development,Business Management,Business Development,Accounting,Manager,Hotel,Resort,Investment,,Buri Rasa,Rasa Collection,Boutique,Traditional, Pai, Mae Hong Son,Thailand';
$local      = 'en-US';
$page_url   = 'rasa-family-suite.php';
$ogimage    = ['images/accommodation/rasa-family-suite/01.jpg', '1500', '1000'];

$lang_en    = $page_url;
$lang_zh    = 'zh/'. $page_url;
$lang_th    = 'th/'. $page_url;
include_once '_header.php' ?>
        <main class="site-main">
            <div class="swiper-container swiper-main">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img class="img-cover lazy-load" alt="best resort Pai" data-src="images/accommodation/rasa-family-suite/main-pic-01.jpg">
                    </div>
                </div>
                <?php include 'inc/scroll-down.php'; ?>
            </div>
            <section id="section" class="intro">
                <div class="bg-texture mountain">
                    <div class="container py-5">
                        <h1 class="header text-center mb-3 mb-md-5" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="100"><?php echo _room_name_1; ?></h1>
                        <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">Boasting more than 100 square meters of space with the ability to accommodate up to four guests, our one and only Rasa Family Suite is set high above the restaurant overlooking the resort and the local area. With outdoor space for private dining and a Sala perfect curl up in with friends or family, the Rasa Family Suite is the perfect choice for those who want to escape, yet be in the heart of it all. The Suite features a king size bed and private access via its own staircase. Due to the location of this suite near the front of the resort and nearby nightlife, there may or may not be noise from local establishments until late at night.</p>
                        <div class="row py-4">
                            <div class="col-12 col-md-4 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <h2 class="subject"><b>Amenities</b></h2>
                                <ul class="pl-0">
                                    <li>Air Conditioning</li>
                                    <li>A Cable Flat Screen TV</li>
                                    <li>Bathrobe and Bath Amenities</li>
                                    <li>Coffee & Tea Making Facilities</li>
                                    <li>Private Balcony or Terrace</li>
                                    <li>Complimentary Refreshment</li>
                                </ul>
                            </div>
                            <div class="col-12 col-md-4 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <h2 class="subject"><b>Services</b></h2>
                                <ul class="pl-0">
                                    <li>Turn Down Service</li>
                                    <li>Daily Housekeeping</li>
                                </ul>
                            </div>
                            <div class="col-12 col-md-4 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <h2 class="subject"><b>Information</b></h2>
                                <ul class="pl-0">
                                    <li>Indoor Space <?php echo _room_space_1; ?> Sq.m</li>
                                    <li>Allow up to <?php echo _room_guest_1; ?> guests</li>
                                </ul>
                            </div>
                            <div class="col-12 py-3 text-center" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="250">
                                <a class="btn btn-radius-main-color" href="<?php echo ibe_url(get_info('ibeID'), get_info('site_lang')); ?>">BOOK NOW</a>
                            </div>
                        </div>
                        <div class="gallery-slider" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">
                            <h2 class="subject mb-4">Gallery :</h2>
                            <div class="swiper-container swiper-gallery">
                                <div class="swiper-wrapper gallery-popup">
                                    <a class="swiper-slide" href="./images/accommodation/rasa-family-suite/main-pic-01.jpg"><img src="./images/accommodation/rasa-family-suite/thumb/main-pic-01.jpg" alt="Pai hotel booking" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/accommodation/rasa-family-suite/main-pic-02.jpg"><img src="./images/accommodation/rasa-family-suite/thumb/main-pic-02.jpg" alt="best resort Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/accommodation/rasa-family-suite/main-pic-03.jpg"><img src="./images/accommodation/rasa-family-suite/thumb/main-pic-03.jpg" alt="homestay Mae Hong Son" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/accommodation/rasa-family-suite/main-pic-04.jpg"><img src="./images/accommodation/rasa-family-suite/thumb/main-pic-04.jpg" alt="Mae Hong Son hotels" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/accommodation/rasa-family-suite/main-pic-05.jpg"><img src="./images/accommodation/rasa-family-suite/thumb/main-pic-05.jpg" alt="homestay Pai Thailand" width="1500" height="843"></a>
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <?php include_once '_footer.php'; ?>