<?php
$html_class = '';
$body_class = 'page-accommodation';
$cur_page   = 'deluxe-villa';
$par_page   = 'accommodation';
$title      = 'Deluxe Villa - Pai Village Boutique Resort & Farm';
$desc       = 'Exclusive Offers available on Official Site ONLY. Best Rates Guaranteed. Member Deal. Private Villa and Cottage, located along the banks of Pai River featuring Rustic Bamboo Cottage set among widing pathways.';
$keyw       = 'Rasa,Hospitality,Rasa Tower,Management,Development,Business Management,Business Development,Accounting,Manager,Hotel,Resort,Investment,,Buri Rasa,Rasa Collection,Boutique,Traditional, Pai, Mae Hong Son,Thailand';
$local      = 'en-US';
$page_url   = 'deluxe-villa.php';
$ogimage    = ['images/accommodation/deluxe-villa/06.jpg', '1500', '1000'];

$lang_en    = $page_url;
$lang_zh    = 'zh/'. $page_url;
$lang_th    = 'th/'. $page_url;
include_once '_header.php' ?>
        <main class="site-main">
            <div class="swiper-container swiper-main">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img class="img-cover lazy-load" alt="sustainable hotels Mae Hong Son" data-src="images/accommodation/deluxe-villa/06.jpg">
                    </div>
                </div>
                <?php include 'inc/scroll-down.php'; ?>
            </div>
            <section id="section" class="intro">
                <div class="bg-texture mountain">
                    <div class="container py-5">
                        <h1 class="header text-center mb-3 mb-md-5" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="100"><?php echo _room_name_4; ?></h1>
                        <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">Our Rasa Villas incorporate all the best of Pai Village while taking inspiration from our sister resorts in Koh Samui and Koh Phangan, Buri Rasa, yet staying true to the northern charm of Pai. </p>
                        <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">You can relax in style with more than 65 square meters of private luxury featuring your own garden, streaming television and a large bathroom area. Our villas feature VIP service and offer a king size bed with room for an extra bed, accommodating up to four guests. Enjoy luxury in a local, yet refined style.</p>
                        <div class="row py-4">
                            <div class="col-12 col-md-4 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <h2 class="subject"><b>Amenities</b></h2>
                                <ul class="pl-0">
                                    <li>Air Condition</li>
                                    <li>A Cable Flat Screen TV</li>
                                    <li>Bathrobe and Bath Amenities</li>
                                    <li>Coffee & Tea Making Facilities</li>
                                    <li>Private Balcony or Terrace</li>
                                    <li>Complimentary Refreshment</li>
                                </ul>
                            </div>
                            <div class="col-12 col-md-4 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <h2 class="subject"><b>Services</b></h2>
                                <ul class="pl-0">
                                    <li>Turn Down Service</li>
                                    <li>Daily Housekeeping</li>
                                </ul>
                            </div>
                            <div class="col-12 col-md-4 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <h2 class="subject"><b>Information</b></h2>
                                <ul class="pl-0">
                                    <li>Indoor Space <?php echo _room_space_4; ?> Sq.m</li>
                                    <li>Allow up to <?php echo _room_guest_4; ?> guests</li>
                                </ul>
                            </div>
                            <div class="col-12 py-3 text-center" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="250">
                                <a class="btn btn-radius-main-color" href="<?php echo ibe_url(get_info('ibeID'), get_info('site_lang')); ?>">BOOK NOW</a>
                            </div>
                        </div>
                        <div class="gallery-slider" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">
                            <h2 class="subject mb-4">Gallery :</h2>
                            <div class="swiper-container swiper-gallery">
                                <div class="swiper-wrapper gallery-popup">
                                    <a class="swiper-slide" href="./images/accommodation/deluxe-villa/06.jpg"><img src="./images/accommodation/deluxe-villa/thumb/06.jpg" alt="sustainable hotels Mae Hong Son" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/accommodation/deluxe-villa/07.jpg"><img src="./images/accommodation/deluxe-villa/thumb/07.jpg" alt="Pai best hotels" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/accommodation/deluxe-villa/08.jpg"><img src="./images/accommodation/deluxe-villa/thumb/08.jpg" alt="Pai good view resort" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/accommodation/deluxe-villa/09.jpg"><img src="./images/accommodation/deluxe-villa/thumb/09.jpg" alt="Pai unique resort" width="1500" height="843"></a>
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <?php include_once '_footer.php'; ?>