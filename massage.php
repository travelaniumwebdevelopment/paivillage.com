<?php
$html_class = '';
$body_class = 'page-massage';
$cur_page   = 'massage';
$par_page   = '';
$title      = 'Pai Village Boutique Resort & Farm | Traditional Thai Spa';
$desc       = 'Exclusive Offers available on Official Site ONLY. Best Rates Guaranteed. Member Deal. Private Villa and Cottage, located along the banks of Pai River featuring Rustic Bamboo Cottage set among widing pathways.';
$keyw       = 'Rasa,Hospitality,Rasa Tower,Management,Development,Business Management,Business Development,Accounting,Manager,Hotel,Resort,Investment,,Buri Rasa,Rasa Collection,Boutique,Traditional, Pai, Mae Hong Son,Thailand';
$local      = 'en-US';
$page_url   = 'massage.php';
$ogimage    = ['images/massage/10.jpg', '1500', '1000'];

$lang_en    = $page_url;
$lang_zh    = 'zh/'. $page_url;
$lang_th    = 'th/'. $page_url;
include_once '_header.php' ?>
        <main class="site-main">
            <div class="swiper-container swiper-main">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img class="img-cover lazy-load" alt="best massage Pai" data-src="images/massage/10.jpg">
                    </div>
                </div>
                <?php include 'inc/scroll-down.php'; ?>
            </div>
            <section id="section" class="intro">
                <div class="bg-texture mountain">
                    <div class="container py-5">
                        <h1 class="header text-center mb-3 mb-md-5" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="100">Massage</h1>
                        <div class="row py-4">
                            <div class="col-12 col-md-9 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">Pai Village Boutique Resort & Farm is in collaboration with local legend ‘Mama Lon’ who was the first person in Mae Hong Son province which practice traditional Thai massage.</p>
                                <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150"> To ensure the highest quality, Mama Lon grows her own herbs organically for the amazing herbal Thai compress massage. The spa offers both open-air & air conditioned services.</p>
                            </div>
                            <div class="col-12 col-md-3 py-3 d-flex justify-content-center align-items-start" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <div class="box-information">
                                    <span class="d-block mb-2">OPENING HOURS</span>
                                    <span class="d-block main-color">09.00 AM - 8.00 PM</span>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-slider" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">
                            <h2 class="subject mb-4">Gallery :</h2>
                            <div class="swiper-container swiper-gallery">
                                <div class="swiper-wrapper gallery-popup">
                                    <!-- <a class="swiper-slide" href="./images/massage/04.jpg"><img src="./images/massage/thumb/04.jpg" alt="best massage Pai" width="1500" height="843"></a> -->
                                    <a class="swiper-slide" href="./images/massage/05.jpg"><img src="./images/massage/thumb/05.jpg" alt="Pai best massage" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/massage/06.jpg"><img src="./images/massage/thumb/06.jpg" alt="traditional Thai massage Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/massage/07.jpg"><img src="./images/massage/thumb/07.jpg" alt="Spa hotel Pai" width="1500" height="843"></a>
                                    <!-- <a class="swiper-slide" href="./images/massage/08.jpg"><img src="./images/massage/thumb/08.jpg" alt="traditional Thai massage Mae Hong Son" width="1500" height="843"></a> -->
                                    <a class="swiper-slide" href="./images/massage/01.jpg"><img src="./images/massage/thumb/01.jpg" alt="spa resort Pai" width="1500" height="843"></a>
                                    <!-- <a class="swiper-slide" href="./images/massage/02.jpg"><img src="./images/massage/thumb/02.jpg" alt="Pai Village" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/massage/03.jpg"><img src="./images/massage/thumb/03.jpg" alt="Pai Village" width="1500" height="843"></a> -->
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <?php include_once '_footer.php'; ?>