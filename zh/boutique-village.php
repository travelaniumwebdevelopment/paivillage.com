<?php
$html_class = '';
$body_class = 'page-accommodation';
$cur_page   = 'deluxe-village';
$par_page   = 'accommodation';
$title      = 'Boutique Village - Pai Village Boutique Resort & Farm';
$desc       = 'Exclusive Offers available on Official Site ONLY. Best Rates Guaranteed. Member Deal. Private Villa and Cottage, located along the banks of Pai River featuring Rustic Bamboo Cottage set among widing pathways.';
$keyw       = 'Rasa,Hospitality,Rasa Tower,Management,Development,Business Management,Business Development,Accounting,Manager,Hotel,Resort,Investment,,Buri Rasa,Rasa Collection,Boutique,Traditional, Pai, Mae Hong Son,Thailand';
$local      = 'en-US';
$page_url   = 'boutique-village.php';
$ogimage    = ['../images/accommodation/boutique-village/main-pic-05.jpg', '1500', '1000'];

$lang_zh    = $page_url;
$lang_en    = '../'. $page_url;
$lang_th    = 'th/'. $page_url;
include_once '_header.php' ?>
        <main class="site-main">
            <div class="swiper-container swiper-main">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img class="img-cover lazy-load" alt="Pai Village" data-src="../images/accommodation/boutique-village/main-pic-05.jpg">
                    </div>
                </div>
                <?php include 'inc/scroll-down.php'; ?>
            </div>
            <section id="section" class="intro">
                <div class="bg-texture mountain">
                    <div class="container py-5">
                        <h1 class="header text-center mb-3 mb-md-5" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="100"><?php echo _room_name_2; ?></h1>
                        <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">豪华乡村房拥有35平方米的居住空间，且坐落于度假村中心区域，就如其名字一般，聚集成一个小村落，提供给您与其他客人交流的机会。这22套豪华乡村房配有书桌，宽广的露台与靠枕。房间内设有特大号双人床，并有一间可放置容纳3人的加床房。</p>
                        <div class="row py-4">
                            <div class="col-12 col-md-4 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <h2 class="subject"><b>设施和设施</b></h2>
                                <ul class="pl-0">
                                    <li>空调</li>
                                    <li>浴袍和浴室设施</li>
                                    <li>咖啡/沏茶设施</li>
                                    <li>私人阳台</li>
                                    <li>迷你酒</li>
                                </ul>
                            </div>
                            <div class="col-12 col-md-4 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <h2 class="subject"><b>维修服务</b></h2>
                                <ul class="pl-0">
                                    <li>夜床服务</li>
                                    <li>每日清洁服务</li>
                                </ul>
                            </div>
                            <div class="col-12 col-md-4 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <h2 class="subject"><b>信息</b></h2>
                                <ul class="pl-0">
                                    <li><?php echo _room_space_2; ?> 平方米室内生活空间</li>
                                    <li>最大 <?php echo _room_guest_2; ?> 名成人</li>
                                </ul>
                            </div>
                            <div class="col-12 py-3 text-center" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="250">
                                <a class="btn btn-radius-main-color" href="<?php echo ibe_url(get_info('ibeID'), get_info('site_lang')); ?>">现在预订</a>
                            </div>
                        </div>
                        <div class="gallery-slider" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">
                            <h2 class="subject mb-4">相册 :</h2>
                            <div class="swiper-container swiper-gallery">
                                <div class="swiper-wrapper gallery-popup">
                                    <a class="swiper-slide" href="../images/accommodation/boutique-village/main-pic-01.jpg"><img src="../images/accommodation/boutique-village/thumb/main-pic-01.jpg" alt="booking Pai hotel" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="../images/accommodation/boutique-village/main-pic-02.jpg"><img src="../images/accommodation/boutique-village/thumb/main-pic-02.jpg" alt="cottage stay Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="../images/accommodation/boutique-village/main-pic-03.jpg"><img src="../images/accommodation/boutique-village/thumb/main-pic-03.jpg" alt="Oasis in Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="../images/accommodation/boutique-village/main-pic-04.jpg"><img src="../images/accommodation/boutique-village/thumb/main-pic-04.jpg" alt="Oasis in Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="../images/accommodation/boutique-village/main-pic-05.jpg"><img src="../images/accommodation/boutique-village/thumb/main-pic-05.jpg" alt="Pai best resort" width="1500" height="843"></a>
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <?php include_once '_footer.php'; ?>