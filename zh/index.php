<?php
$html_class = '';
$body_class = 'page-home';
$cur_page   = 'home';
$par_page   = '';
$title      = '拜县乡村精品农场度假酒店 - Pai resort, Pai resorts, Resort in pai, Pai resort thailand, Pai boutique hotel';
$desc       = 'Welcome to Our Village & Farm - The team at 拜县乡村精品农场度假酒店 are happy to welcome you to our little piece of paradise. Our focus is ensuring you enjoy your time with us and to also do our part for Pai. All of us love Pai and are dedicated to ensuring it remains as green as possible.';
$keyw       = 'Rasa,Hospitality,Rasa Tower,Management,Development,Business Management,Business Development,Accounting,Manager,Hotel,Resort,Investment,,Buri Rasa,Rasa Collection,Boutique,Traditional, Pai, Mae Hong Son,Thailand';
$local      = 'en-US';
$page_url   = 'index.php';
$ogimage    = ['../images/home/main-slide-001.jpg', '1500', '1000'];

$lang_zh    = $page_url;
$lang_en    = '../'. $page_url;
$lang_th    = 'th/'. $page_url;
include_once '_header.php' ?>
        <main class="site-main">
            <div class="multiple-slider-container">
                <div class="swiper-container swiper-image">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide"><img class="img-cover" src="../images/home/new/Landing-Page-01.jpg" srcset="../images/home/new/Landing-Page-01-400w.jpg 414w, ../images/home/new/Landing-Page-01.jpg" alt="Pai Village" width="1500" height="1000"></div>
                        <div class="swiper-slide"><img class="img-cover" src="../images/home/new/Landing-Page-02.jpg" srcset="../images/home/new/Landing-Page-02-400w.jpg 414w, ../images/home/new/Landing-Page-02.jpg" alt="sustainable hotel Pai" width="1500" height="1000"></div>
                        <div class="swiper-slide"><img class="img-cover" src="../images/home/new/Landing-Page-03.jpg" srcset="../images/home/new/Landing-Page-03-400w.jpg 414w, ../images/home/new/Landing-Page-03.jpg" alt="sustainable hotel Pai" width="1500" height="1000"></div>
                        <div class="swiper-slide"><img class="img-cover" src="../images/home/new/Landing-Page-04.jpg" srcset="../images/home/new/Landing-Page-04-400w.jpg 414w, ../images/home/new/Landing-Page-04.jpg" alt="Pai best homestay" width="1500" height="1000"></div>
                        <div class="swiper-slide"><img class="img-cover" src="../images/home/new/Landing-Page-05.jpg" srcset="../images/home/new/Landing-Page-05-400w.jpg 414w, ../images/home/new/Landing-Page-05.jpg" alt="best hotel Pai" width="1500" height="1000"></div>
                        <div class="swiper-slide"><img class="img-cover" src="../images/home/new/Landing-Page-06.jpg" srcset="../images/home/new/Landing-Page-06-400w.jpg 414w, ../images/home/new/Landing-Page-06.jpg" alt="sustainable hotel Pai" width="1500" height="1000"></div>
                        <div class="swiper-slide"><img class="img-cover" src="../images/home/new/Landing-Page-07.jpg" srcset="../images/home/new/Landing-Page-07-400w.jpg 414w, ../images/home/new/Landing-Page-06.jpg" alt="sustainable hotel Pai" width="1500" height="1000"></div>
                    </div>
                    <div class="swiper-pagination"></div>
                    <div class="swiper-button-prev swiper-button-white"><i class="icon-tab fal fa-chevron-circle-left"></i></div>
                    <div class="swiper-button-next swiper-button-white"><i class="icon-tab fal fa-chevron-circle-right"></i></div>
                </div>
                <div class="swiper-container swiper-video hide">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide video-container">
                            <video class="video-cover lazy-load" id="intro_video" poster="../video/pai-village-720p.jpg" loop muted data-hd-src="../video/pai-village-720p.mp4" data-sd-src="../video/pai-village-360p.mp4">
                                Your browser does not support the video tag.
                            </video>
                        </div>
                    </div>
                </div>
                <div class="button-play-position show">
                    <a href="#" title="Image/Video" class="button button-options image"></a>
                    <a href="#" title="Mute/Muted" class="button button-mute hide"></a>
                    <a href="#" title="Play/Pause" class="button button-play hide"></a>
                </div>
               
                <?php include 'inc/scroll-down.php'; ?>
            </div>
            <section id="section" class="intro">
                <div class="bg-noise mountain lazy-load" data-bg="url(../assets/elements/bg-texture-noise.png)">
                    <div class="container text-center py-5">
                        <h1 class="header mb-3 mb-md-5 pt-md-5" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="100">欢迎来到我们的度假村与农场, 泰国拜县</h1>
                        <p class="intro-desc mb-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">拜县乡村精品农场度假酒店全体员工诚挚的欢迎您来到这一度假天堂。在您入住期间， 我们将竭诚为您服务以确保您拥有一个完美假期。我们都爱着这个美丽的拜县，我们也将竭尽全力的去保护它并让它美丽延续，为您营造出最原生太的自然度假环境。因此您会 发现，房间内部并未放置电视机，取而代之的是让您欣赏到来自大自然的燕语莺声和     蛙鸣蝉叫。我们利用厨房的余料，做成天然化肥使得我们的有机蔬果不含任何化学添加。</p>
                        <p class="intro-desc pb-md-5" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">除此之外，我们特地贡献出部分收入以多种方式回馈社会，包括在当地修建学校。我们真诚的希望您也可以加入我们，来体验我们独特的酒店文化与服务——我们不仅仅是一个精品度假村。我们将以全心全意的服务使您宾至如归，成为您在拜县的 “家”。</p>
                    </div>
                    <div class="row no-gutters py-md-2">
                        <div class="col-12 col-md-7 pr-md-2" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="100">
                            <a href="accommodation.php">
                                <div class="box-hover">
                                    <img class="img-cover lazy-load" data-src="../images/home/main-pic-01.jpg" alt="Pai Village" width="916" height="537">
                                    <div class="content">
                                        <div class="content-wrapper">
                                            <h2 class="title">睡梦</h2>
                                            <p>找到完美，宁静的场地遏放松和梦想</p>
                                            <div class="click-button">
                                                <span>房间和房态</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-5 pl-md-2">
                            <div class="h-100" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">
                                <a href="massage.php">
                                    <div class="box-hover">
                                        <img class="img-cover lazy-load" data-src="../images/home/main-pic-02.jpg" alt="Pai Village" width="916" height="537">
                                        <div class="content">
                                            <div class="content-wrapper">
                                                <h2 class="title">复原</h2>
                                                <p>慢下来和放松</p>
                                                <div class="click-button">
                                                    <span>体验水疗</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row no-gutters py-md-2">
                        <div class="col-12 col-md-6 pr-md-2" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">
                            <a href="dining.php">
                                <div class="box-hover">
                                    <img class="img-cover lazy-load" data-src="../images/home/main-pic-04.jpg" alt="Pai Village" width="916" height="537">
                                    <div class="content">
                                        <div class="content-wrapper">
                                            <h2 class="title">用餐和享受</h2>
                                            <p>享受用餐</p>
                                            <div class="click-button">
                                                <span>查看我们的餐饮选择</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-6 pl-md-2" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                            <a href="offers.php">
                                <div class="box-hover">
                                    <img class="img-cover lazy-load" data-src="../images/home/main-pic-05.jpg" alt="Pai Village" width="916" height="537">
                                    <div class="content">
                                        <div class="content-wrapper">
                                            <h2 class="title">独家优惠</h2>
                                            <p>看到我们的特别优惠</p>
                                            <div class="click-button">
                                                <span>查看我们的特色菜</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="bg-noise lazy-load" data-bg="url(../assets/elements/bg-texture-noise.png)">
                    <div class="container py-5">
                        <div class="row">
                            <div class="col-12 col-md-7 py-3 order-md-2" data-aos="fade-right" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">
                                <img class="img-cover lazy-load" data-src="../images/home/pic-01.jpg" alt="Pai Village" width="765" height="519">
                            </div>
                            <div class="col-12 col-md-5 py-3 d-flex justify-content-center align-items-center order-md-1" data-aos="fade-left" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <div class="content-highlight text-md-right">
                                    <h2 class="title font-italic">夜丰颂城</h2>
                                    <p>拜县，是一个你可以什么都不用做，静静的让灵魂与这个天堂般的花园融合的地方。</p>
                                    <!-- <div class="d-block py-3">
                                        <a class="btn btn-default" href="">发现更多</a>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-gray">
                    <div class="container py-5">
                        <div class="row py-md-5">
                            <div class="col-12 col-md-4 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="300">
                                <h2 class="subject main-color"><i class="fab fa-facebook-square" aria-hidden="true"></i> Facebook</h2>
                                <a href="https://www.facebook.com/paivillageresort/" target="_blank">
                                    <picture>
                                        <source srcset="../uploads/2024/01/pai_village_fb-600x600.jpg, ../uploads/2024/01/pai_village_fb-1200x1200.jpg 2x" />
                                        <img src="../uploads/2024/01/pai_village_fb.jpg" alt="Pai Village Facebook" width="3544" height="3544" loading="lazy" />
                                    </picture>
                                </a>
                            </div>
                            <div class="col-12 col-md-4 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="350">
                                <h2 class="subject main-color"><i class="fab fa-instagram" aria-hidden="true"></i> Instagram</h2>
                                <a href="<?php echo get_info('instagram'); ?>" target="_bank">
                                    <img src="../images/PVB_IG.jpg" style="width:100%">
                                </a>
                            </div>
                            <div class="col-12 col-md-4 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="400">
                                <h2 class="subject main-color"><i class="fas fa-video" aria-hidden="true"></i> Video</h2>
                                <iframe class="lazy-load" width="100%" height="367" data-src="https://www.youtube.com/embed/SOR-oJZSkvg?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-texture inset-shadow lazy-load" data-bg="url(../assets/elements/bg-texture.png)">
                    <div class="container py-5">
                        <style>.list-awards-custom { gap: 20px; }</style>
                        <ul class="--list-awards list-awards-custom d-flex flex-wrap align-items-center justify-content-center" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="450">
                            <li><span class="d-inline-block" style="width:100px;"><img class="lazy-load" data-src="../assets/elements/awards/aw2.png" alt="Pai Village" width="105" height="98"></span></li>
                            <li><img class="lazy-load" data-src="../assets/elements/awards/aw4.png" alt="Pai Village" width="130" height="101"></li>
                            <li><img class="lazy-load" data-src="../assets/elements/awards/Logo-Cleantogether.png" alt="Pai Village" width="130" height="101"></li>
                            <li><span class="d-inline-block" style="width:110px;"><img class="lazy-load" data-src="../assets/elements/awards/MPH-LOGO.png" alt="Pai Village" width="130" height="101"></span></li>
                            <li><img class="lazy-load" data-src="../assets/elements/awards/SHA+.png" alt="Pai Village" width="150" height="auto"></li>
                            <li><span class="d-inline-block" style="width:90px;"><img class="lazy-load" data-src="../assets/elements/awards/SafeTravels.png" alt="Pai Village" width="101" height="101"></span></li>
                            <li><span class="d-inline-block" style="width:160px"><img class="lazy-load" data-src="../uploads/2024/03/stgs-horizontal.png" alt="STGS" width="1000" height="291"></span></li>
                        </ul>
                    </div>
                </div>
            </section>
        </main>
        <?php include_once '_footer.php'; ?>