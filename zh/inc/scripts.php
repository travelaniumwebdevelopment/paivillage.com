    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?= get_base_uri() . '/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js' ?>"></script>
    <script type="text/javascript" src="<?= get_base_uri() . '/node_modules/object-fit-images/dist/ofi.js' ?>"></script>
    <script type="text/javascript" src="<?= get_base_uri() . '/assets/plugins/object-fit-videos-1.0.4/dist/object-fit-videos.min.js' ?>"></script>
    <script type="text/javascript" src="<?= get_base_uri() . '/node_modules/js-throttle-debounce/build/js-throttle-debounce.min.js' ?>"></script>
    <script type="text/javascript" src="<?= get_base_uri() . '/node_modules/swiper/dist/js/swiper.min.js' ?>"></script>
    <script type="text/javascript" src="<?= get_base_uri() . '/node_modules/jquery-touchswipe/jquery.touchSwipe.min.js' ?>"></script>
    <script type="text/javascript" src="<?= get_base_uri() . '/node_modules/jquery-form-validator/form-validator/jquery.form-validator.min.js' ?>"></script>
    <script type="text/javascript" src="<?= get_base_uri() . '/node_modules/moment/moment.js' ?>"></script>
    <script type="text/javascript" src="<?= get_base_uri() . '/node_modules/moment-timezone/builds/moment-timezone-with-data-2012-2022.min.js' ?>"></script>
    <script type="text/javascript" src="<?= get_base_uri() . '/node_modules/js-cookie/src/js.cookie.js' ?>"></script>
    <script type="text/javascript" src="<?= get_base_uri() . '/node_modules/magnific-popup/dist/jquery.magnific-popup.min.js' ?>"></script>
    <script type="text/javascript" src="<?= get_base_uri() . '/node_modules/simplebar/dist/simplebar.js' ?>"></script>
    <script type="text/javascript" src="<?= get_base_uri() . '/node_modules/aos/dist/aos.js' ?>"></script>
    <script type="text/javascript" src="<?= get_base_uri() . '/node_modules/jquery-smooth-scroll/jquery.smooth-scroll.min.js' ?>"></script>
    <script type="text/javascript" src="<?= get_base_uri() . '/node_modules/vanilla-lazyload/dist/lazyload.min.js' ?>"></script>
    <script type="text/javascript" src="<?= get_base_uri() . '/assets/libs/tl-form.js' ?>"></script>
    <script type="text/javascript" src="<?= get_base_uri() . '/assets/plugins/t-datepicker/public/theme/js/t-datepicker.min.js' ?>"></script>
    <script type="text/javascript" src="<?= get_base_uri() . '/assets/libs/tl-booking.js' ?>"></script>
    <script type="text/javascript" src="<?= get_base_uri() . '/assets/js/booking.js' ?>"></script>
    <script type="text/javascript" src="<?= get_base_uri() . '/assets/libs/tl-member.js' ?>"></script>
    <script type="text/javascript" src="<?= get_base_uri() . '/assets/js/member.js' ?>"></script>
    <script type="text/javascript" src="<?= get_base_uri() . '/assets/js/instagram.js' ?>"></script>
    <script type="text/javascript" src="<?= get_base_uri() . '/assets/js/main.min.js' ?>"></script>
    <script type="text/javascript" src="<?= get_base_uri() . '/assets/js/custom.js?ver=' . filemtime( get_base_dir() . '/assets/js/custom.js' ) ?>"></script>

    <?php if(is_page('home')) : ?>
    <script type="text/javascript">
        if( moment.tz('Asia/Bangkok').isBetween( '2020-10-10 00:00:00', '2020-12-12 23:59:59' ) ) {
            $.magnificPopup.open({
                items: {
                    type: 'image',
                    src: '../images/popup/PVB-For-web-1500.jpg',
                },
                // callbacks: {
                //     open: function() {
                //         var imgUrl = 'https://reservation.travelanium.net/propertyibe2/rates?propertyId=253&onlineId=4&accesscode=TWELVE12&lang=zh',
                //         imgTar = '_blank';
                //         $(this.content).find('.mfp-img').wrap('<a href="'+imgUrl+'" target="'+imgTar+'" rel="noopener" />');
                //     },
                // },
            });
        }

        setInterval(function(){  
            var divUtc = $('#divUTC');
            var divLocal = $('#divLocal');  
            divUtc.text(moment.utc().format('YYYY-MM-DD HH:mm:ss'));      
            
            var localTime  = moment.utc(divUtc.text()).toDate();
            localTime = moment(localTime).format('YYYY-MM-DD HH:mm:ss');
            divLocal.text(localTime);  
        
            $('#local-time').text(moment.tz('Asia/Bangkok').format('hh:mm:ss A'));
            $('#local-date').text(moment.tz('Asia/Bangkok').format('DD MMMM YYYY'));
            $('#date-str').text(moment.tz('Asia/Bangkok').format('ddd'));
            $('#date-num').text(moment.tz('Asia/Bangkok').format('DD'));
            $('#month').text(moment.tz('Asia/Bangkok').format('MMM'));
            $('#year').text(moment.tz('Asia/Bangkok').format('YYYY'));
        },1000);
        
        // var $fbVideo = $('<div>').addClass('fb-video').attr({
        //     'data-href': 'https://www.facebook.com/goodviewkohchang/videos/374113176351328/',
        //     'data-width': 500,
        //     'data-show-text': true
        // }).append(`<blockquote cite="https://developers.facebook.com/goodviewkohchang/videos/374113176351328/" class="fb-xfbml-parse-ignore"><a href="https://developers.facebook.com/goodviewkohchang/videos/374113176351328/"></a><p></p>โพสต์โดย <a href="https://www.facebook.com/goodviewkohchang/">Good View Resort Koh Chang</a> เมื่อ วันพุธที่ 25 ตุลาคม  2017</blockquote>`);
        
        var $fbPage = $('<div>').addClass('fb-page').attr({
            'data-href': 'https://www.facebook.com/paivillageboutiqueresort/',
            'data-tabs': 'timeline',
            'data-width': 500,
            'data-height': 370,
            'data-small-header': true,
            'data-adapt-container-width': true,
            'data-hide-cover': true,
            'data-show-facepile': true
        }).append(`<blockquote cite="https://www.facebook.com/paivillageboutiqueresort/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/paivillageboutiqueresort/">Pai Village Boutique Resort and Farm</a></blockquote>`);

        $(window).on('load scroll', function() {
            var winY = window.scrollY || window.pageYOffset;
            var winH = window.innerHeight;

            var LazyIg    = $('.ig-feed');
            // var LazyVideo = $('.lazy-fb-video');
            var LazyPage  = $('.lazy-fb-page');

            // if (!LazyVideo.hasClass('loaded')) {
            //     var trigger = LazyVideo.offset().top;
            //     if (trigger <= winY + winH) {
            //         LazyVideo.addClass('loaded').append($fbVideo);
            //         FB.XFBML.parse();
            //     }
            // };

            if (!LazyPage.hasClass('loaded')) {
                var trigger = LazyPage.offset().top;
                if (trigger <= winY + winH) {
                    LazyPage.addClass('loaded').append($fbPage);
                    FB.XFBML.parse();
                }
            };

            if(!LazyIg.hasClass('loaded')) {
                var trigger = LazyIg.offset().top;
                if (trigger <= winY + winH) {
                    // Instagram
                    $('.loading-indicator').addClass('show');
                    LazyIg.addClass('loaded');
                    LazyIg.instagramGallery({
                        limit: 9,
                    });
                }
            }

            return false
        });
        
        var slideImage = new Swiper('.swiper-image', {
            loop: true,
            effect: 'fade',
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            autoplay: {
                delay: 2000,
            },
            speed : 1500,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
        });

        // Video Playback
        var $container = $('.video-container');
        var _vid = $('#intro_video').get(0);
        var _vidBtn = $('.button-play').get(0);
        var $options = $('.button-options');
        var $btnMute = $('.button-mute');
        var $btnPlay = $('.button-play');

        $options.on('click', function(event) {
            event.preventDefault();
            $(this).toggleClass('image');
            $btnMute.toggleClass('hide');
            $btnPlay.toggleClass('hide');
            $('.button-play-position').toggleClass('show');
            $('.swiper-image').toggleClass('hide');
            $('.swiper-video').toggleClass('hide');

            if($('.swiper-image')) {
                _vid.pause();
            }

            if($('.swiper-video')) {
                setTimeout(() => {
                    _vid.play();
                }, 500);
            }
            
            var slideImage = new Swiper('.swiper-image', {
                loop: true,
                effect: 'fade',
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                autoplay: {
                    delay: 2000,
                },
                speed : 1500,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
            });
        });

        // Video Resolution Dynamic
        function renderSource(src, type) {
            return '<source src="'+src+'" type="'+type+'">';
        }

        if ($(_vid).length) {
            if (window.innerWidth > 640) {
                $(_vid).prepend(renderSource(_vid.dataset.hdSrc, 'video/mp4'));
            } else {
                $(_vid).prepend(renderSource(_vid.dataset.sdSrc, 'video/mp4'));
            }

            $(_vid).on('play', function() {
                $(_vidBtn).removeClass('played');
            });

            $(_vid).on('pause', function() {
                $(_vidBtn).addClass('played');
            });
        }

        $(window).on('load', function() {
            if ($(_vid).length) {
                if( _vid.paused ) {
                    $(_vidBtn).addClass('played');
                } else {
                    $(_vidBtn).removeClass('played');
                }

                $('.button-play-position').removeClass('d-none');
            }
        });

        $('.button-play').on('click', function(e) {
            e.preventDefault();
            if( _vid.paused ) {
                _vid.play();
            } else {
                _vid.pause();
            }
        });

        $('.button-mute').on('click', function(ev) {
            ev.preventDefault();
            if( _vid.muted ) {
                _vid.muted=false;
                $(this).addClass('muted');
            } else {
                _vid.muted=true;
                $(this).removeClass('muted');
            }
        });

    </script>
    <?php endif; ?>

    <?php if (is_page('gallery')) : ?>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/4.1.4/imagesloaded.pkgd.min.js'></script>
    <script>
        /** gallery page */
        var $grid = $('.grid').isotope({
            itemSelector: '.element-item',
            layoutMode: 'masonry'
        });
        var filterFns = {};
        $(window).on('load', function(){
            var filterValue = $('.button').attr('data-filter');
            filterValue = filterFns[filterValue] || filterValue;
            galleryFilter(filterValue);
        });
        
        $('#filters').on('click', 'li', function () {
            var filterValue = $(this).attr('data-filter'); 
            filterValue = filterFns[filterValue] || filterValue;
            $grid.isotope({
                filter: filterValue
            });
        });
        
        $('.button-group').each(function (i, buttonGroup) {
            var $buttonGroup = $(buttonGroup);
            $buttonGroup.on('click', 'li', function () {
                $buttonGroup.find('.is-checked').removeClass('is-checked');
                $(this).addClass('is-checked');
            });
        });
        
        $grid.imagesLoaded().progress(function(){
            $grid.isotope('layout');
        });

        /*** gallery */
        popupFilter('.element-item');
        galleryFilter('.hightlight');

        $('.filter-tabs').on('click', 'li', function() {
            var filter = this.dataset.filter;
            popupFilter(filter);
        });

        function galleryFilter(hash) {
            if (!hash) return;
            var filter = '.'+hash.substring(1);
            $grid.isotope({
                filter: filter
            });
        }

        function popupFilter(category) {
            $grid.magnificPopup({
                delegate: category,
                type: 'image',
                tLoading: 'Loading image #%curr%...',
                mainClass: 'mfp-img-mobile',
                gallery: {
                    enabled: true,
                    navigateByImgClick: true,
                    preload: [0,1]
                },
                image: {
                    tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                    titleSrc: function(item) {
                        return '相册' + '<small>by Pai Village Boutique Resort & Farm</small>';
                    },
                },
                callbacks: {
                    elementParse: function(item) {
                        if(item.el.context.className == 'element-item vistualtour') {
                            item.type = 'iframe';
                        } else {
                            item.type = 'image';
                        }
                    }
                },
            });
        }
    </script>
    <?php endif; ?>

    <?php if (is_page('contact')) : ?>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.79/jquery.form-validator.min.js'></script>
        <script>
            ;(function ($) {
                // Contact Form 
                var $contactForm = $('#contact_form');
                var $contactFormResult = $contactForm.find('.form-result');
                var emailto = 'reservations@paivillage.com';
                $.validate({
                    form: $contactForm,
                    modules: 'html5, security',
                    onSuccess: function($form){
                        var form    = $form.get(0);
                        var url     = form.action;
                        var data    = new FormData(form);

                        data.append('base', form.baseURI);

                        var postAjax = $.ajax({
                            type: 'POST',
                            url: url,
                            data: data,
                            processData: false,
                            contentType: false,
                            beforeSend: function() {
                                $form.addClass('form-sending');
                            }
                        });

                        postAjax.done(function(data, status, jqXHR) {
                            var data = JSON.parse(data);
                            if (data.status=='success') {
                                displayResult($contactFormResult, 'alert-success', 'Thank You! Your information has been sent and we should be in touch with you soon.');
                                form.reset();
                            } else {
                                displayResult($contactFormResult, 'alert-danger', 'Message cannot be sent. Please contact directly to <a class="alert-link" href="mailto:'+ emailto +'">'+ emailto +'</a> <br/>We apologize for any inconvenience.');
                            }
                        });

                        postAjax.fail(function(jqXHR) {
                            displayResult($contactFormResult, 'alert-danger', 'Sorry, Message cannot be sent!')
                        });

                        postAjax.always(function() {
                            $form.removeClass('form-sending');
                        });

                        return false;
                    }
                });

                function displayResult(selector, addClass, message) {
                    var $el = $(selector);
                    $el.removeClass('alert-success alert-danger')
                    .addClass(addClass)
                    .html(message)
                    .fadeIn();
                }

            })(jQuery);
        </script>
    <?php endif; ?>

    <?php if (is_page('location')) : ?>
        <script type="text/javascript">
            ;(function($){
                // Location Carousel
                var $map_carousel = $('#google_map_carousel');
                var $map_navigation = $('#google_map_nav');
                new Swiper($map_carousel, {
                    effect: 'fade',
                    fadeEffect: {
                        crossFade: 1,
                    },
                    hashNavigation: {
                        watchState: 1,
                        replaceState: 1
                    },
                    simulateTouch: 0,
                    on: {
                        init: function() {
                            var active = this.activeIndex;
                            update_map_navigation(active);
                        },
                        slideChange: function() {
                            var active = this.activeIndex;
                            update_map_navigation(active);
                        }
                    }
                });
                function update_map_navigation(active) {
                    var $active = $map_navigation.find('li').eq(active);
                    $active.siblings().find('a').removeClass('active');
                    $active.find('a').addClass('active');
                }
            })(jQuery);
        </script>
    <?php endif; ?>

    <?php if(is_page('massage')) : ?>
        <!-- <script>
            $.magnificPopup.open({
                items: {
                    src:    '<div id="small-dialog" class="zoom-anim-dialog">'+
                                '<h1 class="header">Notice</h1>'+
                                '<p class="desc"><b>Closed</b> For Renovation Until 1 January 2020</p>'+
                            '</div>',
                    type:   'inline'
                },

                fixedContentPos: true,
                fixedBgPos: true,

                overflowY: 'auto',

                closeBtnInside: true,
                preloader: false,
                
                midClick: true,
                removalDelay: 700,
                mainClass: 'my-mfp-slide-bottom'
            });
        </script> -->
    <?php endif; ?>

    <?php if(is_page('farm')) : ?>
        <style>.mfp-bg {background-color:transparent;}.site-header, #scroll_down, .site-footer {display: none !important;}</style>
        <script language='javascript'>
            $.magnificPopup.open({
                items: {
                    src:    '<div id="small-dialog" class="zoom-anim-dialog text-center" style="max-width:1500px;width:100%;margin:0 auto;background-color:#ffffff;padding:20px 30px;">'+
                                '<p class="" style="font-size: 38px">Please wait while you are redirected to our sister property Farm Stay at Pai, home of The Village Farm. The Village Farm is complimentary for all guests of Pai Village Boutique Resort.</p>'+
                            '</div>',
                    type:   'inline'
                },

                fixedContentPos: true,
                fixedBgPos: true,

                overflowY: 'auto',

                closeBtnInside: true,
                preloader: false,
                
                midClick: true,
                removalDelay: 700,
                mainClass: 'my-mfp-slide-bottom'
            });
            setTimeout(function() {
                window.location="http://www.farmstayatpai.com/";
            }, 6000);
        </script>
    <?php endif; ?>