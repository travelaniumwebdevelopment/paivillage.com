    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php web_title(); ?></title>
    <?php web_desc(); ?>
    <?php web_keyw(); ?>
    <?php the_opengraph(); ?>

    <link rel="apple-touch-icon" sizes="57x57" href="../assets/elements/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../assets/elements/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../assets/elements/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/elements/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../assets/elements/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../assets/elements/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../assets/elements/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/elements/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../assets/elements/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="../assets/elements/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../assets/elements/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../assets/elements/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/elements/favicons/favicon-16x16.png">
    <link rel="manifest" href="../assets/elements/favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#947e35">
    <meta name="msapplication-TileImage" content="../assets/elements/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#e2dac4">

    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/simplebar/2.6.1/simplebar.min.css'/>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.79/theme-default.min.css'/>
    
    <link rel="stylesheet" href="../assets/css/main.css">
    <link rel="stylesheet" href="<?= get_base_uri() . '/assets/css/custom.css?ver=' . filemtime( get_base_dir() . '/assets/css/custom.css' ) ?>">

    <style>
        .pv-cookie-bar {
            --pv--cookie-bar--accent-color: #a98f23;
            --pv--cookie-bar--text: #fff;
            --pv--cookie-bar--bg: var(--pv--cookie-bar--accent-color);
            --pv--cookie-bar--link-text--text: var(--pv--cookie-bar--text);
            --pv--cookie-bar--link-text--text--hover: var(--pv--cookie-bar--link-text--text);
            --pv--cookie-bar--accept-button--bg: #fff;
            --pv--cookie-bar--accept-button--text: var(--pv--cookie-bar--accent-color);
            --pv--cookie-bar--box-shadow: 0 0 1rem rgba(0,0,0,0.125);
        }
    </style>

    <?php if(is_page('home')) : ?>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v3.3"></script>
    <?php endif; ?>

    <!-- Google Analytics -->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-63923616-3', 'auto', {
            'allowLink': true
        });
        ga('require', 'linker');
        ga('linker:autoLink', ['travelanium.net']);
        ga('send', 'pageview');
    </script>
    <!-- End Google Analytics -->
    <!-- Google Tag Manager -->
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-5H7BB78');
    </script>
    <!-- End Google Tag Manager -->