<?php
$html_class = '';
$body_class = 'page-contact';
$cur_page   = 'contact';
$par_page   = '';
$title      = 'Contact - Pai Village Boutique Resort & Farm';
$desc       = 'Exclusive Offers available on Official Site ONLY. Best Rates Guaranteed. Member Deal. Private Villa and Cottage, located along the banks of Pai River featuring Rustic Bamboo Cottage set among widing pathways.';
$keyw       = 'Rasa,Hospitality,Rasa Tower,Management,Development,Business Management,Business Development,Accounting,Manager,Hotel,Resort,Investment,,Buri Rasa,Rasa Collection,Boutique,Traditional, Pai, Mae Hong Son,Thailand';
$local      = 'en-US';
$page_url   = 'contact.php';

$lang_zh    = $page_url;
$lang_en    = '../'. $page_url;
$lang_th    = 'th/'. $page_url;
include_once '_header.php' ?>
        <main class="site-main">
            <section id="section" class="intro">
                <div class="bg-texture mountain pt-md-5">
                    <div class="container py-5">
                        <h1 class="header text-center mb-3 my-md-5" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="100">联系方式</h1>
                        <div class="row py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">
                            <div class="col-12 col-md-6 my-md-5">
                            <form id="contact_form" action="../forms/sendmail.php" method="POST">
                                    <div class="form-result alert" style="display: none;"></div>
                                    <div class="row">
                                        <div class="col-12 col-md-2 pdr">
                                            <div class="form-group">
                                                <select name="title" class="form-control custom-select" required>
                                                    <option value="" selected="selected">Title</option>
                                                    <option value="Mr">Mr.</option>
                                                    <option value="Mrs">Mrs.</option>
                                                    <option value="Miss">Miss</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-5 pdx">
                                            <div class="form-group">
                                                <input class="form-control" type="text" name="firstname" placeholder="名字" required="">
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-5 pdl">
                                            <div class="form-group">
                                                <input class="form-control" type="text" name="lastname" placeholder="姓" required="">
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6 pdr">
                                            <div class="form-group">
                                                <input class="form-control" type="email" name="email" placeholder="电子邮件地址" data-validation="email" required="">
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6 pdl">
                                            <div class="form-group">
                                                <input class="form-control" type="text" name="tel" placeholder="电话" data-validation="number" required="">
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-12 text-left">
                                            <div class="form-group">
                                                <select name="country" class="form-control custom-select" required>
                                                    <option value="" selected="selected" >- 国家 -</option>
                                                    <?php include('../inc/country.php') ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <textarea name="message" class="form-control" required></textarea>
                                            </div>
                                            <div class="form-group">
                                                <input data-validation="recaptcha" data-validation-recaptcha-sitekey="6Ld-FasUAAAAAFLWcFouWqsyiGxD5ydjFBdyena2">
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-send">发送 <span class="loading-indicator"></span></button>
                                                
                                            </div>   
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-12 col-md-6 my-md-5">
                                <div class="office-address">
                                    <h3><?php echo get_info('site_name') ?></h3>
                                    <p class="mb-0"><?php echo get_info('address') ?></p>
                                </div>
                                <div class="button-contact pt-3">
                                    <a class="btn-contacts buttons arrows-side" href="tel:+6653698152" target="_blank"><i class="icon-contact fas fa-phone-volume" aria-hidden="true"></i> +66 (0) 5369 8152</a>
                                    <a class="btn-contacts buttons arrows-side" href="<?php echo get_info('facebook') ?>" target="_blank"><i class="icon-contact fab fa-facebook-square" aria-hidden="true"></i> <?php echo get_info('site_name') ?></a>
                                    <a class="btn-contacts buttons arrows-side" href="mailto:<?php echo get_info('email') ?>" target="_blank"><i class="icon-contact fas fa-envelope" aria-hidden="true"></i> <?php echo get_info('email') ?></a>
                                    <a class="btn-contacts buttons arrows-side" href="<?php echo get_info('map_url') ?>" target="_blank"><i class="icon-contact fas fa-map-marker-alt" aria-hidden="true"></i> Google Map</a>
                                </div>
                                <a class="btn-manage buttons arrows-side" target="_blank" href="https://reservation.travelanium.net/propertyibe2/booking-management?propertyId=<?php echo get_info('ibeID') ?>&onlineId=4"><i class="icon fal fa-user-circle" aria-hidden="true"></i> Manage My Booking</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <?php include_once '_footer.php'; ?>