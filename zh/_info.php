<?php

/* ==General information */
define( '_site_url', './' );
define( '_site_name', '拜县乡村精品农场度假酒店' );
define( '_site_local', 'zh_CN' );
define( '_site_lang', 'zh' );
define( '_site_version', '20190724' );

/* ==Travelanium system infomation */
define( '_ibe_ID', '253' );
define( '_ibe_url', 'https://reservation.travelanium.net/propertyibe2/rates?propertyId=253&onlineId=4&lang=en' );

/* ==Social */
define( '_facebook', 'https://www.facebook.com/paivillageboutiqueresortandfarm' );
define( '_flickr', '#' );
define( '_googleplus', '#' );
define( '_instagram', 'https://www.instagram.com/paivillageboutique/' );
define( '_line', '#' );
define( '_pinterest', '#' );
define( '_tripadvisor', 'https://www.tripadvisor.com/Hotel_Review-g303916-d729469-Reviews-Pai_Village_Boutique_Resort_Farm-Pai_Mae_Hong_Son_Province.html' );
define( '_twitter', '#' );
define( '_vimeo', '#' );
define( '_weibo', '#' );
define( '_whatapps', '#' );
define( '_youtube', 'https://www.youtube.com/watch?v=SOR-oJZSkvg&t=5s' );
define( '_email_contact', 'reservations@paivillage.com' );
define( '_tel_contact', '+66 (0) 5369 8152' );
define( '_address_contact', '88 Moo 3, Vieng Tai, Pai Maehongson, 58130 Thailand' );

/* ==Google map */
define( '_map_url', 'https://goo.gl/maps/rKtsx2XTugocQ6cJA' );

/* Name Room */
define( '_room_name_1', 'Rasa Family Suite' );
define( '_room_space_1', '90' );
define( '_room_guest_1', '4' );
define( '_room_slug_1', 'family-suite' );

define( '_room_name_2', 'Boutique Village' );
define( '_room_space_2', '21' );
define( '_room_guest_2', '3' );
define( '_room_slug_2', 'deluxe-village' );

define( '_room_name_3', 'Boutique Garden' );
define( '_room_space_3', '20' );
define( '_room_guest_3', '2' );
define( '_room_slug_3', 'deluxe-garden' );

define( '_room_name_4', 'Rasa Villa' );
define( '_room_space_4', '51' );
define( '_room_guest_4', '4' );
define( '_room_slug_4', 'deluxe-villa' );

define( '_room_name_5', 'Boutique Mountain View' );
define( '_room_space_5', '36' );
define( '_room_guest_5', '4' );
define( '_room_slug_5', 'deluxe-pool-view' );

define( '_room_name_6', 'Boutique Pool View' );
define( '_room_space_6', '36' );
define( '_room_guest_6', '4' );
define( '_room_slug_6', 'boutique-pool-view' );

define( '_room_name_7', 'Boutique Grand Village' );
define( '_room_space_7', '35' );
define( '_room_guest_7', '3' );
define( '_room_slug_7', 'boutique-grand-village' );
// define( '_room_name_1', '拉沙露天阳台套房' );
// define( '_room_space_1', '100' );
// define( '_room_guest_1', '4' );
// define( '_room_slug_1', 'family-suite' );

// define( '_room_name_2', '豪华乡村房' );
// define( '_room_space_2', '35' );
// define( '_room_guest_2', '3' );
// define( '_room_slug_2', 'deluxe-village' );

// define( '_room_name_3', '豪华园景房' );
// define( '_room_space_3', '20' );
// define( '_room_guest_3', '2' );
// define( '_room_slug_3', 'deluxe-garden' );

// define( '_room_name_4', '豪华别墅' );
// define( '_room_space_4', '65' );
// define( '_room_guest_4', '4' );
// define( '_room_slug_4', 'deluxe-villa' );

// define( '_room_name_5', '豪华池景房' );
// define( '_room_space_5', '36' );
// define( '_room_guest_5', '4' );
// define( '_room_slug_5', 'deluxe-pool-view' );

/* Name Dining */
define( '_dining_name_1', '牛排馆' );
define( '_dining_slug_1', 'the-blue-ox-steak-house' );
define( '_dining_name_2', '小兔子咖啡店' );
define( '_dining_slug_2', 'rabbit-cafe' );
