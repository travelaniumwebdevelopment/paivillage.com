<?php

require_once '../configs.php';
require_once( '_info.php' );


function get_info( $var ) {
    switch ($var) {
        case 'site_url': return _site_url; break;
        case 'site_name'; return _site_name; break;
        case 'site_lang'; return _site_lang; break;
        case 'local'; return _site_local; break;
        case 'version'; return _site_version; break;
        case 'ibeID'; return _ibe_ID; break;
        case 'ibeID_url'; return _ibe_url; break;
        case 'map_url'; return _map_url; break;
        case 'facebook'; return _facebook; break;
        case 'flickr'; return _flickr; break;
        case 'googleplus'; return _googleplus; break;
        case 'instagram'; return _instagram; break;
        case 'line'; return _line; break;
        case 'pinterest'; return _pinterest; break;
        case 'tripadvisor'; return _tripadvisor; break;
        case 'twitter'; return _twitter; break;
        case 'vimeo'; return _vimeo; break;
        case 'weibo'; return _weibo; break;
        case 'whatapps'; return _whatapps; break;
        case 'youtube'; return _youtube; break;
        case 'email'; return _email_contact; break;
        case 'address'; return _address_contact; break;
        case 'tel'; return _tel_contact; break;
    }
}

function web_title() {
    global $title;
    if( isset($title) ) {
		echo $title;
	} else {
		echo get_info('site_name');
	}
}

function web_desc() {
    global $desc;
    if( isset($desc) ) {
        echo '<meta name="description" content="'.$desc.'">' . "\r\n";
	}
}

function web_keyw() {
    global $keyw;
    if( isset($keyw) ) {
        echo '<meta name="keywords" content="'.$keyw.'">' . "\r\n";
	}
}

function html_class() {
    global $html_class;
    if( isset($html_class) ) {
		echo $html_class;
	}
}

function body_class() {
    global $body_class;
    if( isset($body_class) ) {
        echo $body_class;
	}
}

function get_current_lang( $lang = NULL ) {
    $language = get_info('site_lang');
    if( $lang != NULL && $language == $lang )
        return 'active';
}

function get_current_class( $page = NULL ) {
    global $cur_page;
    global $par_page;
    if( $page != NULL && $cur_page == $page )
        return 'current';
    if( $page != NULL && $cur_page == $page || $page != NULL && $par_page == $page )
        return 'ancestor';
}

function get_opengraph_meta_tag( $property, $content ) {
	return '<meta property="'.$property.'" content="'.$content.'" />' . "\r\n";
}

function the_opengraph() {
	global $title, $desc, $page_url, $local, $ogimage, $ogtype;
        
    $type = ( isset($ogtype) ) ? $ogtype : 'website';
    echo get_opengraph_meta_tag('og:type', $type);
    
	if( isset($title) ) {
		echo get_opengraph_meta_tag('og:title', $title);
	}

	if( isset($desc) ) {
		echo get_opengraph_meta_tag('og:description', $desc);
	}

	if( isset($page_url) ) {
		echo get_opengraph_meta_tag('og:url', $page_url);
	}

	if( isset($local) ) {
		echo get_opengraph_meta_tag('og:local', $local);
	}

	if( isset($ogimage) ) {
		echo get_opengraph_meta_tag('og:image', $ogimage[0]);
		echo get_opengraph_meta_tag('og:image:width', $ogimage[1]);
		echo get_opengraph_meta_tag('og:image:height', $ogimage[2]);
	}
}

function ibe_url( $hid = NULL, $lang = 'en', $pid = NULL, $code = NULL ) {
    global $ibeID;
    
    $hid = ( $hid !== NULL ) ? $hid : $ibeID;
    $page = ($pid !== NULL) ? 'hotelpage' : 'propertyibe2' ;
    
    $url = 'https://reservation.travelanium.net/'.$page.'/rates?propertyId='.$hid.'&onlineId=4&lang='.$lang;
    
    if( $pid !== NULL)
        $url .= '&pid='.$pid;
    if( $code !== NULL)
        $url .= '&accesscode='.$code;
    if( $pid !== NULL && $code !== NULL )  
        $url .= '&pid='.$pid.'&accesscode='.$code;

    echo $url;
}

function is_page( $current ) {
    global $cur_page;
    if( $cur_page === $current )
        return true;
}

function is_child_page_of( $parent ) {
    global $par_page;
    if( $par_page === $parent )
        return true;
}

function get_base_uri() {
    return BASE_URI;
}

function get_base_dir() {
    return BASE_DIR;
}