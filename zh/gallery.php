<?php
$html_class = '';
$body_class = 'page-gallery';
$cur_page   = 'gallery';
$par_page   = '';
$title      = 'Photo Gallery - Pai Village Boutique Resort & Farm';
$desc       = 'Exclusive Offers available on Official Site ONLY. Best Rates Guaranteed. Member Deal. Private Villa and Cottage, located along the banks of Pai River featuring Rustic Bamboo Cottage set among widing pathways.';
$keyw       = 'Rasa,Hospitality,Rasa Tower,Management,Development,Business Management,Business Development,Accounting,Manager,Hotel,Resort,Investment,,Buri Rasa,Rasa Collection,Boutique,Traditional, Pai, Mae Hong Son,Thailand';
$local      = 'en-US';
$page_url   = 'gallery.php';

$lang_zh    = $page_url;
$lang_en    = '../'. $page_url;
$lang_th    = 'th/'. $page_url;
include_once '_header.php' ?>
        <main class="site-main">
            <section id="section" class="intro">
                <div class="bg-texture mountain pt-md-5">
                    <div class="container py-5">
                        <h1 class="header text-center mb-3 mb-md-5" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="100">相册</h1>
                        <div class="row py-5">
                            <div class="col-12 col-md-12">
                                <div id="filters" class="button-group filter-tabs">
                                    <ul class="">
                                        <!-- <li class="button is-checked" data-filter=".element-item">全部照片</li> -->
                                        <li class="button is-checked" data-filter=".highlight">HIGHLIGHTS</li>
                                        <li class="button" data-filter=".accommodation">客房</li>
                                        <li class="button" data-filter=".dining">餐饮</li>
                                        <li class="button" data-filter=".leisure">LEISURE</li>
                                        <!-- <li class="button" data-filter=".farm">度假村农场</li>
                                        <li class="button" data-filter=".spa">按摩</li> -->
                                        <li class="button" data-filter=".activities">休闲活动</li>
                                    </ul>
                                </div>
                                <div class="grid pb-5">
                                    <a href="../images/gallery/highlight/main-pic-01.jpg" class="element-item highlight"><img class="img-cover" src="../images/gallery/highlight/main-pic-01.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/gallery/highlight/main-pic-02.jpg" class="element-item highlight"><img class="img-cover" src="../images/gallery/highlight/main-pic-02.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/gallery/highlight/main-pic-03.jpg" class="element-item highlight"><img class="img-cover" src="../images/gallery/highlight/main-pic-03.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/gallery/highlight/main-pic-04.jpg" class="element-item highlight"><img class="img-cover" src="../images/gallery/highlight/main-pic-04.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/gallery/highlight/main-pic-05.jpg" class="element-item highlight"><img class="img-cover" src="../images/gallery/highlight/main-pic-05.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/gallery/highlight/main-pic-06.jpg" class="element-item highlight"><img class="img-cover" src="../images/gallery/highlight/main-pic-06.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/gallery/highlight/main-pic-07.jpg" class="element-item highlight"><img class="img-cover" src="../images/gallery/highlight/main-pic-07.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/gallery/highlight/main-pic-08.jpg" class="element-item highlight"><img class="img-cover" src="../images/gallery/highlight/main-pic-08.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/gallery/highlight/main-pic-09.jpg" class="element-item highlight"><img class="img-cover" src="../images/gallery/highlight/main-pic-09.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/gallery/highlight/main-pic-10.jpg" class="element-item highlight"><img class="img-cover" src="../images/gallery/highlight/main-pic-10.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/gallery/highlight/main-pic-11.jpg" class="element-item highlight"><img class="img-cover" src="../images/gallery/highlight/main-pic-11.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/gallery/highlight/main-pic-12.jpg" class="element-item highlight"><img class="img-cover" src="../images/gallery/highlight/main-pic-12.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/gallery/highlight/main-pic-13.jpg" class="element-item highlight"><img class="img-cover" src="../images/gallery/highlight/main-pic-13.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/gallery/highlight/main-pic-14.jpg" class="element-item highlight"><img class="img-cover" src="../images/gallery/highlight/main-pic-14.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/gallery/highlight/main-pic-15.jpg" class="element-item highlight"><img class="img-cover" src="../images/gallery/highlight/main-pic-15.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/gallery/highlight/main-pic-16.jpg" class="element-item highlight"><img class="img-cover" src="../images/gallery/highlight/main-pic-16.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/gallery/highlight/main-pic-17.jpg" class="element-item highlight"><img class="img-cover" src="../images/gallery/highlight/main-pic-17.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/gallery/highlight/main-pic-18.jpg" class="element-item highlight"><img class="img-cover" src="../images/gallery/highlight/main-pic-18.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/gallery/highlight/main-pic-19.jpg" class="element-item highlight"><img class="img-cover" src="../images/gallery/highlight/main-pic-19.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/gallery/highlight/main-pic-20.jpg" class="element-item highlight"><img class="img-cover" src="../images/gallery/highlight/main-pic-20.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/gallery/highlight/main-pic-21.jpg" class="element-item highlight"><img class="img-cover" src="../images/gallery/highlight/main-pic-21.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <!-- <a href="../images/gallery/highlight/main-pic-22.jpg" class="element-item highlight"><img class="img-cover" src="../images/gallery/highlight/main-pic-22.jpg" title="Highlights" alt="homestay in Pai"></a> -->

                                    <a href="../images/accommodation/boutique-garden/main-pic-01.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-garden/main-pic-01.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-garden/main-pic-02.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-garden/main-pic-02.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-garden/main-pic-03.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-garden/main-pic-03.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-garden/main-pic-04.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-garden/main-pic-04.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-grand-village/main-pic-01.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-grand-village/main-pic-01.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-grand-village/main-pic-02.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-grand-village/main-pic-02.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-grand-village/main-pic-03.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-grand-village/main-pic-03.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-mountain-view/main-pic-01.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-mountain-view/main-pic-01.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-mountain-view/main-pic-02.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-mountain-view/main-pic-02.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-mountain-view/main-pic-03.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-mountain-view/main-pic-03.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-mountain-view/main-pic-04.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-mountain-view/main-pic-04.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-mountain-view/main-pic-05.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-mountain-view/main-pic-05.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-mountain-view/main-pic-06.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-mountain-view/main-pic-06.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-mountain-view/main-pic-07.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-mountain-view/main-pic-07.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-mountain-view/main-pic-08.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-mountain-view/main-pic-08.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-mountain-view/main-pic-09.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-mountain-view/main-pic-09.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-mountain-view/main-pic-10.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-mountain-view/main-pic-10.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-mountain-view/main-pic-11.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-mountain-view/main-pic-11.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-mountain-view/main-pic-12.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-mountain-view/main-pic-12.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-pool-view/main-pic-01.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-pool-view/main-pic-01.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-pool-view/main-pic-02.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-pool-view/main-pic-02.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-pool-view/main-pic-03.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-pool-view/main-pic-03.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-pool-view/main-pic-04.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-pool-view/main-pic-04.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-pool-view/main-pic-05.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-pool-view/main-pic-05.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-pool-view/main-pic-06.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-pool-view/main-pic-06.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-pool-view/main-pic-07.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-pool-view/main-pic-07.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-pool-view/main-pic-08.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-pool-view/main-pic-08.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-pool-view/main-pic-09.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-pool-view/main-pic-09.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-pool-view/main-pic-10.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-pool-view/main-pic-10.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-pool-view/main-pic-11.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-pool-view/main-pic-11.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-pool-view/main-pic-12.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-pool-view/main-pic-12.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-village/main-pic-01.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-village/main-pic-01.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-village/main-pic-02.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-village/main-pic-02.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-village/main-pic-03.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-village/main-pic-03.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-village/main-pic-04.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-village/main-pic-04.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/boutique-village/main-pic-05.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/boutique-village/main-pic-05.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/rasa-family-suite/main-pic-01.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/rasa-family-suite/main-pic-01.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/rasa-family-suite/main-pic-02.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/rasa-family-suite/main-pic-02.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/rasa-family-suite/main-pic-03.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/rasa-family-suite/main-pic-03.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/rasa-family-suite/main-pic-04.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/rasa-family-suite/main-pic-04.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/rasa-family-suite/main-pic-05.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/rasa-family-suite/main-pic-05.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/rasa-villa/main-pic-01.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/rasa-villa/main-pic-01.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/rasa-villa/main-pic-02.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/rasa-villa/main-pic-02.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/rasa-villa/main-pic-03.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/rasa-villa/main-pic-03.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    <a href="../images/accommodation/rasa-villa/main-pic-04.jpg" class="element-item accommodation"><img class="img-cover" src="../images/accommodation/rasa-villa/main-pic-04.jpg" title="Highlights" alt="homestay in Pai"></a>
                                    
                                    <a href="../images/gallery/dining/main-pic-01.jpg" class="element-item dining"><img class="img-cover" src="../images/gallery/dining/main-pic-01.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/dining/main-pic-02.jpg" class="element-item dining"><img class="img-cover" src="../images/gallery/dining/main-pic-02.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/dining/main-pic-03.jpg" class="element-item dining"><img class="img-cover" src="../images/gallery/dining/main-pic-03.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/dining/main-pic-04.jpg" class="element-item dining"><img class="img-cover" src="../images/gallery/dining/main-pic-04.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/dining/main-pic-05.jpg" class="element-item dining"><img class="img-cover" src="../images/gallery/dining/main-pic-05.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/dining/main-pic-06.jpg" class="element-item dining"><img class="img-cover" src="../images/gallery/dining/main-pic-06.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/dining/main-pic-07.jpg" class="element-item dining"><img class="img-cover" src="../images/gallery/dining/main-pic-07.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/dining/main-pic-08.jpg" class="element-item dining"><img class="img-cover" src="../images/gallery/dining/main-pic-08.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/dining/main-pic-09.jpg" class="element-item dining"><img class="img-cover" src="../images/gallery/dining/main-pic-09.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/dining/main-pic-10.jpg" class="element-item dining"><img class="img-cover" src="../images/gallery/dining/main-pic-10.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/dining/main-pic-11.jpg" class="element-item dining"><img class="img-cover" src="../images/gallery/dining/main-pic-11.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/dining/main-pic-12.jpg" class="element-item dining"><img class="img-cover" src="../images/gallery/dining/main-pic-12.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/dining/main-pic-13.jpg" class="element-item dining"><img class="img-cover" src="../images/gallery/dining/main-pic-13.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/dining/main-pic-14.jpg" class="element-item dining"><img class="img-cover" src="../images/gallery/dining/main-pic-14.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/dining/main-pic-15.jpg" class="element-item dining"><img class="img-cover" src="../images/gallery/dining/main-pic-15.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/dining/main-pic-16.jpg" class="element-item dining"><img class="img-cover" src="../images/gallery/dining/main-pic-16.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/dining/main-pic-17.jpg" class="element-item dining"><img class="img-cover" src="../images/gallery/dining/main-pic-17.jpg" title="Dining" alt="Pai Dining"></a>
                                    
                                    <!-- <a href="../images/gallery/leisure/main-pic-01.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-01.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/leisure/main-pic-02.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-02.jpg" title="Dining" alt="Pai Dining"></a> -->
                                    <a href="../images/gallery/leisure/main-pic-03.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-03.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/leisure/main-pic-04.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-04.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/leisure/main-pic-05.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-05.jpg" title="Dining" alt="Pai Dining"></a>
                                    <!-- <a href="../images/gallery/leisure/main-pic-06.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-06.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/leisure/main-pic-07.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-07.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/leisure/main-pic-08.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-08.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/leisure/main-pic-09.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-09.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/leisure/main-pic-10.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-10.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/leisure/main-pic-11.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-11.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/leisure/main-pic-12.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-12.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/leisure/main-pic-13.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-13.jpg" title="Dining" alt="Pai Dining"></a> -->
                                    <!-- <a href="../images/gallery/leisure/main-pic-14.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-14.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/leisure/main-pic-15.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-15.jpg" title="Dining" alt="Pai Dining"></a> -->
                                    <!-- <a href="../images/gallery/leisure/main-pic-16.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-16.jpg" title="Dining" alt="Pai Dining"></a> -->
                                    <a href="../images/gallery/leisure/main-pic-17.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-17.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/leisure/main-pic-18.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-18.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/leisure/main-pic-19.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-19.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/leisure/main-pic-20.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-20.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/leisure/main-pic-21.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-21.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/leisure/main-pic-22.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-22.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/leisure/main-pic-23.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-23.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/leisure/main-pic-24.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-24.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/leisure/main-pic-25.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-25.jpg" title="Dining" alt="Pai Dining"></a>
                                    <!-- <a href="../images/gallery/leisure/main-pic-26.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-26.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/leisure/main-pic-27.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-27.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/leisure/main-pic-28.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-28.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/leisure/main-pic-29.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-29.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/leisure/main-pic-30.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-30.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/leisure/main-pic-31.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-31.jpg" title="Dining" alt="Pai Dining"></a> -->
                                    <!-- <a href="../images/gallery/leisure/main-pic-32.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-32.jpg" title="Dining" alt="Pai Dining"></a> -->
                                    <!-- <a href="../images/gallery/leisure/main-pic-33.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-33.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/leisure/main-pic-34.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-34.jpg" title="Dining" alt="Pai Dining"></a>
                                    <a href="../images/gallery/leisure/main-pic-35.jpg" class="element-item leisure"><img class="img-cover" src="../images/gallery/leisure/main-pic-35.jpg" title="Dining" alt="Pai Dining"></a> -->
                                    
                                    <a href="../images/tour/01.jpg" class="element-item activities"><img class="img-cover" src="../images/tour/thumb/01.jpg" title="Tour & Excursions" alt="Pai activities"></a>
                                    <a href="../images/tour/02.jpg" class="element-item activities"><img class="img-cover" src="../images/tour/thumb/02.jpg" title="Tour & Excursions" alt="outdoor activities Pai"></a>
                                    <a href="../images/tour/03.jpg" class="element-item activities"><img class="img-cover" src="../images/tour/thumb/03.jpg" title="Tour & Excursions" alt="Pai tour experience"></a>
                                    <a href="../images/tour/04.jpg" class="element-item activities"><img class="img-cover" src="../images/tour/thumb/04.jpg" title="Tour & Excursions" alt="tour experience Pai"></a>
                                    <a href="../images/tour/05.jpg" class="element-item activities"><img class="img-cover" src="../images/tour/thumb/05.jpg" title="Tour & Excursions" alt="best activities Pai"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <?php include_once '_footer.php'; ?>