<?php
$html_class = '';
$body_class = 'page-massage';
$cur_page   = 'massage';
$par_page   = '';
$title      = 'Pai Village Boutique Resort & Farm | Traditional Thai Spa';
$desc       = 'Exclusive Offers available on Official Site ONLY. Best Rates Guaranteed. Member Deal. Private Villa and Cottage, located along the banks of Pai River featuring Rustic Bamboo Cottage set among widing pathways.';
$keyw       = 'Rasa,Hospitality,Rasa Tower,Management,Development,Business Management,Business Development,Accounting,Manager,Hotel,Resort,Investment,,Buri Rasa,Rasa Collection,Boutique,Traditional, Pai, Mae Hong Son,Thailand';
$local      = 'en-US';
$page_url   = 'massage.php';
$ogimage    = ['../images/massage/10.jpg', '1500', '1000'];

$lang_zh    = $page_url;
$lang_en    = '../'. $page_url;
$lang_th    = 'th/'. $page_url;
include_once '_header.php' ?>
        <main class="site-main">
            <div class="swiper-container swiper-main">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img class="img-cover lazy-load" alt="Pai Village" data-src="../images/massage/10.jpg">
                    </div>
                </div>
                <?php include 'inc/scroll-down.php'; ?>
            </div>
            <section id="section" class="intro">
                <div class="bg-texture mountain">
                    <div class="container py-5">
                        <h1 class="header text-center mb-3 mb-md-5" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="100">按摩</h1>
                        <div class="row py-4">
                            <div class="col-12 col-md-9 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">我们的水疗中心是和当地的一位传奇阿嬷Lon合作的，她是夜丰颂府第一个练习泰式传统按摩的人。为了保证提供最高质量的水疗服务，阿嬷Lon亲自种植水疗香草, 调制出最具原始功效的植物精油，只为给您一个最正宗的泰式香草按摩。</p>
                            </div>
                            <div class="col-12 col-md-3 py-3 d-flex justify-content-center align-items-start" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <div class="box-information">
                                    <span class="d-block mb-2">时候营业</span>
                                    <span class="d-block main-color">早上9点到晚上8点</span>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-slider" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">
                            <h2 class="subject mb-4">相册 :</h2>
                            <div class="swiper-container swiper-gallery">
                                <div class="swiper-wrapper gallery-popup">
                                    <a class="swiper-slide" href="../images/massage/05.jpg"><img src="../images/massage/thumb/05.jpg" alt="Pai best massage" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="../images/massage/06.jpg"><img src="../images/massage/thumb/06.jpg" alt="traditional Thai massage Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="../images/massage/07.jpg"><img src="../images/massage/thumb/07.jpg" alt="Spa hotel Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="../images/massage/01.jpg"><img src="../images/massage/thumb/01.jpg" alt="spa resort Pai" width="1500" height="843"></a>
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <?php include_once '_footer.php'; ?>