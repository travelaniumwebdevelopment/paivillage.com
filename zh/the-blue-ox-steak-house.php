<?php
$html_class = '';
$body_class = 'page-dining';
$cur_page   = 'the-blue-ox-steak-house';
$par_page   = 'dining';
$title      = 'Pai Village Boutique Resort & Farm | The Blue Ox Steak House';
$desc       = 'Exclusive Offers available on Official Site ONLY. Best Rates Guaranteed. Member Deal. Private Villa and Cottage, located along the banks of Pai River featuring Rustic Bamboo Cottage set among widing pathways.';
$keyw       = 'Rasa,Hospitality,Rasa Tower,Management,Development,Business Management,Business Development,Accounting,Manager,Hotel,Resort,Investment,,Buri Rasa,Rasa Collection,Boutique,Traditional, Pai, Mae Hong Son,Thailand';
$local      = 'en-US';
$page_url   = 'the-blue-ox-steak-house.php';
$ogimage    = ['../images/dining/the-blue-ox-steak-house/main-slide-01.jpg', '1500', '1000'];

$lang_zh    = $page_url;
$lang_en    = '../'. $page_url;
$lang_th    = 'th/'. $page_url;
include_once '_header.php' ?>
        <main class="site-main">
            <div class="swiper-container swiper-main">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img class="img-cover lazy-load" alt="Pai Village" data-src="../images/dining/the-blue-ox-steak-house/main-slide-01.jpg">
                    </div>
                </div>
                <?php include '../inc/scroll-down.php'; ?>
            </div>
            <section id="section" class="intro">
                <div class="bg-texture mountain">
                    <div class="container text-center py-5">
                        <img src="../assets/elements/logo-b-ox.png" alt="Pai Village" width="275" height="87" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="100">
                        <h1 class="header text-center mb-3 mb-md-5" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="100"><?php echo _dining_name_1; ?></h1>
                        <div class="row py-4 text-left">
                            <div class="col-12 col-md-9 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">一日之计在于晨，全天候餐厅以精美丰富的早餐拉开帷幕，紧接着您可以在正午十分享用最具有特色的泰式和欧式午餐。随着夜幕的降临，我们的餐厅将变换成一家拜县很有名的牛仔餐厅。在国际大厨Chef Joseph的领导下，牛排馆以繁多的本地及进口牛排，精致的意大利面，新鲜沙拉，烘培面包及各种特制鸡尾酒为特色，再加上每日的演艺活动，这注定是一顿饕餮盛宴。</p>
                            </div>
                            <div class="col-12 col-md-3 py-3 d-flex justify-content-center align-items-start" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <div class="box-information">
                                    <span class="d-block mb-2">时候营业</span>
                                    <span class="d-block main-color">早上5.30点到晚上10.30点</span>
                                    <span class="d-block main-color">(最后的订单 ：晚上10点)</span>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-slider" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">
                            <h2 class="subject mb-4 text-left">相册 :</h2>
                            <div class="swiper-container swiper-gallery">
                                <div class="swiper-wrapper gallery-popup">
                                    <a class="swiper-slide" href="../images/dining/the-blue-ox-steak-house/main-pic-01.jpg"><img src="../images/dining/the-blue-ox-steak-house/thumb/main-pic-01.jpg" alt="best restaurant Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="../images/dining/the-blue-ox-steak-house/main-pic-02.jpg"><img src="../images/dining/the-blue-ox-steak-house/thumb/main-pic-02.jpg" alt="best steak house Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="../images/dining/the-blue-ox-steak-house/main-pic-03.jpg"><img src="../images/dining/the-blue-ox-steak-house/thumb/main-pic-03.jpg" alt="Pai best steak house" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="../images/dining/the-blue-ox-steak-house/main-pic-04.jpg"><img src="../images/dining/the-blue-ox-steak-house/thumb/main-pic-04.jpg" alt="Pai best restaurant" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="../images/dining/the-blue-ox-steak-house/main-pic-05.jpg"><img src="../images/dining/the-blue-ox-steak-house/thumb/main-pic-05.jpg" alt="dining experience Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="../images/dining/the-blue-ox-steak-house/main-pic-06.jpg"><img src="../images/dining/the-blue-ox-steak-house/thumb/main-pic-06.jpg" alt="lunch restaurant Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="../images/dining/the-blue-ox-steak-house/main-pic-07.jpg"><img src="../images/dining/the-blue-ox-steak-house/thumb/main-pic-07.jpg" alt="Pai best breakfast" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="../images/dining/the-blue-ox-steak-house/main-pic-08.jpg"><img src="../images/dining/the-blue-ox-steak-house/thumb/main-pic-08.jpg" alt="amazing dining experience Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="../images/dining/the-blue-ox-steak-house/main-pic-09.jpg"><img src="../images/dining/the-blue-ox-steak-house/thumb/main-pic-09.jpg" alt="popular dinner restaurants Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="../images/dining/the-blue-ox-steak-house/main-pic-10.jpg"><img src="../images/dining/the-blue-ox-steak-house/thumb/main-pic-10.jpg" alt="breakfast buffet Pai" width="1500" height="843"></a>
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <?php include_once '_footer.php'; ?>