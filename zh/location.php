<?php
$html_class = '';
$body_class = 'page-location';
$cur_page   = 'location';
$par_page   = '';
$title      = 'Pai Village Boutique Resort and Farm | Boutique Resort';
$desc       = 'Exclusive Offers available on Official Site ONLY. Best Rates Guaranteed. Member Deal. Private Villa and Cottage, located along the banks of Pai River featuring Rustic Bamboo Cottage set among widing pathways.';
$keyw       = 'Rasa,Hospitality,Rasa Tower,Management,Development,Business Management,Business Development,Accounting,Manager,Hotel,Resort,Investment,,Buri Rasa,Rasa Collection,Boutique,Traditional, Pai, Mae Hong Son,Thailand';
$local      = 'en-US';
$page_url   = 'location.php';

$lang_zh    = $page_url;
$lang_en    = '../'. $page_url;
$lang_th    = 'th/'. $page_url;
include_once '_header.php' ?>
        <main class="site-main">
            <div class="swiper-container" id="google_map_carousel">
                <div class="swiper-wrapper">
                    <div class="swiper-slide" data-hash="hotel">
                        <iframe width="100%" height="100%" frameborder="0" style="border:0" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3764.2470646651977!2d98.444834!3d19.358452!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa6e5ed82e6905a9a!2sPai+Village!5e0!3m2!1sen!2sth!4v1561633344595!5m2!1sen!2sth" allowfullscreen></iframe>
                    </div>
                    <div class="swiper-slide" data-hash="transport">
                        <iframe width="100%" height="100%" frameborder="0" style="border:0" src="https://www.google.com/maps/embed?pb=!1m28!1m12!1m3!1d7528.2608544987415!2d98.4357550263741!3d19.363504419663265!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m13!3e6!4m5!1s0x30da81cdba3fa25b%3A0x9a5b7b98afe2bf17!2sPai+Airport+(PYY)%2C+Wiang+Tai%2C+Pai+District%2C+Mae+Hong+Son!3m2!1d19.370172099999998!2d98.4361356!4m5!1s0x30da80323a98248b%3A0xa6e5ed82e6905a9a!2zUGFpIFZpbGxhZ2UsIDg4IOC4q-C4oeC4ueC5iOC4l-C4teC5iCAzIFdpYW5nIFRhaSwgQW1waG9lIFBhaSwgQ2hhbmcgV2F0IE1hZSBIb25nIFNvbiA1ODEzMA!3m2!1d19.358451799999997!2d98.44483389999999!5e0!3m2!1sen!2sth!4v1561633389884!5m2!1sen!2sth" allowfullscreen></iframe>
                    </div>
                    <div class="swiper-slide" data-hash="shopping">
                        <iframe width="100%" height="100%" frameborder="0" style="border:0" src="https://www.google.com/maps/embed?pb=!1m12!1m8!1m3!1d15056.98601672939!2d98.438267897885!3d19.358476281644197!3m2!1i1024!2i768!4f13.1!2m1!1sshopping!5e0!3m2!1sen!2sth!4v1561633246635!5m2!1sen!2sth" allowfullscreen></iframe>
                    </div>
                </div>
                <nav class="carousel-nav" id="google_map_nav">
                    <ul class="pl-0 mb-0">
                        <li><a href="#hotel">酒店地图</a></li>
                        <li><a href="#transport">酒店到机场</a></li>
                        <li><a href="#shopping">酒店地图</a></li>
                    </ul>
                </nav>
            </div>
            <section id="section" class="intro">
                <div class="bg-texture mountain">
                    <div class="container py-5">
                        <h1 class="header text-center mb-3 mb-md-5" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="100">地理位置</h1>
                        <div class="row py-4">
                            <div class="col-12 col-md-6 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">
                                <img class="img-cover" src="../images/location/main-pic-01.jpg" alt="Pai Village" width="636" height="604">
                            </div>
                            <div class="col-12 col-md-6 py-3 d-flex align-items-center" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <div class="box-destination">
                                    <div class="header">
                                        <h3 class="title">飞机场</h3>
                                    </div>
                                    <div class="d-flex py-3">
                                        <span class="d-block">拜县机场 </span>
                                        <span class="d-block ml-auto">1.8 千米</span>
                                    </div>
                                    <div class="d-flex py-3">
                                        <span class="d-block">清迈机场</span>
                                        <span class="d-block ml-auto">135 千米</span>
                                    </div>
                                    <div class="header">
                                        <h3 class="title">最受欢迎的地标</h3>
                                    </div>
                                    <div class="d-flex py-3">
                                        <span class="d-block">拜县的步行街</span>
                                        <span class="d-block ml-auto">170 米</span>
                                    </div>
                                    <div class="d-flex py-3">
                                        <span class="d-block">夜丰颂城 </span>
                                        <span class="d-block ml-auto">650 米</span>
                                    </div>
                                    <div class="d-flex py-3">
                                        <span class="d-block">拜县的汽车站</span>
                                        <span class="d-block ml-auto">5 千米</span>
                                    </div>
                                    <div class="d-flex py-3">
                                        <span class="d-block">拜县的瀑布 </span>
                                        <span class="d-block ml-auto">8.7 千米</span>
                                    </div>
                                    <div class="d-flex py-3">
                                        <span class="d-block">Boon Ko Ku So桥</span>
                                        <span class="d-block ml-auto">11.2 千米</span>
                                    </div>
                                    <div class="d-flex py-3">
                                        <span class="d-block">拜县的纪念桥</span>
                                        <span class="d-block ml-auto">10.2 千米</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="250">
                                <h2 class="subject"><b>“运输”</b></h2>
                                <p class="details mb-3">乘坐酒店汽车在清迈机场接机，并护送到度假村或从度假村前往清迈机场，享受绝佳的 <br>便利。 我们的私人接送和共享接送服务可应要求提供。</p>
                                <a target="_blank" href="../downloads/PVB-Transfer-Factsheet.pdf"><h2 class="subject"><u>下载转移资料单</u></h2></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <?php include_once '_footer.php'; ?>