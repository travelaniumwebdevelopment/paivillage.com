<?php
$html_class = '';
$body_class = 'page-farm';
$cur_page   = 'farm';
$par_page   = '';
$title      = 'Pai Village Boutique Resort & Farm | Village Farm';
$desc       = 'Exclusive Offers available on Official Site ONLY. Best Rates Guaranteed. Member Deal. Private Villa and Cottage, located along the banks of Pai River featuring Rustic Bamboo Cottage set among widing pathways.';
$keyw       = 'Rasa,Hospitality,Rasa Tower,Management,Development,Business Management,Business Development,Accounting,Manager,Hotel,Resort,Investment,,Buri Rasa,Rasa Collection,Boutique,Traditional, Pai, Mae Hong Son,Thailand';
$local      = 'en-US';
$page_url   = 'farm.php';
$ogimage    = ['../images/farm/06.jpg', '1500', '1000'];

$lang_zh    = $page_url;
$lang_en    = '../'. $page_url;
$lang_th    = 'th/'. $page_url;
include_once '_header.php' ?>
        <main class="site-main">
            <div class="swiper-container swiper-main">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img class="img-cover lazy-load" alt="Pai Village" data-src="../images/farm/06.jpg" width="1500" height="1000">
                    </div>
                </div>
                <?php include 'inc/scroll-down.php'; ?>
            </div>
            <section id="section" class="intro">
                <div class="bg-texture mountain">
                    <div class="container py-5">
                        <h1 class="header text-center mb-3 mb-md-5" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="100">度假村农场</h1>
                        <div class="row py-4">
                            <div class="col-12 col-md-9 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">农场距离度假村和拜县城中心不到5分钟的车程。在这里您将会得到一个欣赏奇花异草，近距离接触小动物的特别体验。同时您也能享受到当地的特色咖啡和小食，并且还有一个纪念商品店可供您为家人或朋友挑选伴手礼。</p>
                                <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">农场是您取景留念的绝佳场地，记载下您在拜县最难忘的回忆瞬间。</p>
                                <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">作为拜县乡村精品农场度假酒店的客人，您可免费进入农场并请尽情的享受那满园的鸟语花香。</p>
                                <div class="text-left py-3">
                                    <a class="btn btn-radius-main-color" href="../downloads/HomeStay-and-Farm-Walking-Map.pdf">Download Map</a>
                                </div>
                            </div>
                            <div class="col-12 col-md-3 py-3 d-flex justify-content-center align-items-start" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <div class="box-information">
                                    <span class="d-block mb-2">时候营业</span>
                                    <span class="d-block main-color">早上9点到晚上4点</span>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-slider" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">
                            <h2 class="subject mb-4">相册 :</h2>
                            <div class="swiper-container swiper-gallery">
                                <div class="swiper-wrapper gallery-popup">
                                    <a class="swiper-slide" href="../images/farm/01.jpg"><img src="../images/farm/thumb/01.jpg" alt="selling organic fruits Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="../images/farm/13.jpg"><img src="../images/farm/thumb/13.jpg" alt="relaxed atmosphere Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="../images/farm/14.jpg"><img src="../images/farm/thumb/14.jpg" alt="relaxed atmosphere Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="../images/farm/02.jpg"><img src="../images/farm/thumb/02.jpg" alt="relaxed atmosphere Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="../images/farm/03.jpg"><img src="../images/farm/thumb/03.jpg" alt="stunning landscape Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="../images/farm/04.jpg"><img src="../images/farm/thumb/04.jpg" alt="selling fresh products Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="../images/farm/05.jpg"><img src="../images/farm/thumb/05.jpg" alt="farm and fresh Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="../images/farm/12.jpg"><img src="../images/farm/thumb/12.jpg" alt="beautiful garden Pai" width="1500" height="843"></a>
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <?php include_once '_footer.php'; ?>