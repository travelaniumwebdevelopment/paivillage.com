<?php
$html_class = '';
$body_class = 'page-tour-excursion';
$cur_page   = 'tour-excursion';
$par_page   = '';
$title      = 'Pai Village Boutique Resort and Farm | Tours In Pai';
$desc       = 'Exclusive Offers available on Official Site ONLY. Best Rates Guaranteed. Member Deal. Private Villa and Cottage, located along the banks of Pai River featuring Rustic Bamboo Cottage set among widing pathways.';
$keyw       = 'Rasa,Hospitality,Rasa Tower,Management,Development,Business Management,Business Development,Accounting,Manager,Hotel,Resort,Investment,,Buri Rasa,Rasa Collection,Boutique,Traditional, Pai, Mae Hong Son,Thailand';
$local      = 'en-US';
$page_url   = 'tour-excursion.php';
$ogimage    = ['../images/tour/01.jpg', '1500', '1000'];

$lang_zh    = $page_url;
$lang_en    = '../'. $page_url;
$lang_th    = 'th/'. $page_url;
include_once '_header.php' ?>
        <main class="site-main">
            <div class="swiper-container swiper-main">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img class="img-cover lazy-load" alt="Pai Village" data-src="../images/tour/04.jpg">
                    </div>
                </div>
                <?php include 'inc/scroll-down.php'; ?>
            </div>
            <section id="section" class="intro">
                <div class="bg-texture mountain">
                    <div class="container py-5">
                        <h1 class="header text-center mb-3 mb-md-5" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="100">休闲活动</h1>
                        <div class="row py-4">
                            <div class="col-12 col-md-9 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">拜县，是一个你既可以体验到各种不同的休闲活动来充实你一天的地方，也是一个你可以什么都不用做，静静的让灵魂与这个天堂般的花园融合的地方。无论你是选择喝着咖啡，静静的坐在吊床上悠闲的读着一本自己喜欢的书，一晃一整天，或是骑着自行车漫游整个小镇；体验水上漂流；做泰式按摩；和小伙伴坐浪漫的马车；在当地酒吧小酌一杯;在艺术咖啡店或是当地市场挑选一些精致的小玩意儿---都会令你爱上拜县这独特的生活节奏，和当地人文韵味。</p>
                            </div>
                            <div class="col-12 col-md-3 py-3 d-flex justify-content-center align-items-start" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <div class="box-information">
                                    <span class="d-block mb-2">要预订或了解更多信息，请联系</span>
                                    <span class="d-block">邮箱: <a class="main-color" href="mailto:reservations@paivillage.com">reservations@paivillage.com</a></span>
                                    <span class="d-block">电话: <a class="main-color" href="tel:+6653698152">+66 5369 8152</a></span>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-slider" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">
                            <h2 class="subject mb-4">相册 :</h2>
                            <div class="swiper-container swiper-gallery">
                                <div class="swiper-wrapper gallery-popup">
                                    <a class="swiper-slide" href="../images/tour/04.jpg"><img src="../images/tour/thumb/04.jpg" alt="Pai Village" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="../images/tour/05.jpg"><img src="../images/tour/thumb/05.jpg" alt="Pai Village" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="../images/tour/01.jpg"><img src="../images/tour/thumb/01.jpg" alt="Pai Village" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="../images/tour/02.jpg"><img src="../images/tour/thumb/02.jpg" alt="Pai Village" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="../images/tour/03.jpg"><img src="../images/tour/thumb/03.jpg" alt="Pai Village" width="1500" height="843"></a>
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <?php include_once '_footer.php'; ?>