                <div class="tl-side-date">
                    <div class="tl-side-date-pos">
                        <div class="collapse show" id="collapseLocalTime">
                            <div class="tl-side-date-data">
                                <span class="day-string" id="local-time"></span>
                                <span class="day-string" id="local-date"></span>
                            </div>
                        </div>
                    </div>
                </div>