        <div class="booking-panel-bg"></div>
        <div class="booking-panel" data-simplebar>
            <div class="booking-panel-wrapper">
                <div class="site-reservation">
                    <div class="my-4 d-flex justify-content-center"><h3 class="mb-0 mt-3">MAKE A RESERVATION</h3></div>
                    <form id="form_booking" class="notranslate">
                        <div class="row gutters-5">
                            <div class="col-8 px-0">
                                <div class="t-datepicker datepicker-panel">
                                    <div class="col pr-0">
                                        <div class="block form-group mb-2">
                                            <label for="checkin">Check-in</label>
                                            <div class="t-check-in control control-datepicker"></div>
                                            <span class="bottom"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                                            <input type="hidden" name="checkin" required>
                                        </div>
                                    </div>
                                    <div class="col px-1">
                                        <div class="block form-group mb-2">
                                            <label for="checkout">Check-out</label>
                                            <div class="t-check-out control control-datepicker"></div>
                                            <span class="bottom"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                                            <input type="hidden" name="checkout" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4 pl-0">
                                <div class="block form-group mb-4">
                                    <label for="room">Rooms</label>
                                    <div class="control control-room" id="room_display">
                                        <span class="val value">1</span>
                                        <span class="top value-up" tabindex="0"><i class="fa fa-angle-up" aria-hidden="true"></i></span>
                                        <span class="bottom value-down" tabindex="0"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                                        <input type="hidden" name="numofroom" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-3 pr-0">
                                <div class="block form-group">
                                    <label for="adult">Adult(s)</label>
                                    <select name="numofadult" id="adult" class="form-control custom-select" required>
                                        <option value="1">1</option>
                                        <option value="2" selected>2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-3 px-1">
                                <div class="block form-group">
                                    <label for="adult">Child(ren)</label>
                                    <select name="numofchild" id="child" class="form-control custom-select" required>
                                        <option value="0" selected>0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                    <span class="note">5-11 years</span>
                                </div>
                            </div>
                            <div class="col-6 pl-0">
                                <div class="block form-group">
                                    <label for="accesscode" style="color: transparent;">Promo Code</label>
                                    <input type="text" name="accesscode" id="accesscode" class="form-control" placeholder="Promo Code" style="height:42.5px;">
                                </div>
                            </div>
                            <div class="col-12 mt-2">
                                <button type="submit" class="btn btn-block btn-checkavailability text-uppercase">Check Availability</button>
                            </div>
                        </div>
                    </form>
                </div>
                <button class="booking-panel-close" data-booking-panel="hide"><i class="fal fa-times"></i></button>
            </div>
        </div>