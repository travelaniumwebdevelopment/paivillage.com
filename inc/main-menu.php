                            <li class="menu-item menu-item-has-children">
                                <a class="<?php echo get_current_class('accommodation'); ?>" href="accommodation.php">Accommodation</a>
                                <ul class="sub-menu">
                                    <li><a href="deluxe-garden.php" class="<?php echo get_current_class('deluxe-garden'); ?>"><?php echo _room_name_3; ?></a></li>
                                    <li><a href="deluxe-village.php" class="<?php echo get_current_class('deluxe-village'); ?>"><?php echo _room_name_2; ?></a></li>
                                    <li><a href="boutique-grand-village.php" class="<?php echo get_current_class('boutique-grand-village'); ?>"><?php echo _room_name_7; ?></a></li>

                                    <li><a href="grand-village.php" class="<?php echo get_current_class('grand-village'); ?>"><?php echo 'Grand Village'; ?></a></li>
                                    
                                    <li><a href="rasa-family-suite.php" class="<?php echo get_current_class('rasa-family-suite'); ?>"><?php echo _room_name_1; ?></a></li>
                                    <li><a href="deluxe-pool-view.php" class="<?php echo get_current_class('deluxe-pool-view'); ?>"><?php echo _room_name_5; ?></a></li>
                                    <li><a href="boutique-pool-view.php" class="<?php echo get_current_class('boutique-pool-view'); ?>"><?php echo _room_name_6; ?></a></li>

                                    

                                    <li><a href="deluxe-villa.php" class="<?php echo get_current_class('deluxe-villa'); ?>"><?php echo _room_name_4; ?></a></li>

                                    
                                </ul>
                            </li>
                            <li class="menu-item menu-item-has-children">
                                <a class="<?php echo get_current_class('dining'); ?>" href="the-blue-ox-steak-house.php">Dining</a>
                                <ul class="sub-menu">
                                    <li><a href="the-blue-ox-steak-house.php" class="<?php echo get_current_class('the-blue-ox-steak-house'); ?>"><?php echo _dining_name_1; ?></a></li>
                                    <li><a href="rabbit-cafe.php" class="<?php echo get_current_class('rabbit-cafe'); ?>"><?php echo _dining_name_2; ?></a></li>
                                </ul>
                            </li>
                            <li class="menu-item"><a class="<?php echo get_current_class('massage'); ?>" href="massage.php">Massage</a></li>
                            <li class="menu-item"><a class="<?php echo get_current_class('tour-excursion'); ?>" href="tour-excursion.php">Tours &amp; Excursions</a></li>
                            <li class="menu-item"><a class="<?php echo get_current_class('gallery'); ?>" href="gallery.php">Gallery</a></li>
                            <li class="menu-item"><a class="<?php echo get_current_class('offers'); ?>" href="offers.php">Offers</a></li>
                            <li class="menu-item"><a class="<?php echo get_current_class('location'); ?>" href="location.php">Location</a></li>
                            <li class="menu-item"><a class="<?php echo get_current_class('contact'); ?>" href="contact.php">Contact</a></li>