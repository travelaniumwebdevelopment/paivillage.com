<div class="member-panel" id="member_panel">
    <div class="guest-ui">
        <div class="member-panel-inner">
            <div class="member-panel-title">
                <button class="button" data-member-panel>
                    <span class="text">SIGN UP TO GET SECRET DEAL</span>
                </button>
            </div>
            <div class="member-panel-content">
                <h2 class="h6 font-weight-bold">BENEFIT MEMBER</h2>
                <ul class="fa-ul" style="margin-left:1rem;font-size:.875rem;">
                    <li><i class="fas fa-check-circle main-color"></i> Book our lowest price here</li>
                    <!-- <li><i class="fas fa-check-circle main-color"></i> Members get instant 10% off best rate</li> -->
                    <li><i class="fas fa-check-circle main-color"></i> Fast, FREE WIFI</li>
                    <li><i class="fas fa-check-circle main-color"></i> Better booking policies</li>
                    <li><i class="fas fa-check-circle main-color"></i> Exclusive online offers</li>
                </ul>

                <div class="member-panel-form">
                    <form id="form_member">
                        <label for="mb_email">Gain Access to Exclusive Room Types and Rates after Sign Up</label>
                        <div class="form-group">
                            <input type="email" name="email" id="mb_email" class="form-control" placeholder="Your email address" data-validation="email" required>
                        </div>
                        <div>
                            <button class="btn btn-signup">SIGN UP</button>
                            <a href="javascript:;" class="btn-cancel ml-2" data-member-panel="hide">No thanks.</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="member-ui">
        <div class="member-panel-inner">
            <div class="member-panel-title">
                <a class="button" href="<?php ibe_url( get_info('ibeID'), get_info('site_lang') ); ?>" target="_blank">
                    <span class="text">Hi! Member, <small>You now can get secret deal</small></span>
                </a>
            </div>
        </div>
    </div>
</div>