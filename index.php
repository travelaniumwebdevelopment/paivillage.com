<?php
$html_class = '';
$body_class = 'page-home';
$cur_page   = 'home';
$par_page   = '';
$title      = 'Pai Village Boutique Resort and Farm | Boutique Resort';
$desc       = 'Exclusive Offers available on Official Site ONLY. Best Rates Guaranteed. Member Deal. Private Villa and Cottage, located along the banks of Pai River featuring Rustic Bamboo Cottage set among widing pathways.';
$keyw       = 'Rasa,Hospitality,Rasa Tower,Management,Development,Business Management,Business Development,Accounting,Manager,Hotel,Resort,Investment,,Buri Rasa,Rasa Collection,Boutique,Traditional, Pai, Mae Hong Son,Thailand';
$local      = 'en-US';
$page_url   = 'index.php';
$ogimage    = ['images/home/main-slide-001.jpg', '1500', '1000'];

$lang_en    = $page_url;
$lang_zh    = 'zh/'. $page_url;
$lang_th    = 'th/'. $page_url;
include_once '_header.php' ?>
        <main class="site-main">
            <div class="multiple-slider-container">
                <div class="swiper-container swiper-image">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide"><img class="img-cover" src="./images/home/new/Landing-Page-01.jpg" srcset="./images/home/new/Landing-Page-01-400w.jpg 414w, ./images/home/new/Landing-Page-01.jpg" alt="Pai Village" width="1500" height="1000"></div>
                        <div class="swiper-slide"><img class="img-cover" src="./images/home/new/Landing-Page-02.jpg" srcset="./images/home/new/Landing-Page-02-400w.jpg 414w, ./images/home/new/Landing-Page-02.jpg" alt="sustainable hotel Pai" width="1500" height="1000"></div>
                        <div class="swiper-slide"><img class="img-cover" src="./images/home/new/Landing-Page-03.jpg" srcset="./images/home/new/Landing-Page-03-400w.jpg 414w, ./images/home/new/Landing-Page-03.jpg" alt="sustainable hotel Pai" width="1500" height="1000"></div>
                        <div class="swiper-slide"><img class="img-cover" src="./images/home/new/Landing-Page-04.jpg" srcset="./images/home/new/Landing-Page-04-400w.jpg 414w, ./images/home/new/Landing-Page-04.jpg" alt="Pai best homestay" width="1500" height="1000"></div>
                        <div class="swiper-slide"><img class="img-cover" src="./images/home/new/Landing-Page-05.jpg" srcset="./images/home/new/Landing-Page-05-400w.jpg 414w, ./images/home/new/Landing-Page-05.jpg" alt="best hotel Pai" width="1500" height="1000"></div>
                        <div class="swiper-slide"><img class="img-cover" src="./images/home/new/Landing-Page-06.jpg" srcset="./images/home/new/Landing-Page-06-400w.jpg 414w, ./images/home/new/Landing-Page-06.jpg" alt="sustainable hotel Pai" width="1500" height="1000"></div>
                        <div class="swiper-slide"><img class="img-cover" src="./images/home/new/Landing-Page-07.jpg" srcset="./images/home/new/Landing-Page-07-400w.jpg 414w, ./images/home/new/Landing-Page-07.jpg" alt="sustainable hotel Pai" width="1500" height="1000"></div>
                    </div>
                    <div class="swiper-pagination"></div>
                    <div class="swiper-button-prev swiper-button-white"><i class="icon-tab fal fa-chevron-circle-left"></i></div>
                    <div class="swiper-button-next swiper-button-white"><i class="icon-tab fal fa-chevron-circle-right"></i></div>
                </div>
                <div class="swiper-container swiper-video hide">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide video-container">
                            <video class="video-cover lazy-load" id="intro_video" poster="./video/pai-village-720p.jpg" loop muted data-hd-src="./video/pai-village-720p.mp4" data-sd-src="./video/pai-village-360p.mp4">
                                Your browser does not support the video tag.
                            </video>
                        </div>
                    </div>
                </div>
                <div class="button-play-position show">
                    <a href="#" title="Image/Video" class="button button-options image"></a>
                    <a href="#" title="Mute/Muted" class="button button-mute hide"></a>
                    <a href="#" title="Play/Pause" class="button button-play hide"></a>
                </div>
                <!-- <?php include 'inc/local-time.php'; ?> -->
                <?php include 'inc/scroll-down.php'; ?>
            </div>
            <section id="section" class="intro">
                <div class="bg-noise mountain lazy-load" data-bg="url(./assets/elements/bg-texture-noise.png)">
                    <div class="container text-center py-5">
                        <h1 class="header mb-3 mb-md-5 pt-md-5" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="100">Welcome To Your Home Away From Home<br><small>Pai Village Boutique Resort, Pai - Thailand</small></h1>
                        <p class="intro-desc mb-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">The team at Pai Village Boutique Resort are happy to welcome you to our oasis of green gardens nestled in the center of Pai. Whether you join us for an evening, week or more you will be certain to find yourself relaxed and embraced by nature.</p>
                        <p class="intro-desc mb-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">Our award winning eco-resort is only steps away from walking street where you can take a comfortable evening stroll, shopping along the way and enjoying the unique nightlife for which Pai is famous. When you return to our resort, an intimate experience where everyone knows your name awaits; a home away from home where you are not just a guest but where you will be treated as family. Your every need will be catered too with our personal goal being your relaxing holiday and genuine happiness.</p>
                        <p class="intro-desc mb-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">Whether you choose one of our modern Thai Lanna Heritage rooms or one of our traditional Pai Hill Tribe Bungalows, you will relax knowing all guest rooms in our village are designed to provide boutique standards of comfort and design to enhance your stay with us in Pai. Whether it be relaxing in a hammock on your terrace or enjoying one of our signature swings or outdoor daybeds, comfort is assured. Our hideaway village also features a swimming pool, exclusively for guests, which is heated when the temperatures drop for enjoyment year-round. Complimentary access is also available to our local farm where you can enjoy a picturesque sunset, calming stream, flower filled walkways and a pond filled with colorful Koi fish and Thai carp.</p>
                        <p class="intro-desc pb-md-5" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">And as no great home would be complete without a great kitchen, we are happy to be home to Pai’s most popular restaurant, The Blue Ox. The resort transforms as day turns to night and the smells of fresh barbecue and Thai delicacies fill the air. Daily entertainment will add to the experience ensuring a night to remember under the direction of our head chef who was recently featured on Iron Chef Thailand.</p>
                        <p class="intro-desc pb-md-5" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">In reference to health and safety, you will be comforted to know we are operating under the concept ‘commitment to cleanliness & hygiene’ to ensure all guests are offered the highest level of cleanliness and safety possible.</p>
                        <p class="intro-desc pb-md-5" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">We hope you will join us in celebrating all that Pai has to offer and consider us more than just a boutique resort; We hope you consider us your home away from home while in the Pai Valley.</p>
                    </div>
                    <div class="row no-gutters py-md-2">
                        <div class="col-12 col-md-7 pr-md-2" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="100" itemscope itemtype="http://schema.org/Rooms">
                            <a title="Accommodation" href="accommodation.php" itemprop="url">
                                <div class="box-hover">
                                    <img class="img-cover lazy-load" data-src="./images/home/main-pic-01.jpg" alt="Pai Village" width="916" height="537">
                                    <div class="content">
                                        <div class="content-wrapper">
                                            <h2 class="title" itemprop="title">Sleep & Dream</h2>
                                            <p itemprop="description">Find the perfect, tranquil space to relax and dream</p>
                                            <div class="click-button">
                                                <span>ROOMS & AVAILABILITY</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-5 pl-md-2">
                            <div class="h-100" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">
                                <a href="massage.php">
                                    <div class="box-hover">
                                        <img class="img-cover lazy-load" data-src="./images/home/main-pic-02.jpg" alt="Pai Village" width="916" height="537">
                                        <div class="content">
                                            <div class="content-wrapper">
                                                <h2 class="title">Rejuvenate</h2>
                                                <p>Slow down, take a moment and relax</p>
                                                <div class="click-button">
                                                    <span>EXPERIENCE MORE</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row no-gutters py-md-2">
                        <div class="col-12 col-md-6 pr-md-2" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">
                            <a href="the-blue-ox-steak-house.php">
                                <div class="box-hover">
                                    <img class="img-cover lazy-load" data-src="./images/home/main-pic-04.jpg" alt="Pai Village" width="916" height="537">
                                    <div class="content">
                                        <div class="content-wrapper">
                                            <h2 class="title">Dine & Enjoy</h2>
                                            <p>Sit back and enjoy casual dining</p>
                                            <div class="click-button">
                                                <span>DISCOVER MORE</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-6 pl-md-2" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                            <a href="offers.php">
                                <div class="box-hover">
                                    <img class="img-cover lazy-load" data-src="./images/home/main-pic-05.jpg" alt="Pai Village" width="916" height="537">
                                    <div class="content">
                                        <div class="content-wrapper">
                                            <h2 class="title">Exclusive Savings</h2>
                                            <p>View our offers and promotions exclusively for you</p>
                                            <div class="click-button">
                                                <span>VIEW OUR SPECIALS</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="bg-noise lazy-load" data-bg="url(./assets/elements/bg-texture-noise.png)">
                    <div class="container py-5">
                        <div class="row">
                            <div class="col-12 col-md-7 py-3 order-md-2" data-aos="fade-right" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">
                                <img class="img-cover lazy-load" data-src="./images/home/pic-01.jpg" alt="Pai Village" width="765" height="519">
                            </div>
                            <div class="col-12 col-md-5 py-3 d-flex justify-content-center align-items-center order-md-1" data-aos="fade-left" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <a href="tour-excursion.php">
                                    <div class="content-highlight text-md-right">
                                        <h2 class="title font-italic">Mae Hong Son City</h2>
                                        <p>Pai is a place where you can do it all or simply do nothing at all.</p>
                                        <div class="click-button">
                                            <span>EXPLORE MORE</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-gray">
                    <div class="container py-5">
                        <div class="row py-md-5">
                            <div class="col-12 col-md-4 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="300">
                                <h2 class="subject main-color"><i class="fab fa-facebook-square" aria-hidden="true"></i> Facebook</h2>
                                <!--<div class="lazy-fb-page"></div>-->
                                <a href="https://www.facebook.com/paivillageresort/" target="_blank">
                                    <picture>
                                        <source srcset="./uploads/2024/01/pai_village_fb-600x600.jpg, ./uploads/2024/01/pai_village_fb-1200x1200.jpg 2x" />
                                        <img src="./uploads/2024/01/pai_village_fb.jpg" alt="Pai Village Facebook" width="3544" height="3544" loading="lazy" />
                                    </picture>
                                </a>
                            </div>

                            <div class="col-12 col-md-4 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="350">
                                <h2 class="subject main-color"><i class="fab fa-instagram" aria-hidden="true"></i> Instagram</h2>
                                <!--<aside id="instagram" class="ig-post">
                                    <span class="loading-indicator" style="border-color: transparent #e2dac4 #e2dac4;"></span>
                                    <ul class="ig-post-album ig-feed" data-instagram-feed=""></ul>
                                </aside>-->
                                <aside id="instagram" class="ig-post">
                                    <!--<script src="https://apps.elfsight.com/p/platform.js" defer></script>
                                    <div class="elfsight-app-aee05a6e-15b1-4db8-a93d-7be951abc6df"></div>-->
                                    <a href="<?php echo get_info('instagram'); ?>" target="_bank">
                                        <img src="images/PVB_IG.jpg" style="width:100%">
                                    </a>
                                </aside>
                            </div>

                            <div class="col-12 col-md-4 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="400">
                                <h2 class="subject main-color"><i class="fas fa-video" aria-hidden="true"></i> Video</h2>
                                <iframe class="lazy-load" width="100%" height="367" data-src="https://www.youtube.com/embed/SOR-oJZSkvg?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-texture inset-shadow lazy-load" data-bg="url(./assets/elements/bg-texture.png)">
                    <div class="container-fluid py-5">
                        <style>.list-awards-custom { gap: 20px; }</style>
                        <ul class="--list-awards list-awards-custom d-flex flex-wrap align-items-center justify-content-center" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="450">
                            <!-- <li><div style='text-align: center' itemscope itemtype="http://schema.org/Organization"><span style='display: none' itemprop="name">Pai Village Boutique Resort</span><div itemprop=aggregateRating itemscope itemtype="http://schema.org/AggregateRating"><a href='https://www.kayak.co.uk/Pai-Hotels-Pai-Village-Boutique-Resort-Farm.242838.ksp' target='_blank'><img width='130' height="101" src='https://content.r9cdn.net/seo-res/badges/v2/ORANGE_LARGE_BEST_RESORT_TH_en_GB.png' /></a><p style='color: black; font-size: 14px; font-family:Arial, Helvetica, sans-serif; margin: 4px 0;'>Traveller rating: <span itemprop="ratingvalue">7.7</span><meta itemprop="bestRating" content="10"></meta><meta itemprop="ratingCount" content='57'></meta> Good</p></div></div></li> -->
                            <!-- <li><img class="lazy-load" data-src="./assets/elements/awards/ORANGE_LARGE_BEST_RESORT.png" alt="Pai Village" width="130" height="101"></li> -->
                            <li><span class="d-inline-block" style="width:100px;"><img class="lazy-load" data-src="./assets/elements/awards/aw2.png" alt="Pai Village" width="105" height="98"></span></li>
                            <li><img class="lazy-load" data-src="./assets/elements/awards/aw4.png" alt="Pai Village" width="130" height="101"></li>
                            <li><img class="lazy-load" data-src="./assets/elements/awards/Logo-Cleantogether.png" alt="Pai Village" width="130" height="101"></li>
                            <li><span class="d-inline-block" style="width:110px;"><img class="lazy-load" data-src="./assets/elements/awards/MPH-LOGO.png" alt="Pai Village" width="130" height="101"></span></li>
                            <li><img class="lazy-load" data-src="./assets/elements/awards/SHA+.png" alt="Pai Village" width="150" height="auto"></li>
                            <li><span class="d-inline-block" style="width:90px;"><img class="lazy-load" data-src="./assets/elements/awards/SafeTravels.png" alt="Pai Village" width="101" height="101"></span></li>
                            <li><span class="d-inline-block" style="width:160px"><img class="lazy-load" data-src="./uploads/2024/03/stgs-horizontal.png" alt="STGS" width="1000" height="291"></span></li>
                        </ul>
                    </div>
                </div>
            </section>
        </main>
        <?php include_once '_footer.php'; ?>