<?php
$html_class = '';
$body_class = 'page-accommodation';
$cur_page   = 'grand-village';
$par_page   = 'accommodation';
$title      = 'Grand Village - Pai Village Boutique Resort & Farm';
$desc       = 'Exclusive Offers available on Official Site ONLY. Best Rates Guaranteed. Member Deal. Private Villa and Cottage, located along the banks of Pai River featuring Rustic Bamboo Cottage set among widing pathways.';
$keyw       = 'Rasa,Hospitality,Rasa Tower,Management,Development,Business Management,Business Development,Accounting,Manager,Hotel,Resort,Investment,,Buri Rasa,Rasa Collection,Boutique,Traditional, Pai, Mae Hong Son,Thailand';
$local      = 'en-US';
$page_url   = 'grand-village.php';
$ogimage    = ['images/accommodation/deluxe-village/01.jpg', '1500', '1000'];

$lang_en    = $page_url;
$lang_zh    = 'zh/'. $page_url;
$lang_th    = 'th/'. $page_url;
include_once '_header.php' ?>
        <main class="site-main">
            <div class="swiper-container swiper-main">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img class="img-cover lazy-load" alt="booking Pai hotel" data-src="images/accommodation/grand-village/grand-village-01.jpg">
                    </div>
                </div>
                <?php include 'inc/scroll-down.php'; ?>
            </div>
            <section id="section" class="intro">
                <div class="bg-texture mountain">
                    <div class="container py-5">
                        <h1 class="header text-center mb-3 mb-md-5" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="100"><?php echo 'Grand Village'; ?></h1>
                        <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">With more than 35 square meters of space, our Grand Village Room is located in the center of the resort, yet with an ample amount of privacy. Fitted with a king size bed, there is room for an extra bed to accommodate up to 3 guests.</p>
                        <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">This guest room is ideal for those seeking the charm of our Grand Village Room yet require a bit more space and exclusive privacy.”</p>
                        <div class="row py-4">
                            <div class="col-12 col-md-4 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <h2 class="subject"><b>Amenities</b></h2>
                                <ul class="pl-0">
                                    <li>Air Conditioning</li>
                                    <li>Bath Amenities</li>
                                    <li>Coffee & Tea Making Facilities</li>
                                    <li>Private Balcony or Terrace</li>
                                    <li>Complimentary Refreshment</li>
                                </ul>
                            </div>
                            <div class="col-12 col-md-4 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <h2 class="subject"><b>Services</b></h2>
                                <ul class="pl-0">
                                    <li>Turn Down Service</li>
                                    <li>Daily Housekeeping</li>
                                </ul>
                            </div>
                            <div class="col-12 col-md-4 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <h2 class="subject"><b>Information</b></h2>
                                <ul class="pl-0">
                                    <li>Indoor Space <?php echo _room_space_8; ?> Sq.m</li>
                                    <li>Allow up to <?php echo _room_guest_8; ?> guests</li>
                                </ul>
                            </div>
                            <div class="col-12 py-3 text-center" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="250">
                                <a class="btn btn-radius-main-color" href="https://reservation.travelanium.net/propertyibe2/rates?propertyId=253&onlineId=4&checkinauto=1&numofnight=1&numofadult=2&numofchild=0&numofroom=1&pid=MDcyMjkyM3wwNzQxNzUzfDA3NDQyOTR8MDc0NDM3NXwwNzQ0OTc0fDA3NDQ5NzV8MDc0NDk3NnwwNzQ0OTc3fDA3NDQ5Nzh8MDc0NTUwOA%3D%3D&rid=47d453a63bcdf4fe02d1efdd0f6913df">BOOK NOW</a>

                                <!--<a class="btn btn-radius-main-color" href="<?php echo ibe_url(get_info('ibeID'), get_info('site_lang')); ?>">BOOK NOW</a>-->
                            </div>
                        </div>
                        <div class="gallery-slider" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">
                            <h2 class="subject mb-4">Gallery :</h2>
                            <div class="swiper-container swiper-gallery">
                                <div class="swiper-wrapper gallery-popup">
                                    <a class="swiper-slide" href="./images/accommodation/grand-village/grand-village-01.jpg"><img src="./images/accommodation/grand-village/grand-village-01.jpg" alt="Pai best resort" width="1500" height="843"></a>                                    
                                    <a class="swiper-slide" href="./images/accommodation/grand-village/grand-village-02.jpg"><img src="./images/accommodation/grand-village/grand-village-02.jpg" alt="Pai best resort" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/accommodation/grand-village/grand-village-03.jpg"><img src="./images/accommodation/grand-village/grand-village-03.jpg" alt="Pai best resort" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/accommodation/grand-village/grand-village-04.jpg"><img src="./images/accommodation/grand-village/grand-village-04.jpg" alt="Pai best resort" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/accommodation/grand-village/grand-village-05.jpg"><img src="./images/accommodation/grand-village/grand-village-05.jpg" alt="Pai best resort" width="1500" height="843"></a>

                                    <a class="swiper-slide" href="./images/accommodation/grand-village/grand-village-06.jpg"><img src="./images/accommodation/grand-village/grand-village-07.jpg" alt="Pai best resort" width="1500" height="843"></a>

                                    <!-- <a class="swiper-slide" href="./images/IMG_2210-1-copy.jpg"><img src="./images/Untitledsd-1.jpg" alt="Pai best resort" width="1500" height="843"></a> -->
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <?php include_once '_footer.php'; ?>