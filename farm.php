<?php
$html_class = '';
$body_class = 'page-farm';
$cur_page   = 'farm';
$par_page   = '';
$title      = 'Pai Village Boutique Resort & Farm | Village Farm';
$desc       = 'Exclusive Offers available on Official Site ONLY. Best Rates Guaranteed. Member Deal. Private Villa and Cottage, located along the banks of Pai River featuring Rustic Bamboo Cottage set among widing pathways.';
$keyw       = 'Rasa,Hospitality,Rasa Tower,Management,Development,Business Management,Business Development,Accounting,Manager,Hotel,Resort,Investment,,Buri Rasa,Rasa Collection,Boutique,Traditional, Pai, Mae Hong Son,Thailand';
$local      = 'en-US';
$page_url   = 'farm.php';
$ogimage    = ['images/farm/06.jpg', '1500', '1000'];

$lang_en    = $page_url;
$lang_zh    = 'zh/'. $page_url;
$lang_th    = 'th/'. $page_url;
include_once '_header.php' ?>
        <main class="site-main">
            <div class="swiper-container swiper-main">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img class="img-cover" alt="beautiful garden Pai" src="images/farm/PVB-Farm.jpg" width="1500" height="1000">
                        <!-- <img class="img-cover lazy-load" alt="beautiful garden Pai" data-src="images/farm/11.jpg" width="1500" height="1000"> -->
                    </div>
                </div>
                <?php include 'inc/scroll-down.php'; ?>
            </div>
            <section id="section" class="intro d-none">
                <div class="bg-texture mountain">
                    <div class="container py-5">
                        <h1 class="header text-center mb-3 mb-md-5" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="100">Village Farm</h1>
                        <div class="row py-4">
                            <div class="col-12 col-md-9 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">Located less than 5 minutes from Pai Village Boutique Resort and the center of Pai, the Village Farm offers the chance to experience a unique world of fruits, flowers and animals such as sheep and rabbits. You will also enjoy local coffee and snacks as well as a souvenir shop. </p>
                                <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">The Village Farm is the perfect scenic opportunity for the most memorable photos of Pai.</p>
                                <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">And as a guest of Pai Village, the entrance fee to the Village Farm is waived with our compliments.<br>Please be our guest and enjoy our beautiful garden.</p>
                                <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">The Village Farm may be easily accessed by complementary shuttle from Pai Village Boutique Resort. Or for those who prefer, the farm is a short walking distance away.</p>
                                <div class="text-left py-3">
                                    <a class="btn btn-radius-main-color" href="./downloads/HomeStay-and-Farm-Walking-Map.pdf">Download Map</a>
                                </div>
                            </div>
                            <div class="col-12 col-md-3 py-3 d-flex justify-content-center align-items-start" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <div class="box-information">
                                    <span class="d-block mb-2">OPENING HOURS</span>
                                    <span class="d-block main-color">09.00 AM – 04.00 PM</span>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-slider" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">
                            <h2 class="subject mb-4">Gallery :</h2>
                            <div class="swiper-container swiper-gallery">
                                <div class="swiper-wrapper gallery-popup">
                                    <a class="swiper-slide" href="./images/farm/01.jpg"><img src="./images/farm/thumb/01.jpg" alt="selling organic fruits Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/farm/13.jpg"><img src="./images/farm/thumb/13.jpg" alt="relaxed atmosphere Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/farm/14.jpg"><img src="./images/farm/thumb/14.jpg" alt="relaxed atmosphere Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/farm/02.jpg"><img src="./images/farm/thumb/02.jpg" alt="relaxed atmosphere Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/farm/03.jpg"><img src="./images/farm/thumb/03.jpg" alt="stunning landscape Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/farm/04.jpg"><img src="./images/farm/thumb/04.jpg" alt="selling fresh products Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/farm/05.jpg"><img src="./images/farm/thumb/05.jpg" alt="farm and fresh Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/farm/12.jpg"><img src="./images/farm/thumb/12.jpg" alt="beautiful garden Pai" width="1500" height="843"></a>
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <?php include_once '_footer.php'; ?>