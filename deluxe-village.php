<?php
$html_class = '';
$body_class = 'page-accommodation';
$cur_page   = 'deluxe-village';
$par_page   = 'accommodation';
$title      = 'Deluxe Village - Pai Village Boutique Resort & Farm';
$desc       = 'Exclusive Offers available on Official Site ONLY. Best Rates Guaranteed. Member Deal. Private Villa and Cottage, located along the banks of Pai River featuring Rustic Bamboo Cottage set among widing pathways.';
$keyw       = 'Rasa,Hospitality,Rasa Tower,Management,Development,Business Management,Business Development,Accounting,Manager,Hotel,Resort,Investment,,Buri Rasa,Rasa Collection,Boutique,Traditional, Pai, Mae Hong Son,Thailand';
$local      = 'en-US';
$page_url   = 'deluxe-village.php';
$ogimage    = ['images/accommodation/deluxe-village/01.jpg', '1500', '1000'];

$lang_en    = $page_url;
$lang_zh    = 'zh/'. $page_url;
$lang_th    = 'th/'. $page_url;
include_once '_header.php' ?>
        <main class="site-main">
            <div class="swiper-container swiper-main">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img class="img-cover lazy-load" alt="booking Pai hotel" data-src="images/accommodation/deluxe-village/01.jpg">
                    </div>
                </div>
                <?php include 'inc/scroll-down.php'; ?>
            </div>
            <section id="section" class="intro">
                <div class="bg-texture mountain">
                    <div class="container py-5">
                        <h1 class="header text-center mb-3 mb-md-5" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="100"><?php echo _room_name_2; ?></h1>
                        <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">With 35 square meters of space, our Boutique Village rooms are located in the center of the resort, arranged as the name implies, like a village of individual cottages. Offering an opportunity to interact with other guests or simply relax on your large balcony with a book, the Boutique Village rooms feature a writing desk, daybed and all you need to feel right at home. Fitted with a king size bed, there is room for an extra bed to accommodate up to three guests.</p>
                        <div class="row py-4">
                            <div class="col-12 col-md-4 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <h2 class="subject"><b>Amenities</b></h2>
                                <ul class="pl-0">
                                    <li>Air Condition</li>
                                    <li>Bath Amenities</li>
                                    <li>Coffee & Tea Making Facilities</li>
                                    <li>Private Balcony or Terrace</li>
                                    <li>Complimentary Refreshment</li>
                                </ul>
                            </div>
                            <div class="col-12 col-md-4 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <h2 class="subject"><b>Services</b></h2>
                                <ul class="pl-0">
                                    <li>Turn Down Service</li>
                                    <li>Daily Housekeeping</li>
                                </ul>
                            </div>
                            <div class="col-12 col-md-4 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <h2 class="subject"><b>Information</b></h2>
                                <ul class="pl-0">
                                    <li>Indoor Space <?php echo _room_space_2; ?> Sq.m</li>
                                    <li>Allow up to <?php echo _room_guest_2; ?> guests</li>
                                </ul>
                            </div>
                            <div class="col-12 py-3 text-center" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="250">
                                <a class="btn btn-radius-main-color" href="<?php echo ibe_url(get_info('ibeID'), get_info('site_lang')); ?>">BOOK NOW</a>
                            </div>
                        </div>
                        <div class="gallery-slider" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">
                            <h2 class="subject mb-4">Gallery :</h2>
                            <div class="swiper-container swiper-gallery">
                                <div class="swiper-wrapper gallery-popup">
                                    <a class="swiper-slide" href="./images/accommodation/deluxe-village/01.jpg"><img src="./images/accommodation/deluxe-village/thumb/01.jpg" alt="booking Pai hotel" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/accommodation/deluxe-village/02.jpg"><img src="./images/accommodation/deluxe-village/thumb/02.jpg" alt="unique resort Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/accommodation/deluxe-village/03.jpg"><img src="./images/accommodation/deluxe-village/thumb/03.jpg" alt="cottage stay Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/accommodation/deluxe-village/06.jpg"><img src="./images/accommodation/deluxe-village/thumb/06.jpg" alt="Oasis in Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/accommodation/deluxe-village/07.jpg"><img src="./images/accommodation/deluxe-village/thumb/07.jpg" alt="Pai best resort" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/accommodation/deluxe-village/08.jpg"><img src="./images/accommodation/deluxe-village/thumb/08.jpg" alt="Pai unique hotel" width="1500" height="843"></a>
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <?php include_once '_footer.php'; ?>