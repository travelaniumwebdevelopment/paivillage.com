<?php
$html_class = '';
$body_class = 'page-location';
$cur_page   = 'location';
$par_page   = '';
$title      = 'Pai Village Boutique Resort and Farm | Boutique Resort';
$desc       = 'Exclusive Offers available on Official Site ONLY. Best Rates Guaranteed. Member Deal. Private Villa and Cottage, located along the banks of Pai River featuring Rustic Bamboo Cottage set among widing pathways.';
$keyw       = 'Rasa,Hospitality,Rasa Tower,Management,Development,Business Management,Business Development,Accounting,Manager,Hotel,Resort,Investment,,Buri Rasa,Rasa Collection,Boutique,Traditional, Pai, Mae Hong Son,Thailand';
$local      = 'en-US';
$page_url   = 'location.php';

$lang_en    = $page_url;
$lang_zh    = 'zh/'. $page_url;
$lang_th    = 'th/'. $page_url;
include_once '_header.php' ?>
        <main class="site-main">
            <div class="swiper-container" id="google_map_carousel">
                <div class="swiper-wrapper">
                    <div class="swiper-slide" data-hash="hotel">
                        <iframe width="100%" height="100%" frameborder="0" style="border:0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3764.245550748556!2d98.44284231490555!3d19.358517586926528!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30da8172490e2eb5%3A0x52493f0261a06ea7!2sPai%20Village%20Boutique%20Resort%20%26%20Farm!5e0!3m2!1sen!2sth!4v1573020983383!5m2!1sen!2sth" allowfullscreen></iframe>
                    </div>
                    <div class="swiper-slide" data-hash="transport">
                        <iframe width="100%" height="100%" frameborder="0" style="border:0" src="https://www.google.com/maps/embed?pb=!1m28!1m12!1m3!1d7528.217308847348!2d98.43583572637408!3d19.364447419602456!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m13!3e0!4m5!1s0x30da81cdba3fa25b%3A0x9a5b7b98afe2bf17!2sPai%20Airport%2C%20Wiang%20Tai%2C%20Pai%20District%2C%20Mae%20Hong%20Son!3m2!1d19.3703571!2d98.43617429999999!4m5!1s0x30da8172490e2eb5%3A0x52493f0261a06ea7!2sPai%20Village%20Boutique%20Resort%20%26%20Farm%2088%20Moo%203%20Vieng%20Tai%20Pai%2C%20Pai%20District%2C%20Mae%20Hong%20Son%2058130!3m2!1d19.3585176!2d98.445031!5e0!3m2!1sen!2sth!4v1573021177344!5m2!1sen!2sth" allowfullscreen></iframe>
                    </div>
                    <div class="swiper-slide" data-hash="shopping">
                        <iframe width="100%" height="100%" frameborder="0" style="border:0" src="https://www.google.com/maps/embed?pb=!1m12!1m8!1m3!1d15056.98601672939!2d98.438267897885!3d19.358476281644197!3m2!1i1024!2i768!4f13.1!2m1!1sshopping!5e0!3m2!1sen!2sth!4v1561633246635!5m2!1sen!2sth" allowfullscreen></iframe>
                    </div>
                </div>
                <nav class="carousel-nav" id="google_map_nav">
                    <ul class="pl-0 mb-0">
                        <li><a href="#hotel">Hotel Map</a></li>
                        <li><a href="#transport">to Airport</a></li>
                        <li><a href="#shopping">to Shopping</a></li>
                    </ul>
                </nav>
            </div>
            <section id="section" class="intro">
                <div class="bg-texture mountain">
                    <div class="container py-5">
                        <h1 class="header text-center mb-3 mb-md-5" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="100">Location</h1>
                        <div class="row py-4">
                            <div class="col-12 col-md-6 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">
                                <img class="img-cover" src="./images/location/main-pic-01.jpg" alt="convenience transportation" width="636" height="604">
                            </div>
                            <div class="col-12 col-md-6 py-3 d-flex align-items-center" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <div class="box-destination">
                                    <div class="header">
                                        <h3 class="title">Airports</h3>
                                    </div>
                                    <div class="d-flex py-3">
                                        <span class="d-block">Pai Airport</span>
                                        <span class="d-block ml-auto">1.8 Km.</span>
                                    </div>
                                    <div class="d-flex py-3">
                                        <span class="d-block">Chiang Mai Airport</span>
                                        <span class="d-block ml-auto">135 Km.</span>
                                    </div>
                                    <div class="header">
                                        <h3 class="title">Most popular landmarks</h3>
                                    </div>
                                    <div class="d-flex py-3">
                                        <span class="d-block">Pai’s Walking Street</span>
                                        <span class="d-block ml-auto">170m</span>
                                    </div>
                                    <div class="d-flex py-3">
                                        <span class="d-block">Mae Hong Son City </span>
                                        <span class="d-block ml-auto">650m</span>
                                    </div>
                                    <div class="d-flex py-3">
                                        <span class="d-block">Pai Bus Station</span>
                                        <span class="d-block ml-auto">5 Km.</span>
                                    </div>
                                    <div class="d-flex py-3">
                                        <span class="d-block">Pai’s Waterfalls</span>
                                        <span class="d-block ml-auto">8.7 Km.</span>
                                    </div>
                                    <div class="d-flex py-3">
                                        <span class="d-block">Boon Ko Ku So Bridge</span>
                                        <span class="d-block ml-auto">11.2 Km.</span>
                                    </div>
                                    <div class="d-flex py-3">
                                        <span class="d-block">Pai Memorial Bridge</span>
                                        <span class="d-block ml-auto">10.2 Km.</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="250">
                                <h2 class="subject"><b>“Transportation”</b></h2>
                                <p class="details mb-3">Enjoy amazing convenience by being picked up at Chiang Mai airport by hotel car and escorted to our resort or from the resort to Chiang Mai Airport. Our private transfer and shared transfer services are available on request.</p>
                                <h2 class="subject"><b>“Parking”</b></h2>
                                <p class="details mb-3">Due to our prime location in the center of the Pai and at the foot of walking street, parking onsite is limited. However, we have ample parking available offsite.</p>
                                <a target="_blank" href="./downloads/PVB-Transfer-Factsheet.pdf"><h2 class="subject"><u>Download Transfer Factsheet</u></h2></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <?php include_once '_footer.php'; ?>