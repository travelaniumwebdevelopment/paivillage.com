<?php require dirname(__FILE__) . '/_functions.php'; ?>
<?php
$html_class = '';
$body_class = 'page-privacy-policy';
$cur_page   = 'privacy-policy';
$par_page   = '';
$title      = 'Privacy Policy - Pai Village Boutique Resort & Farm';
$desc       = 'Exclusive Offers available on Official Site ONLY. Best Rates Guaranteed. Member Deal. Private Villa and Cottage, located along the banks of Pai River featuring Rustic Bamboo Cottage set among widing pathways.';
$keyw       = 'Rasa,Hospitality,Rasa Tower,Management,Development,Business Management,Business Development,Accounting,Manager,Hotel,Resort,Investment,,Buri Rasa,Rasa Collection,Boutique,Traditional, Pai, Mae Hong Son,Thailand';
$local      = 'en-US';
$page_url   = get_base_uri() . '/privacy-policy.php';

$lang_en    = get_base_uri() . '/privacy-policy.php';
$lang_zh    = get_base_uri() . '/zh/privacy-policy.php';
$lang_th    = get_base_uri() . '/th/privacy-policy.php';
include_once '_header.php' ?>
<style>
  .privacy-content {
    counter-reset: privacy-heading;
  }
  .privacy-content h2 {
    counter-increment: privacy-heading;
  }
  .privacy-content h2::before {
    content: counter(privacy-heading) ". ";
  }
  .privacy-content ul,
  .privacy-content ol {
    margin-left: 24px;
    padding-left: 0;
  }
  .privacy-content ul>li {
    list-style: disc;
  }
  .privacy-content ol>li {
    list-style: decimal;
  }
</style>
<main class="site-main">
	<section id="section" class="intro">
		<div class="bg-texture mountain pt-md-5">
      <div class="d-none d-xl-block" style="height:60px"></div>

			<div class="container py-5 privacy-content">
				<h1 class="header text-center mb-3 mb-md-5" data-aos="fade" data-aos-offset="100" data-aos-duration="500" data-aos-delay="100">Privacy Policy</h1>
				<p>This website www.paivillage.com is made available to you by Pai Village Boutique Resort No. 88 Moo 3, Vieng Tai, Pai Maehongson, 58130 referred to as “we” or “us”. We are aware of the importance of your privacy rights when we use your personal data for our business operation and provide services to you. Therefore, we strictly protect your personal data in accordance with the Personal Data Protection Act, B.E. 2562 (2019).</p>
				<p>We, as the protector of your personal data, are obliged to strictly comply with the law to inform you of our privacy policy for data subject which describes the collection, usage, or disclosure of your personal data, as well as your rights as the data subject data subject. We hereby confirm that we have strictly complied with the law in order to protect your personal data.</p>

				<h2>Definitions of Personal Data</h2>
				<p>Personal data can be divided into two categories namely</p>
				<p>Personal data (general) means any information relating to a person, which enables the identification of such person, whether directly or indirectly, such as name, surname, address, email address, ID card number.</p>
				<p>Sensitive Personal Data means personal data that requires special protection and particular care when collecting, using or disclosing this type of personal data such as personal data on race, ethnicity, political opinion, cult, religion or philosophy, sexual behavior, criminal record, health information, disability, trade union information, genetic data and biological data.</p>

				<h2>2. Personal data that we collect from you</h2>
				<p>We collect your information when:</p>
				<ul>
					<li>You visit our website.</li>
					<li>You provide information to us through the website or application.</li>
					<li>You provide information to us through our online or regular contact channels.</li>
				</ul>
				<p>We will keep your personal data to the extent necessary for your service provisions, to answer your inquiries and to conduct business operation in accordance with your expectation that we will use your personal data only for the aforementioned purposes. </p>
				<p>We may collect, use, or disclose different types of your personal data which can be classified into the following categories:</p>

				<table class="table table-sm small">
					<tr>
						<th>Contact Data</th>
						<td>include data such as name-surname, address, e-mail, phone number, products/services of your interest, your budget, reasons for purchasing products/services.</td>
					</tr>
					<tr>
						<th>Technic Data</th>
						<td>include data such as IP address of computer, browser type, time zone setting, location, operating system, platform and technology of the device used to access the website.</td>
					</tr>
					<tr>
						<th>Usage Data</th>
						<td>include data such as webpages that you accessed before entering the platform, webpage that you visited, amount of time spent by you on a webpage, products or information that customers search for on the platform, time and date of visiting the website, website viewing information.</td>
					</tr>
					<tr>
						<th>Communication Data</th>
						<td>include data such as e-mails, chat history and communication data.</td>
					</tr>
					<tr>
						<th>Contract - related Data</th>
						<td>include data such as identification number, address, credit card number, date of birth, gender.</td>
					</tr>
				</table>

				<h2>Source of Personal Data</h2>
				<ol>
					<li>We will obtain your personal data directly from the following channels
						<ul>
							<li>You make registration by creating an account through website or application.</li>
							<li>You contact us to obtain product/service information that you are interested in.</li>
							<li>You make an appointment to view our products/services.</li>
							<li>You provide us with information when you agree to purchase products/services from us.</li>
							<li>You participate in our promotional activities via electronic channels or social network channels or regular channels.</li>
						</ul>
					</li>
					<li>Your personal data, that we received from a third party who collects, uses or discloses personal data according to our order or on our behalf, will not be used for any mean other than for the purposes that we have determined or ordered. In addition, we have required the third party to maintain confidentiality and protect your personal data according to personal data protection law of Thailand.</li>
					<li>Personal data obtained from cookies when you visited our website. These data would help us to provide better, faster and safer service while maintaining your privacy when you use our service and/or enter our platform.</li>
				</ol>

				<h2>Objectives for Collection, Usage and Disclosure of Personal Data</h2>
				<p>We use your personal data within the scope of data protection laws and collect only the data required for the same. We will only collect data related to service provision between you and us which are required:</p>
				<ul>
					<li>For our legitimate interests or legitimate interests of a third party or your legitimate interests without prejudice fundamental rights in your personal data;</li>
					<li>For performance of contract in which you are a party or for performance in accordance with your request before entering into the contract;</li>
					<li>To comply with our laws or to prevent or suppress dangers to a person's life, body or health;</li>
				</ul>
				<p>We have summarized our utilization of your personal data for service provision and explained lawful basis of personal data processing which we adopted for each activity in the table below.</p>

				<table class="table table-sm small">
					<thead>
						<tr>
							<th>Objectives</th>
							<th>Type of Data</th>
							<th>Lawful Basis of Personal Data Processing</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Service provision including to provide information about projects of your interests.</td>
							<td>
								<ul>
									<li>Contact data</li>
									<li>Communication data</li>
								</ul>
							</td>
							<td>
								<ol>
									<li>For performance of service contracts</li>
									<li>For our legitimate interest in providing information about the products/services of your interests.</li>
								</ol>
							</td>
						</tr>
						<tr>
							<td>Customer relations management by organizing various promotional activities.</td>
							<td>
								<ul>
									<li>Contact data</li>
									<li>Communication data</li>
								</ul>
							</td>
							<td>
								<ol>
									<li>For performance of contracts</li>
									<li>For our legitimate interest in recommending products/services that meet your needs, collect data and study customers’ service usage patterns in order to develop our services</li>
								</ol>
							</td>
						</tr>
						<tr>
							<td>To manage our website, improve our service and solve technical problems incurred during website usage.</td>
							<td>
								<ul>
									<li>Contact data</li>
									<li>Technical data</li>
								</ul>
							</td>
							<td>For our legitimate interest in using your website usage data and statistics to plan our business and improve our services in order to serve our customers more efficiently.</td>
						</tr>
						<tr>
							<td>To analyze information for website development and improve customer experiences when using our services.</td>
							<td>
								<ul>
									<li>Technical data</li>
									<li>Usage data</li>
								</ul>
							</td>
							<td>For our legitimate interest in defining targeted customers, nature of customers' usage and improvement of website to meet customers' usage requirements.</td>
						</tr>
						<tr>
							<td>To enter into product/service sale and purchase contract</td>
							<td>
								<ul>
									<li>Contact data</li>
									<li>Contract - related data</li>
								</ul>
							</td>
							<td>For performance of contracts</td>
						</tr>
						<tr>
							<td>To conduct direct marketing and offer products/services that may be of your interests (subject only to your consent).</td>
							<td>
								<ul>
									<li>Contact data</li>
								</ul>
							</td>
							<td>We will obtain your prior express consent before using your personal data for direct marketing in order to offer products/services that may be of your interests.</td>
						</tr>
					</tbody>
				</table>

				<p>We may use more than one legitimate personal data processing base for processing your personal data depending on purposes of each of our activities which utilize your personal data.</p>

				<h2>Disclosure or Sharing of Personal Data</h2>
				<ol>
					<li>We may disclose or share your personal data to the following third parties:
						<ul>
							<li>Affiliates;</li>
							<li>Online advertising platform;</li>
							<li>Marketing company or consultant;</li>
							<li>Regulatory agencies such as the Office of the Commissioner for Personal Data Protection;</li>
							<li>Business or professional consultant;</li>
						</ul>

						<p>We require the abovementioned third parties that we disclose or share your personal data to maintain confidentiality and protection of your personal data according to standards stipulated in the personal data protection laws of Thailand and to use your personal data only for the purposes that we have prescribed or instructed. The third parties shall not be able to use your personal data beyond the aforementioned purposes.</p>
					</li>
					<li>Upon request, we may disclose your personal data to relevant authorities to comply with the laws, for example, to prevent threats to life or body, for the purposes of law enforcement, accusation, exercise of legal rights, and fraud prevention.</li>
				</ol>

				<h2>Retention and Retention Period of Personal Data</h2>
				<p>We will retain your personal data in our data system under standardized security for usage of our data system and your personal data with the following measures. </p>
				<ul>
					<li>Restrict access to personal data by employees, agents, partners or third parties. Third parties can only access personal data under requirements or given instruction and shall maintain confidentiality and protection of personal data.</li>
					<li>Provide technological means to prevent unauthorized access to computer systems.</li>
					<li>Destroy your personal data to maintain confidentiality when such personal data are no longer needed for legal and business purposes.</li>
					<li>Establish a process to deal with cases of personal data violations or suspected cases and must report personal data violations to the Office of the Commissioner for Personal Data Protection according to the requirements of legal provisions.</li>
				</ul>
				<p><strong>Retention Period of Personal Data</strong></p>
				<ul>
					<li>We will retain your personal data for a period necessary to carry out the purposes of our service provision and in accordance with the period stipulated by accounting standard, legal standard and other relevant regulations.</li>
					<li>In order to determine retention period of personal data, we will consider the number, nature of usage, service purposes, sensitivity of personal data, potential risks of misuse of personal data, and duration required by applicable law.</li>
					<li>Your personal data, obtained from cookies when you visited our website, will be retained for no more than 13 months or as required by relevant law.</li>
				</ul>

				<h2>Rights of Data Subject</h2>
				<p>As data subject, you are entitled to the following rights to the extent permitted by law:</p>
				<ol>
					<li>You have the right to withdraw your consent to process your personal data that you have previously given your consent to us at any time when your personal data are under our possession (Right to withdraw Consent).</li>
					<li>You have the right to access your personal data and request us to provide you with a copy of your personal data as well as request us to disclose source of your personal data which you have not given consent to us (Right of Access).</li>
					<li>You have the right to request us to rectify your personal data or add incomplete data (Right to Rectification).</li>
					<li>You have the right to request us to delete your data for some reasons (Right to Erasure).</li>
					<li>You have the right to request us to stop using your personal data for some reasons (Right to Restriction of Processing).</li>
					<li>You have the right to transfer personal data that you provided to us to another data controller or yourself for some reasons (Right to Data Portability).</li>
					<li>You have the right to object processing of your personal data for some reasons (Right to Object).</li>
				</ol>
				<p>You can contact Data Protection Officer (DPO) / the Company's officer to submit a request for exercise of the abovementioned rights. </p>

				<h2>Marketing Activities and Sale Promotions</h2>
				<p>We may conduct marketing activities and sale promotions or arrange third parties under our requirements or instructions to do the same in the following cases</p>
				<ul>
					<li>In case, we use your personal data to provide direct marketing to you. We will only do so upon your express consent and we have already informed you the purposes for using your data for direct marketing. In this regard, you can withdraw your consent to us to use your personal data for direct marketing at any time through our opt-out system.</li>
					<li>In case, you participate in sale promotional activities of the Company such as games or activities in various social network channels which aimed to promote our products and services to the general public, we will not process your data for direct marketing unless you have given us an express consent to use your personal data for direct marketing. We will inform you the purposes for using your data for direct marketing. In this regard, you can withdraw your consent to us to use your personal data for direct marketing at any time through our opt-out system.</li>
				</ul>

				<h2>Cookies Policy</h2>
				<p>Cookies are text files in your computer that are used to store log information regarding your internet usage or your browsing behavior.</p>
				<p>We use cookies to store information about your visit to our website to help us provide better, faster and safer service while maintaining your privacy when you use our services via our website. We use cookies in the following cases:</p>
				<ul>
					<li>Functionality Cookies: We use this type of cookies to help us recognize your device or browser so that we can tailor our contents to suit your personal interests more quickly and improve services on the platform to be more convenient and beneficial to you. To disable these cookies, you can set up your device by referring to help and support of your browser or device.</li>
					<li>Analytic Cookies: We use Analytics Cookies provided by Facebook and Google to analyze the traffic of our website visitors in order to offer you products or services through social media advertising. If you do not wish to allow these third parties to process your personal data, you can choose to deactivate these cookies on our website.</li>
				</ul>

				<h2>Personal Data Protection Policy of Other Websites</h2>
				<p>This privacy policy applies solely to our service and usage of our website. If you have clicked on a link to other website (even via channels on our website), you must study and comply with the privacy policy displayed on such other website in addition to ours.</p>

				<h2>Amendment to Personal Data Protection Policy</h2>
				<p>We will regularly review the effectiveness of this privacy policy. However, if there is any change in the privacy policy, we will post or publish such change to the policy on our website or other platforms.</p>

				<h2>Contact Channel</h2>
				<p><u>Details of Data Controller</u></p>
				<p>If you wish to contact us to exercise your rights as data subject or send us any inquiries and (additional) complaints, you can contact us via the following channel.</p>
				<p class="mb-0"><strong>Pai Village Boutique Resort</strong></p>
				<p class="mb-0">Contact address: No. 88 Moo 3, Vieng Tai, Pai Maehongson, 58130</p>
				<p>Contact channels Tel: +66 (0) 53 698 152, E-mail: <a href="mailto:reservations@paivillage.com">reservations@paivillage.com</a></p>
			</div>
		</div>
	</section>
</main>
<?php include_once '_footer.php'; ?>