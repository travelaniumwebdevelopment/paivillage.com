<?php
$html_class = '';
$body_class = 'page-accommodation';
$cur_page   = 'accommodation';
$par_page   = '';
$title      = 'Pai Village Boutique Resort & Farm | Traditional Rooms';
$desc       = 'Exclusive Offers available on Official Site ONLY. Best Rates Guaranteed. Member Deal. Private Villa and Cottage, located along the banks of Pai River featuring Rustic Bamboo Cottage set among widing pathways.';
$keyw       = 'Rasa,Hospitality,Rasa Tower,Management,Development,Business Management,Business Development,Accounting,Manager,Hotel,Resort,Investment,,Buri Rasa,Rasa Collection,Boutique,Traditional, Pai, Mae Hong Son,Thailand';
$local      = 'en-US';
$page_url   = 'accommodation.php';
$ogimage    = ['images/accommodation/main-slide-01.jpg', '1500', '1000'];

$lang_en    = $page_url;
$lang_zh    = 'zh/'. $page_url;
$lang_th    = 'th/'. $page_url;
include_once '_header.php' ?>
        <main class="site-main">
            <div class="swiper-container swiper-main">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img class="img-cover lazy-load" alt="Pai Village" data-src="images/accommodation/main-slide-01.jpg">
                    </div>
                </div>
                <?php include 'inc/scroll-down.php'; ?>
            </div>
            <section id="section" class="intro">
                <div class="bg-texture mountain">
                    <div class="container text-center py-5">
                        <h1 class="header mb-3 mb-md-5" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="100">Accommodation</h1>
                        <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">All guest rooms are exquisitely designed to provide boutique standards of comfort and enhance your holiday.</p>
                        <div class="row py-4">
                            <div class="col-12 col-md-6 py-3 py-md-4" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">
                                <a href="deluxe-garden.php" class="box-accommodation">
                                    <div class="box-accommodation-image">
                                        <img class="img-fluid" src="./images/accommodation/main-pic-04.png" alt="Pai Village" width="472" height="299">
                                        <h2 class="box-accommodation-name"><?php echo _room_name_3; ?></h2>
                                    </div>
                                    <ul class="box-accommodation-content">
                                        <li class="block">
                                            <span class="d-block">Max Guest</span>
                                            <span class="d-block">
                                                <img src="./assets/elements/adult.png" alt="Pai Village" width="10" height="20" role="presentation">
                                                <img src="./assets/elements/adult.png" alt="Pai Village" width="10" height="20" role="presentation">
                                            </span>
                                        </li>
                                        <li class="block">
                                            <span class="d-block">Bed Options</span>
                                            <span class="d-block">
                                                <img src="./assets/elements/king-size.png" alt="Pai Village" width="30" height="22" role="presentation">
                                            </span>
                                        </li>
                                        <li class="block">
                                            <span class="d-block">Space</span>
                                            <span class="d-block num"><?php echo _room_space_3; ?></span>
                                            <span class="d-block unit">sq.m.</span>
                                        </li>
                                        <li class="block">
                                            <span class="d-block">Free WiFi</span>
                                            <span class="d-block"><i class="fas fa-wifi" aria-hidden="true"></i></span>
                                        </li>   
                                    </ul>
                                </a>
                                <ul class="list-button pl-0 d-flex justify-content-center align-items-center">
                                    <li class="px-3"><a class="btn btn-radius-black-color" href="deluxe-garden.php">LEARN MORE</a></li>
                                    <li class="px-3"><a class="btn btn-radius-main-color" href="<?php echo ibe_url(get_info('ibeID'), get_info('site_lang')); ?>">BOOK NOW</a></li>
                                </ul>
                            </div>
                            <div class="col-12 col-md-6 py-3 py-md-4" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <a href="deluxe-village.php" class="box-accommodation">
                                    <div class="box-accommodation-image">
                                        <img class="img-fluid" src="./images/accommodation/boutique-village/boutique-village-display.png" alt="Pai Village" width="472" height="299">
                                        <h2 class="box-accommodation-name"><?php echo _room_name_2; ?></h2>
                                    </div>
                                    <ul class="box-accommodation-content">
                                        <li class="block">
                                            <span class="d-block">Max Guest</span>
                                            <span class="d-block">
                                                <img src="./assets/elements/adult.png" alt="Pai Village" width="10" height="20" role="presentation">
                                                <img src="./assets/elements/adult.png" alt="Pai Village" width="10" height="20" role="presentation">
                                                <!-- <img src="./assets/elements/adult.png" alt="Pai Village" width="10" height="20" role="presentation"> -->
                                            </span>
                                        </li>
                                        <li class="block">
                                            <span class="d-block">Bed Options</span>
                                            <span class="d-block">
                                                <img src="./assets/elements/king-size.png" alt="Pai Village" width="30" height="22" role="presentation">
                                            </span>
                                        </li>
                                        <li class="block">
                                            <span class="d-block">Space</span>
                                            <span class="d-block num"><?php echo _room_space_2; ?></span>
                                            <span class="d-block unit">sq.m.</span>
                                        </li>
                                        <li class="block">
                                            <span class="d-block">Free WiFi</span>
                                            <span class="d-block"><i class="fas fa-wifi" aria-hidden="true"></i></span>
                                        </li>   
                                    </ul>
                                </a>
                                <ul class="list-button pl-0 d-flex justify-content-center align-items-center">
                                    <li class="px-3"><a class="btn btn-radius-black-color" href="deluxe-village.php">LEARN MORE</a></li>
                                    <li class="px-3"><a class="btn btn-radius-main-color" href="<?php echo ibe_url(get_info('ibeID'), get_info('site_lang')); ?>">BOOK NOW</a></li>
                                </ul>
                            </div>
                            <div class="col-12 col-md-6 py-3 py-md-4" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <a href="boutique-grand-village.php" class="box-accommodation">
                                    <div class="box-accommodation-image">
                                        <img class="img-fluid" src="./images/main-pic-03-1.jpg" alt="Pai Village" width="472" height="299">
                                        <h2 class="box-accommodation-name"><?php echo _room_name_7; ?></h2>
                                    </div>
                                    <ul class="box-accommodation-content">
                                        <li class="block">
                                            <span class="d-block">Max Guest</span>
                                            <span class="d-block">
                                                <img src="./assets/elements/adult.png" alt="Pai Village" width="10" height="20" role="presentation">
                                                <img src="./assets/elements/adult.png" alt="Pai Village" width="10" height="20" role="presentation">
                                                <img src="./assets/elements/adult.png" alt="Pai Village" width="10" height="20" role="presentation">
                                            </span>
                                        </li>
                                        <li class="block">
                                            <span class="d-block">Bed Options</span>
                                            <span class="d-block">
                                                <img src="./assets/elements/king-size.png" alt="Pai Village" width="30" height="22" role="presentation">
                                            </span>
                                        </li>
                                        <li class="block">
                                            <span class="d-block">Space</span>
                                            <span class="d-block num"><?php echo _room_space_7; ?></span>
                                            <span class="d-block unit">sq.m.</span>
                                        </li>
                                        <li class="block">
                                            <span class="d-block">Free WiFi</span>
                                            <span class="d-block"><i class="fas fa-wifi" aria-hidden="true"></i></span>
                                        </li>   
                                    </ul>
                                </a>
                                <ul class="list-button pl-0 d-flex justify-content-center align-items-center">
                                    <li class="px-3"><a class="btn btn-radius-black-color" href="boutique-grand-village.php">LEARN MORE</a></li>
                                    <li class="px-3"><a class="btn btn-radius-main-color" href="<?php echo ibe_url(get_info('ibeID'), get_info('site_lang')); ?>">BOOK NOW</a></li>
                                </ul>
                            </div>
                            <div class="col-12 col-md-6 py-3 py-md-4" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <a href="grand-village.php" class="box-accommodation">
                                    <div class="box-accommodation-image">
                                        <img class="img-fluid" src="./images/accommodation/main-pic-03.png" alt="Pai Village" width="472" height="299">
                                        <h2 class="box-accommodation-name"><?php echo _room_name_8; ?></h2>
                                    </div>
                                    <ul class="box-accommodation-content">
                                        <li class="block">
                                            <span class="d-block">Max Guest</span>
                                            <span class="d-block">
                                                <img src="./assets/elements/adult.png" alt="Pai Village" width="10" height="20" role="presentation">
                                                <img src="./assets/elements/adult.png" alt="Pai Village" width="10" height="20" role="presentation">
                                                <img src="./assets/elements/adult.png" alt="Pai Village" width="10" height="20" role="presentation">
                                            </span>
                                        </li>
                                        <li class="block">
                                            <span class="d-block">Bed Options</span>
                                            <span class="d-block">
                                                <img src="./assets/elements/king-size.png" alt="Pai Village" width="30" height="22" role="presentation">
                                            </span>
                                        </li>
                                        <li class="block">
                                            <span class="d-block">Space</span>
                                            <span class="d-block num"><?php echo _room_space_8; ?></span>
                                            <span class="d-block unit">sq.m.</span>
                                        </li>
                                        <li class="block">
                                            <span class="d-block">Free WiFi</span>
                                            <span class="d-block"><i class="fas fa-wifi" aria-hidden="true"></i></span>
                                        </li>   
                                    </ul>
                                </a>
                                <ul class="list-button pl-0 d-flex justify-content-center align-items-center">
                                    <li class="px-3"><a class="btn btn-radius-black-color" href="grand-village.php">LEARN MORE</a></li>
                                    <li class="px-3"><a class="btn btn-radius-main-color" href="<?php echo ibe_url(get_info('ibeID'), get_info('site_lang')); ?>">BOOK NOW</a></li>
                                </ul>
                            </div>
                            <div class="col-12 col-md-6 py-3 py-md-4" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="300">
                                <a href="rasa-family-suite.php" class="box-accommodation">
                                    <div class="box-accommodation-image">
                                        <img class="img-fluid" src="./images/accommodation/main-pic-02.png" alt="Pai Village" width="472" height="299">
                                        <h2 class="box-accommodation-name"><?php echo _room_name_1; ?></h2>
                                    </div>
                                    <ul class="box-accommodation-content">
                                        <li class="block">
                                            <span class="d-block">Max Guest</span>
                                            <span class="d-block">
                                                <img src="./assets/elements/adult.png" alt="Pai Village" width="10" height="20" role="presentation">
                                                <img src="./assets/elements/adult.png" alt="Pai Village" width="10" height="20" role="presentation">
                                                <img src="./assets/elements/adult.png" alt="Pai Village" width="10" height="20" role="presentation">
                                                <img src="./assets/elements/adult.png" alt="Pai Village" width="10" height="20" role="presentation">
                                            </span>
                                        </li>
                                        <li class="block">
                                            <span class="d-block">Bed Options</span>
                                            <span class="d-block">
                                                <img src="./assets/elements/king-size.png" alt="Pai Village" width="30" height="22" role="presentation">
                                            </span>
                                        </li>
                                        <li class="block">
                                            <span class="d-block">Space</span>
                                            <span class="d-block num"><?php echo _room_space_1; ?></span>
                                            <span class="d-block unit">sq.m.</span>
                                        </li>
                                        <li class="block">
                                            <span class="d-block">Free WiFi</span>
                                            <span class="d-block"><i class="fas fa-wifi" aria-hidden="true"></i></span>
                                        </li>   
                                    </ul>
                                </a>
                                <ul class="list-button pl-0 d-flex justify-content-center align-items-center">
                                    <li class="px-3"><a class="btn btn-radius-black-color" href="rasa-family-suite.php">LEARN MORE</a></li>
                                    <li class="px-3"><a class="btn btn-radius-main-color" href="<?php echo ibe_url(get_info('ibeID'), get_info('site_lang')); ?>">BOOK NOW</a></li>
                                </ul>
                            </div>
                            <div class="col-12 col-md-6 py-3 py-md-4" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="250">
                                <a href="deluxe-pool-view.php" class="box-accommodation">
                                    <div class="box-accommodation-image">
                                        <img class="img-fluid" src="./images/accommodation/main-pic-00.png" alt="Pai Village" width="472" height="299">
                                        <h2 class="box-accommodation-name"><?php echo _room_name_5; ?></h2>
                                    </div>
                                    <ul class="box-accommodation-content">
                                        <li class="block">
                                            <span class="d-block">Max Guest</span>
                                            <span class="d-block">
                                                <img src="./assets/elements/adult.png" alt="Pai Village" width="10" height="20" role="presentation">
                                                <img src="./assets/elements/adult.png" alt="Pai Village" width="10" height="20" role="presentation">
                                                <img src="./assets/elements/adult.png" alt="Pai Village" width="10" height="20" role="presentation">
                                                
                                            </span>
                                        </li>
                                        <li class="block">
                                            <span class="d-block">Bed Options</span>
                                            <span class="d-block">
                                                <img src="./assets/elements/king-size.png" alt="Pai Village" width="30" height="22" role="presentation">
                                            </span>
                                        </li>
                                        <li class="block">
                                            <span class="d-block">Space</span>
                                            <span class="d-block num"><?php echo _room_space_5; ?></span>
                                            <span class="d-block unit">sq.m.</span>
                                        </li>
                                        <li class="block">
                                            <span class="d-block">Free WiFi</span>
                                            <span class="d-block"><i class="fas fa-wifi" aria-hidden="true"></i></span>
                                        </li>   
                                    </ul>
                                </a>
                                <ul class="list-button pl-0 d-flex justify-content-center align-items-center">
                                    <li class="px-3"><a class="btn btn-radius-black-color" href="deluxe-pool-view.php">LEARN MORE</a></li>
                                    <li class="px-3"><a class="btn btn-radius-main-color" href="<?php echo ibe_url(get_info('ibeID'), get_info('site_lang')); ?>">BOOK NOW</a></li>
                                </ul>
                            </div>
                            <div class="col-12 col-md-6 py-3 py-md-4" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="250">
                                <a href="boutique-pool-view.php" class="box-accommodation">
                                    <div class="box-accommodation-image">
                                        <img class="img-fluid" src="./images/accommodation/main-pic-06.png" alt="Pai Village" width="472" height="299">
                                        <h2 class="box-accommodation-name"><?php echo _room_name_6; ?></h2>
                                    </div>
                                    <ul class="box-accommodation-content">
                                        <li class="block">
                                            <span class="d-block">Max Guest</span>
                                            <span class="d-block">
                                                <img src="./assets/elements/adult.png" alt="Pai Village" width="10" height="20" role="presentation">
                                                <img src="./assets/elements/adult.png" alt="Pai Village" width="10" height="20" role="presentation">
                                                <img src="./assets/elements/adult.png" alt="Pai Village" width="10" height="20" role="presentation">
                                                
                                            </span>
                                        </li>
                                        <li class="block">
                                            <span class="d-block">Bed Options</span>
                                            <span class="d-block">
                                                <img src="./assets/elements/king-size.png" alt="Pai Village" width="30" height="22" role="presentation">
                                            </span>
                                        </li>
                                        <li class="block">
                                            <span class="d-block">Space</span>
                                            <span class="d-block num"><?php echo _room_space_6; ?></span>
                                            <span class="d-block unit">sq.m.</span>
                                        </li>
                                        <li class="block">
                                            <span class="d-block">Free WiFi</span>
                                            <span class="d-block"><i class="fas fa-wifi" aria-hidden="true"></i></span>
                                        </li>   
                                    </ul>
                                </a>
                                <ul class="list-button pl-0 d-flex justify-content-center align-items-center">
                                    <li class="px-3"><a class="btn btn-radius-black-color" href="boutique-pool-view.php">LEARN MORE</a></li>
                                    <li class="px-3"><a class="btn btn-radius-main-color" href="<?php echo ibe_url(get_info('ibeID'), get_info('site_lang')); ?>">BOOK NOW</a></li>
                                </ul>
                            </div>
                            <div class="col-12 col-md-6 py-3 py-md-4" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="300">
                                <a href="deluxe-villa.php" class="box-accommodation">
                                    <div class="box-accommodation-image">
                                        <img class="img-fluid" src="./images/accommodation/rasa-villa/rasa-villa-display.png" alt="Pai Village" width="472" height="299">
                                        <h2 class="box-accommodation-name"><?php echo _room_name_4; ?></h2>
                                    </div>
                                    <ul class="box-accommodation-content">
                                        <li class="block">
                                            <span class="d-block">Max Guest</span>
                                            <span class="d-block">
                                                <img src="./assets/elements/adult.png" alt="Pai Village" width="10" height="20" role="presentation">
                                                <img src="./assets/elements/adult.png" alt="Pai Village" width="10" height="20" role="presentation">
                                                <img src="./assets/elements/adult.png" alt="Pai Village" width="10" height="20" role="presentation">
                                                <!-- <img src="./assets/elements/adult.png" alt="Pai Village" width="10" height="20" role="presentation"> -->
                                            </span>
                                        </li>
                                        <li class="block">
                                            <span class="d-block">Bed Options</span>
                                            <span class="d-block">
                                                <img src="./assets/elements/king-size.png" alt="Pai Village" width="30" height="22" role="presentation">
                                            </span>
                                        </li>
                                        <li class="block">
                                            <span class="d-block">Space</span>
                                            <span class="d-block num"><?php echo _room_space_4; ?></span>
                                            <span class="d-block unit">sq.m.</span>
                                        </li>
                                        <li class="block">
                                            <span class="d-block">Free WiFi</span>
                                            <span class="d-block"><i class="fas fa-wifi" aria-hidden="true"></i></span>
                                        </li>   
                                    </ul>
                                </a>
                                <ul class="list-button pl-0 d-flex justify-content-center align-items-center">
                                    <li class="px-3"><a class="btn btn-radius-black-color" href="deluxe-villa.php">LEARN MORE</a></li>
                                    <li class="px-3"><a class="btn btn-radius-main-color" href="<?php echo ibe_url(get_info('ibeID'), get_info('site_lang')); ?>">BOOK NOW</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <?php include_once '_footer.php'; ?>