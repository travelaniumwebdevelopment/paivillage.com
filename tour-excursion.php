<?php
$html_class = '';
$body_class = 'page-tour-excursion';
$cur_page   = 'tour-excursion';
$par_page   = '';
$title      = 'Pai Village Boutique Resort and Farm | Tours In Pai';
$desc       = 'Exclusive Offers available on Official Site ONLY. Best Rates Guaranteed. Member Deal. Private Villa and Cottage, located along the banks of Pai River featuring Rustic Bamboo Cottage set among widing pathways.';
$keyw       = 'Rasa,Hospitality,Rasa Tower,Management,Development,Business Management,Business Development,Accounting,Manager,Hotel,Resort,Investment,,Buri Rasa,Rasa Collection,Boutique,Traditional, Pai, Mae Hong Son,Thailand';
$local      = 'en-US';
$page_url   = 'tour-excursion.php';
$ogimage    = ['images/tour/01.jpg', '1500', '1000'];

$lang_en    = $page_url;
$lang_zh    = 'zh/'. $page_url;
$lang_th    = 'th/'. $page_url;
include_once '_header.php' ?>
        <main class="site-main">
            <div class="swiper-container swiper-main">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img class="img-cover lazy-load" alt="Pai activities" data-src="images/tour/04.jpg">
                    </div>
                </div>
                <?php include 'inc/scroll-down.php'; ?>
            </div>
            <section id="section" class="intro">
                <div class="bg-texture mountain">
                    <div class="container py-5">
                        <h1 class="header text-center mb-3 mb-md-5" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="100">Tour & Excursions</h1>
                        <div class="row py-4">
                            <div class="col-12 col-md-9 py-3" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">Set in a particular picturesque valley north of Thailand, Pai offers a relaxed atmosphere, surrounded by<br>beautiful mountains, river valleys and the very well-known rice barn scenery.</p>
                                <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">Pai Village Boutique is delighted to offer a variety of outdoor activities for guests. Whether you are<br>seeking the ultimate adventure or a simple sightseeing tour, Pai is always a perfect place to explore,<br>take photos and make magical memories.</p>
                                <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150"><b>Pai Memorial Bridge</b> – It was made of iron, built over the Pai River in 1941 in order to transport<br>weapons and provisions of Japanese army to Myanmar (Burma) during World War II.</p>
                                <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150"><b>Chinese Yunnan Culture Center or Santichon Village</b> - A cultural place where opens for all who want to<br>learn the conservative culture of the Yunan People such as Yunnan foods, clay houses, dressing and their<br>language.</p>
                                <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150"><b>Tha Pai Hot Springs or Pong Nam Ron Thapai</b> – The lovely natural hot springs are the most popular hot<br>springs in Pai. It is located approximately 8 kilometers south of Pai’s town center.</p>
                                <p class="intro-desc" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150"><b>Mo Paeng Waterfall</b> – A beautiful waterfall located in the forests west of Pai town. This is the closet<br>waterfall from Pai center.</p>
                            </div>
                            <div class="col-12 col-md-3 py-3 d-flex justify-content-center align-items-start" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="200">
                                <div class="box-information">
                                    <span class="d-block mb-2">TO BOOK OR FIND OUT MORE, PLEASE CONTACT</span>
                                    <span class="d-block">E. <a class="main-color" href="mailto:reservations@paivillage.com">reservations@paivillage.com</a></span>
                                    <span class="d-block">T. <a class="main-color" href="tel:+6653698152">+66 5369 8152</a></span>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-slider" data-aos="fade-up" data-aos-offset="100" data-aos-duration="500" data-aos-delay="150">
                            <h2 class="subject mb-4">Gallery :</h2>
                            <div class="swiper-container swiper-gallery">
                                <div class="swiper-wrapper gallery-popup">
                                    <a class="swiper-slide" href="./images/tour/04.jpg"><img src="./images/tour/thumb/04.jpg" alt="Pai activities" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/tour/05.jpg"><img src="./images/tour/thumb/05.jpg" alt="outdoor activities Pai" width="1500" height="843"></a>
                                    <!-- <a class="swiper-slide" href="./images/tour/06.jpg"><img src="./images/tour/thumb/06.jpg" alt="Pai Village" width="1500" height="843"></a> -->
                                    <a class="swiper-slide" href="./images/tour/01.jpg"><img src="./images/tour/thumb/01.jpg" alt="Pai tour experience" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/tour/02.jpg"><img src="./images/tour/thumb/02.jpg" alt="tour experience Pai" width="1500" height="843"></a>
                                    <a class="swiper-slide" href="./images/tour/03.jpg"><img src="./images/tour/thumb/03.jpg" alt="Best activities Pai" width="1500" height="843"></a>
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <?php include_once '_footer.php'; ?>